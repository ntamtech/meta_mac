#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "CoordinateContainer.h"
#import "CoordinateManager.h"

FOUNDATION_EXPORT double MTCoordinatorView_objcVersionNumber;
FOUNDATION_EXPORT const unsigned char MTCoordinatorView_objcVersionString[];

