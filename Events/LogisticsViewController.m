//
//  LogisticsViewController.m
//  Events
//
//  Created by esam ahmed eisa on 10/18/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import "LogisticsViewController.h"
#import <GoogleMaps/GoogleMaps.h>

@interface LogisticsViewController ()

@property (weak, nonatomic) IBOutlet GMSMapView *mapview;

@end

@implementation LogisticsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:35.158881
                                                            longitude:33.371543
                                                                 zoom:10];
    [self.mapview setCamera:camera];
    // Creates a marker in the center of the map.
    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.position = CLLocationCoordinate2DMake(35.158881, 33.371543);
    marker.title = @"Cyprus";
    marker.snippet = @" Hilton Park Nicosia.";
    marker.map = _mapview;
}

-(IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}



 

@end
