//
//  AgendaViewController.m
//  Events
//
//  Created by M.I.Kamashany on 3/5/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import "FullAgendaViewController.h"
#import "DynamicItemWidthTabController.h"

@interface FullAgendaViewController (){
    BOOL isSeen ;
}

@end

@implementation FullAgendaViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    isSeen = NO ;
}

-(void)viewWillAppear:(BOOL)animated {
    if (!isSeen) {
        isSeen = YES ;
        DynamicItemWidthTabController *controller1 = [[DynamicItemWidthTabController alloc] init];
        controller1.isFullAgenda = YES ;
        [self addChildViewController:controller1];
        controller1.view.frame = self.view.frame;
        [self.view addSubview:controller1.view];
        [controller1 didMoveToParentViewController:self];
    }
}

-(UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent ;
}
@end
