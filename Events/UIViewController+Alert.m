//
//  UIViewController+Alert.m
//  Ntam Care
//
//  Created by M.I.Kamashany on 1/16/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import "UIViewController+Alert.h"

@implementation UIViewController (Alert)

-(void)showAlert:(NSString *)msg {
    NSString *title = @"";
    NSString *subtitle = msg;
    UIAlertController *alertController=   [UIAlertController
                                           alertControllerWithTitle:title
                                           message:subtitle
                                           preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction
                         actionWithTitle:@"Ok"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [alertController dismissViewControllerAnimated:YES completion:nil];
                             
                         }];
    [alertController addAction:ok];
    [self presentViewController:alertController animated:YES completion:nil];
}

@end
