//
//  AgendaModel.h
//  Events
//
//  Created by M.I.Kamashany on 3/6/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Speaker.h"
#import "JSONModel.h"

@interface AgendaModel : JSONModel

@property (nonatomic,strong)    NSString<Optional> *day_number ;
@property (nonatomic,strong)    NSString<Optional> *event_date ;
@property (nonatomic,strong)    NSMutableArray<Session> *sessions ;

@end
