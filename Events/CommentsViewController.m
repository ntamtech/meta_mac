//
//  CommentsViewController.m
//  Events
//
//  Created by M.I.Kamashany on 3/29/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import "CommentsViewController.h"
#import "SideBarViewController.h"
#import "PublicCommentTableViewCell.h"
#import "UIImageView+WebCache.h"
#import "WritePostViewController.h"
#import <STPopup/STPopup.h>
#import "SharedData.h"
#import "AgendaModel.h"
#import "ServiceHandler.h"

@interface CommentsViewController ()<UITableViewDelegate,UITableViewDataSource,WritePostViewControllerDelegate,ServiceHandlerDelegate>
{
    SharedData *shared ;
    ServiceHandler *service ;
}

@property (nonatomic,weak) IBOutlet UIButton *likeBtn ;
@property (nonatomic,weak) IBOutlet UILabel *likeLabel ;
@property (nonatomic,weak) IBOutlet UITableView *commentsTableView ;
-(IBAction)addComment:(id)sender ;

@end

@implementation CommentsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _commentsTableView.estimatedRowHeight = 44 ;
    _commentsTableView.rowHeight = UITableViewAutomaticDimension ;
    
    shared = [SharedData getSharedObject];
    service = [[ServiceHandler alloc]init];
    service.delegate = self ;
    
    if (self.selectePhoto) {
        if (self.selectePhoto.comments.count == 0) {
            _commentsTableView.hidden = YES ;
        }
    }else if (self.selectedPost){
        if (self.selectedPost.comments.count == 0) {
            _commentsTableView.hidden = YES ;
        }
    }else{
        if (self.selectedSession.session_comments.count == 0) {
            _commentsTableView.hidden = YES ;
        }
    }
}

-(void)viewWillAppear:(BOOL)animated {
//    
//    if (self.selectePhoto) {
//        if ([self.selectePhoto.make_like isEqualToString:@"1"]) {
//            NSString *usernamesStr = @"";
//            if (self.selectePhoto.last_likes != nil) {
//                for (LastLikeUser *lastLike in self.selectePhoto.last_likes) {
//                    if (![lastLike.user_id isEqualToString:shared.user.id]) {
//                        usernamesStr = [usernamesStr stringByAppendingString: [usernames objectForKey:@"username"]];
//                    }
//                }
//                _likeLabel.text = [NSString stringWithFormat:@"You %@ liked this",usernamesStr];
//            }else{
//                 _likeLabel.text = @"You liked this";
//            }
//        } else {
//            if (self.selectePhoto.last_likes != 0) {
//                NSString *usernamesStr = @"";
//                for (NSDictionary *usernames in self.selectePhoto.last_likes) {
//                    usernamesStr = [usernamesStr stringByAppendingString: [usernames objectForKey:@"username"]];
//                }
//                _likeLabel.text = [NSString stringWithFormat:@"%@ liked this",usernamesStr];
//            }else{
//                _likeLabel.text = @"No likes";
//            }
//        }
//    }else if (self.selectedPost){
//        if ([self.selectedPost.make_like isEqualToString:@"1"]) {
//            if (self.selectedPost.last_likes != nil) {
//                NSDictionary *usernameDic = [self.selectedPost.last_likes objectAtIndex:0];
//                _likeLabel.text = [NSString stringWithFormat:@"You and %@ liked this",[usernameDic objectForKey:@"username"]];
//            }else{
//                _likeLabel.text = @"You liked this";
//            }
//        } else {
//            if (self.selectedPost.last_likes.count != 0) {
//                NSString *usernamesStr = @"";
//                for (NSDictionary *usernames in self.selectedPost.last_likes) {
//                    usernamesStr = [usernamesStr stringByAppendingString: [usernames objectForKey:@"username"]];
//                }
//                _likeLabel.text = [NSString stringWithFormat:@"%@ liked this",usernamesStr];
//            }else{
//                _likeLabel.text = @"No likes";
//            }
//        }
//    }else{
//        if ([self.selectedSession.make_like isEqualToString:@"1"]) {
//            if (self.selectedSession.last_likes != nil) {
//                NSDictionary *usernameDic = [self.selectedSession.last_likes objectAtIndex:0];
//                _likeLabel.text = [NSString stringWithFormat:@"You and %@ liked this",[usernameDic objectForKey:@"username"]];
//            }else{
//                _likeLabel.text = @"You liked this";
//            }
//        } else {
//            if (self.selectedSession.last_likes != 0) {
//                NSString *usernamesStr = @"";
//                for (NSDictionary *usernames in self.selectedSession.last_likes) {
//                    usernamesStr = [usernamesStr stringByAppendingString: [usernames objectForKey:@"username"]];
//                }
//                _likeLabel.text = [NSString stringWithFormat:@"%@ liked this",usernamesStr];
//            }else{
//                _likeLabel.text = @"No likes";
//            }
//        }
//    }
}


#pragma mark -TableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1 ;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.selectePhoto) {
        return self.selectePhoto.comments.count ;
    }else if (self.selectedPost){
        return self.selectedPost.comments.count ;
    }else{
        return self.selectedSession.session_comments.count ;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"publicCommentCell";
    PublicCommentTableViewCell *cell = (PublicCommentTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell == nil) {
        cell =[[PublicCommentTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    
    CommentModel *comm ;
    if (self.selectePhoto) {
        comm = [self.selectePhoto.comments objectAtIndex:indexPath.row];
    }else if (self.selectedPost){
        comm = [self.selectedPost.comments objectAtIndex:indexPath.row];
    }else{
        comm = [self.selectedSession.session_comments objectAtIndex:indexPath.row];
    }
    
    cell.nameLabel.text = comm.username;
    cell.commentBodyLabel.text = comm.comment;
    [cell.commentOwnerImageView setShowActivityIndicatorView:YES];
    [cell.commentOwnerImageView setIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [cell.commentOwnerImageView sd_setImageWithURL:[NSURL URLWithString:comm.user_image] placeholderImage:[UIImage imageNamed:@"profile-image"]];
    tableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    cell.selectionStyle = UITableViewCellSelectionStyleNone ;
    return cell ;
}



-(IBAction)addComment:(id)sender {
    WritePostViewController *destination = [self.storyboard instantiateViewControllerWithIdentifier:@"WritePostViewController"];
    destination.delegate = self ;
    destination.isWritePost = NO;
    destination.contentSizeInPopup = CGSizeMake(300, 300);
    STPopupController *popupController = [[STPopupController alloc] initWithRootViewController:destination];
    destination.selectedIndex = self.selectedIndex ;
    popupController.navigationBarHidden = YES ;
    [popupController presentInViewController:self];
}

-(IBAction)like:(id)sender {
    
}



-(IBAction)showMenu:(id)sender {
    SideBarViewController *destination = (SideBarViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"SideBarViewController"];
    [self.navigationController pushViewController:destination animated:YES];
}

-(IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


-(void)dismissWithCommentDetails:(NSString *)content andIndexPath:(NSIndexPath *)index {
    _commentsTableView.hidden = NO ;
    if (self.selectePhoto) {
        CommentModel *newComment = [[CommentModel alloc]init];
        newComment.user_id = shared.user.id ;
        newComment.username = shared.user.name ;
        newComment.user_image = shared.user.image ;
        newComment.comment = content ;
        [self.selectePhoto.comments addObject:newComment];
        [_commentsTableView reloadData];
        [shared.photosArr removeObjectAtIndex:self.selectedIndex.row];
        [shared.photosArr insertObject:self.selectePhoto atIndex:self.selectedIndex.row];
        [service addPhotoComment:[NSNumber numberWithInt:1] andUserID:[NSNumber numberWithInt:[shared.user.id intValue]] andComment:content andPhotoID:[NSNumber numberWithInt:[self.selectePhoto.id intValue]]];
    }else if (self.selectedPost){
        CommentModel *newComment = [[CommentModel alloc]init];
        newComment.user_id = shared.user.id ;
        newComment.username = shared.user.name ;
        newComment.user_image = shared.user.image ;
        newComment.comment = content ;
        [self.selectedPost.comments addObject:newComment];
        [_commentsTableView reloadData];
        [shared.postsArr removeObjectAtIndex:self.selectedIndex.row];
        [shared.postsArr insertObject:self.selectedPost atIndex:self.selectedIndex.row];
        [service addPostComment:[NSNumber numberWithInt:1] andUserID:[NSNumber numberWithInt:[shared.user.id intValue]] andComment:content andPostID:[NSNumber numberWithInt:[self.selectedPost.id intValue]]];
    }else{
        CommentModel *newComment = [[CommentModel alloc]init];
        newComment.user_id = shared.user.id ;
        newComment.username = shared.user.name ;
        newComment.user_image = shared.user.image ;
        newComment.comment = content ;
        [self.selectedSession.session_comments addObject:newComment];
        [_commentsTableView reloadData];
        for (int j =0 ; j < shared.allAgenda.count ; j++){
            AgendaModel *agenda = [shared.allAgenda objectAtIndex:j];
            if ([agenda.day_number isEqualToString:self.agendaDay_number]){
                for (int i = 0 ; i <agenda.sessions.count ; i ++){
                    Session *sess = [agenda.sessions objectAtIndex:i];
                    if ([self.selectedSession.id isEqualToString:sess.id]) {
                        [agenda.sessions removeObjectAtIndex:i];
                        [agenda.sessions insertObject:self.selectedSession atIndex:i];
                        break;
                    }
                }
                [shared.allAgenda removeObjectAtIndex:j];
                [shared.allAgenda insertObject:agenda atIndex:j];
            }
        }
        [service addSessionComment:[NSNumber numberWithInt:[shared.user.id intValue]] andComment:content andSessionID:[NSNumber numberWithInt:[self.selectedSession.id intValue]]];
    }
}


-(void)viewWillDisappear:(BOOL)animated {
    if (self.selectedSession) {
        [self.delegate updateSession:self.selectedSession atIndexPath:self.selectedIndex];
    }
}
@end
