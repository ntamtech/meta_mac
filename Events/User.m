//
//  User.m
//  Events
//
//  Created by M.I.Kamashany on 2/18/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import "User.h"

@implementation User


- (void)encodeWithCoder:(NSCoder *)encoder {
    //Encode properties, other class variables, etc
    [encoder encodeObject:self.id forKey:@"id"];
    [encoder encodeObject:self.name forKey:@"name"];
    [encoder encodeObject:self.email forKey:@"email"];
    [encoder encodeObject:self.code forKey:@"code"];
    [encoder encodeObject:self.company forKey:@"company"];
    [encoder encodeObject:self.position forKey:@"position"];
    [encoder encodeObject:self.image forKey:@"image"];
    [encoder encodeObject:self.bio forKey:@"bio"];
    [encoder encodeObject:self.admin forKey:@"admin"];
    [encoder encodeObject:self.register_id forKey:@"register_id"];
    [encoder encodeObject:self.fb_link forKey:@"fb_link"];
    [encoder encodeObject:self.linkedin_link forKey:@"linkedin_link"];
    [encoder encodeObject:self.event_id forKey:@"event_id"];
    [encoder encodeObject:self.location forKey:@"location"];
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        //decode properties, other class vars
        self.id= [decoder decodeObjectForKey:@"id"];
        self.name =[decoder decodeObjectForKey:@"name"];
        self.email =[decoder decodeObjectForKey:@"email"];
        self.code =[decoder decodeObjectForKey:@"code"];
        self.company= [decoder decodeObjectForKey:@"company"];
        self.position =[decoder decodeObjectForKey:@"position"];
        self.image =[decoder decodeObjectForKey:@"image"];
        self.bio =[decoder decodeObjectForKey:@"bio"];
        self.admin= [decoder decodeObjectForKey:@"admin"];
        self.register_id =[decoder decodeObjectForKey:@"register_id"];
        self.fb_link =[decoder decodeObjectForKey:@"fb_link"];
        self.linkedin_link =[decoder decodeObjectForKey:@"linkedin_link"];
        self.event_id = [decoder decodeObjectForKey:@"event_id"];
        self.location = [decoder decodeObjectForKey:@"location"];
    }
    return self;
}
@end
