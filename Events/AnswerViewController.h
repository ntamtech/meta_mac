//
//  AnswerViewController.h
//  Events
//
//  Created by M.I.Kamashany on 3/13/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PollModel.h"
@interface AnswerViewController : UIViewController

@property (nonatomic,strong) NSIndexPath *pollIndexAtPolls ;
@property (nonatomic,strong) NSIndexPath *postIndexAtNewsFeeds ;

@end
