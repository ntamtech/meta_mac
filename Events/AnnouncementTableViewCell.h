//
//  AnnouncementTableViewCell.h
//  Events
//
//  Created by M.I.Kamashany on 3/11/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AnnouncementTableViewCell : UITableViewCell

@property (nonatomic,weak) IBOutlet UILabel *bodyLabel ;
@property (nonatomic,weak) IBOutlet UILabel *titleLabel ;
@property (nonatomic,weak) IBOutlet UILabel *dateTimeLabel ;

@end
