//
//  CommentTableViewCell.m
//  Events
//
//  Created by M.I.Kamashany on 3/27/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import "CommentTableViewCell.h"

@implementation CommentTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        NSArray *nibs = [[NSBundle mainBundle] loadNibNamed:@"CommentTableViewCell" owner:self options:nil];
        self = [nibs objectAtIndex:0];
    }
    self.bgView.layer.cornerRadius = 6 ;
    self.bgView.layer.masksToBounds = YES ;
    return self;
}
@end
