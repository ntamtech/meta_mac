//
//  WritePostViewController.h
//  Events
//
//  Created by M.I.Kamashany on 2/28/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol  WritePostViewControllerDelegate <NSObject>

@optional

-(void)dismissWithPostDetails:(NSString *)details ;
-(void)dismissWithCommentDetails:(NSString *)content andIndexPath:(NSIndexPath *)index;
@end

@interface WritePostViewController : UIViewController

@property (nonatomic,assign) BOOL isWritePost ;
@property (nonatomic,strong) NSIndexPath *selectedIndex ;
@property (nonatomic,weak) id<WritePostViewControllerDelegate> delegate ;

@property (weak, nonatomic) IBOutlet UITextView *contentTV;
@property (weak, nonatomic) IBOutlet UIButton *postBtn;
- (IBAction)Post:(id)sender;
- (IBAction)cancel:(id)sender;
@end
