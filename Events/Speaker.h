//
//  Speaker.h
//  Events
//
//  Created by M.I.Kamashany on 2/19/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"
#import "PostModel.h"

@protocol Session ;
@protocol Speaker ;


@interface Speaker : JSONModel

@property (nonatomic) NSString<Optional> *id ;
@property (nonatomic,strong)    NSString<Optional> *name ;
@property (nonatomic,strong)    NSString<Optional> *facebook_link ;
@property (nonatomic,strong)    NSString<Optional> *twitter_link ;
@property (nonatomic,strong)    NSString<Optional> *linkedin_link ;
@property (nonatomic,strong)    NSString<Optional> *bio ;
@property (nonatomic,strong)    NSString<Optional> *image ;
@property (nonatomic,strong)    NSString<Optional> *position ;
@property (nonatomic,strong)    NSString<Optional> *company ;
@property (nonatomic,strong)    NSString<Optional> *event_id ;
@property (nonatomic,strong)    NSMutableArray<Session,Optional> *sessions ;

@end


@interface Session : JSONModel

@property (nonatomic) NSString<Optional> *id ;
@property (nonatomic,strong) NSString<Optional> *session_name ;
@property (nonatomic,strong) NSString<Optional> *session_date ;
@property (nonatomic,strong) NSString<Optional> *start_time ;
@property (nonatomic,strong) NSString<Optional> *end_time ;
@property (nonatomic,strong) NSString<Optional> *session_tags ;
@property (nonatomic,strong) NSString<Optional> *desc ;
@property (nonatomic,strong)    NSString<Optional> *has_rate ;
@property (nonatomic,strong)    NSString<Optional> *venue ;
@property (nonatomic,strong)    NSString<Optional> *physical_address ;
@property (nonatomic,strong)    NSMutableArray<Speaker,Optional> *list_of_speaker ;
@property (nonatomic,strong)    NSString<Optional> *no_of_interested ;
@property (nonatomic,strong)    NSString<Optional> *session_rate_of_user;

@property (nonatomic,strong)    NSString<Optional> *add_to_agenda;
@property (nonatomic,strong)    NSMutableArray<CommentModel,Optional> *session_comments;
@property (nonatomic,strong)    NSString<Optional> *session_likes ;
@property (nonatomic,strong)    NSString<Optional> *make_like ;
@property (nonatomic,strong)    NSMutableArray<LastLikeUser,Optional> *last_likes ;

@end
