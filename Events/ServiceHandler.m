//
//  ServiceHandler.m
//  Ntam Care
//
//  Created by M.I.Kamashany on 1/16/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import "ServiceHandler.h"
#import "URL.h"
#import "AFNetworking.h"
#import "Constants.h"
#import "SharedData.h"
#import "Speaker.h"
#import "SliderModel.h"
#import "PostModel.h"
#import "AttendeeModel.h"
#import "PollModel.h"
#import "AgendaModel.h"
#import "AnnouncementType.h"
#import "Question.h"
#import "Answer.h"

@implementation ServiceHandler

-(void)loadAboutData {
    NSString *url = [NSString stringWithFormat:@"%@%@",BASE_URL,[URL getAboutAppURL]];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSDictionary *params = @{@"event_id":[NSNumber numberWithInt:[EVENT_ID intValue]]};
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSString *responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSData *dataStream = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
        id json = [NSJSONSerialization JSONObjectWithData:dataStream options:0 error:nil];
        NSDictionary *responseDic = (NSDictionary *) json ;
        NSError *error ;
        NSString *status = [NSString stringWithFormat:@"%@",[responseDic objectForKey:@"status"]];
        
        if ([status isEqualToString:@"true"]) {
            NSDictionary *userData_dic = (NSDictionary *)[responseDic objectForKey:@"about_event"];
            About *aboutData = [[About alloc]initWithDictionary:userData_dic error:&error];
            [self.delegate getAboutData:aboutData];
        } else {
            NSString *error = [responseDic objectForKey:@"massage"];
            [self.delegate requestFailWithError:error];
        }
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        [self.delegate requestFailWithError:requestFailed];
    }];
}

-(void)loadWebData:(ContentType)type  {
    NSString *url = @"";
    switch (type) {
        case 0:
            url = [NSString stringWithFormat:@"http://ntam.tech/istanbul_event_service/general_information.php"];
            break;
        case 1:
            url = [NSString stringWithFormat:@"http://ntam.tech/istanbul_event_service/leadership_principles.php"];
            break;
        case 2:
            url = [NSString stringWithFormat:@"%@%@",BASE_URL,[URL getAboutAppURL]];
            break;
        case 3:
            url = [NSString stringWithFormat:@"http://ntam.tech/istanbul_event_service/dinners.php"];
            break;
        default:
            break;
    }
    
    if (type == 2) {
        [self loadAboutData];
    }else{
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        [manager.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        [manager POST:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            NSString *responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
            NSData *dataStream = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
            id json = [NSJSONSerialization JSONObjectWithData:dataStream options:0 error:nil];
            NSDictionary *responseDic = (NSDictionary *) json ;
            NSError *error ;
            NSString *status = [NSString stringWithFormat:@"%@",[responseDic objectForKey:@"status"]];
            
            if ([status isEqualToString:@"true"]) {
                NSDictionary *webDataDictionary = (NSDictionary *)[responseDic objectForKey:@"data"];
                WebData *webData = [[WebData alloc]initWithDictionary:webDataDictionary error:&error];
                [self.delegate getWebData:webData];
            } else {
                NSString *error = [responseDic objectForKey:@"massage"];
                [self.delegate requestFailWithError:error];
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [self.delegate requestFailWithError:requestFailed];
        }];
    }
}

-(void)loginByEmail:(NSString *)email code:(NSString *)code registerID:(NSString *)registerID eventID:(NSNumber *)eventID {
    
    SharedData *shared = [SharedData getSharedObject];
    NSString *url = [NSString stringWithFormat:@"%@%@",BASE_URL,[URL getLoginURL]];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSDictionary *params = @{@"name":email,@"code":code,@"register_id":registerID,@"event_id":eventID};
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSString *responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSData *dataStream = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
        id json = [NSJSONSerialization JSONObjectWithData:dataStream options:0 error:nil];
        NSDictionary *responseDic = (NSDictionary *) json ;
        NSString *status = [NSString stringWithFormat:@"%@",[responseDic objectForKey:@"status"]];
        NSError *error ;
        if ([status isEqualToString:@"true"]) {
            NSDictionary *userData_dic = (NSDictionary *)[responseDic objectForKey:@"user_data"];
            User *LoginUser = [[User alloc]initWithDictionary:userData_dic error:&error];
            shared.user = LoginUser ;
            [self.delegate loginSuccess];   
        } else {
            NSString *error = [responseDic objectForKey:@"massage"];
            [self.delegate requestFailWithError:error];
        }
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        [self.delegate requestFailWithError:requestFailed];
    }];
}

-(void)completeData:(NSString *)email country:(NSString *)country hospital:(NSString *)hospital mobile:(NSString *)mobile userID:(NSNumber *)userID {
    NSString *url = [NSString stringWithFormat:@"%@%@",BASE_URL,[URL getSignupURL]];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSDictionary *params = @{@"event_id":@1,@"user_id":userID,@"country":country,@"email":email,@"mobile":mobile,@"hospital":hospital};
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSString *responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSData *dataStream = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
        id json = [NSJSONSerialization JSONObjectWithData:dataStream options:0 error:nil];
        NSDictionary *responseDic = (NSDictionary *) json ;
        NSString *status = [NSString stringWithFormat:@"%@",[responseDic objectForKey:@"status"]];
        if ([status isEqualToString:@"true"]) {
            [self.delegate completeDataSuccess];
        } else {
            NSString *error = [responseDic objectForKey:@"message"];
            [self.delegate requestFailWithError:error];
        }
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        [self.delegate requestFailWithError:requestFailed];
    }];
}

-(void)loadAllMessages:(NSNumber *)userID eventID:(NSNumber *)eventID  {
    
    NSString *url = [NSString stringWithFormat:@"%@%@",BASE_URL,[URL getAllMessagesURL]];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSDictionary *params = @{@"user_id":userID,@"event_id":eventID};
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSString *responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSData *dataStream = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
        id json = [NSJSONSerialization JSONObjectWithData:dataStream options:0 error:nil];
        NSDictionary *responseDic = (NSDictionary *) json ;
        NSString *status = [NSString stringWithFormat:@"%@",[responseDic objectForKey:@"status"]];
        NSError *error ;
        if ([status isEqualToString:@"true"]) {
            NSMutableArray *messagesArr = [[NSMutableArray alloc]init];
            NSArray *messagesArrJSON = (NSArray *)[responseDic objectForKey:@"chat_list"];
            for (NSDictionary *messageDic in messagesArrJSON) {
                Message *message = [[Message alloc]initWithDictionary:messageDic error:&error];
                if (message) {
                    [messagesArr addObject:message];
                }
            }
            [self.delegate getAllMessages:messagesArr];
        } else {
            NSString *error = [responseDic objectForKey:@"massage"];
            [self.delegate requestFailWithError:error];
        }
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        [self.delegate requestFailWithError:requestFailed];
    }];
}


-(void)loadAllSpeakers:(NSNumber *)eventID UserID:(NSNumber *)userID{
    NSString *url = [NSString stringWithFormat:@"%@%@",BASE_URL,[URL getAllSpeakersURL]];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSDictionary *params = @{@"event_id":eventID,@"user_id":userID};
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSString *responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSData *dataStream = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
        id json = [NSJSONSerialization JSONObjectWithData:dataStream options:0 error:nil];
        NSDictionary *responseDic = (NSDictionary *) json ;
        NSString *status = [NSString stringWithFormat:@"%@",[responseDic objectForKey:@"status"]];
        NSError *error ;
        if ([status isEqualToString:@"true"]) {
            NSMutableArray *speakersArr = [[NSMutableArray alloc]init];
            NSArray *speakersArrJSON = (NSArray *)[responseDic objectForKey:@"all_speaker"];
            for (NSDictionary *speakerDic in speakersArrJSON) {
                Speaker *speaker = [[Speaker alloc]initWithDictionary:speakerDic error:&error];
                if (speaker) {
                    [speakersArr addObject:speaker];
                }
            }
            [self.delegate getAllSpeakers:speakersArr];
        } else {
            NSString *error = [responseDic objectForKey:@"massage"];
            [self.delegate requestFailWithError:error];
        }
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        [self.delegate requestFailWithError:requestFailed];
    }];
}

-(void)loadAllAttendees:(NSNumber *)eventID  {
    NSString *url = [NSString stringWithFormat:@"%@%@",BASE_URL,[URL getAttendeesURL]];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSDictionary *params = @{@"event_id":eventID};
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSString *responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSData *dataStream = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
        id json = [NSJSONSerialization JSONObjectWithData:dataStream options:0 error:nil];
        NSDictionary *responseDic = (NSDictionary *) json ;
        NSString *status = [NSString stringWithFormat:@"%@",[responseDic objectForKey:@"status"]];
        NSError *error ;
        if ([status isEqualToString:@"true"]) {
            
            NSMutableDictionary *result = [[NSMutableDictionary alloc]init];
            NSArray *attendeesArrJSON = (NSArray *)[responseDic objectForKey:@"all_attends"];
            for (NSDictionary *attendeesDic in attendeesArrJSON) {
                NSString *letter = [attendeesDic objectForKey:@"letter"];
                NSArray *attendees = (NSArray *)[attendeesDic objectForKey:@"attends"];
                
                NSMutableArray *attendeesArr = [[NSMutableArray alloc]init];
                for (NSDictionary *attendeeDic in attendees) {
                    AttendeeModel *attendee = [[AttendeeModel alloc]initWithDictionary:attendeeDic error:&error];
                    [attendeesArr addObject:attendee];
                }
                [result setValue:attendeesArr forKey:letter];
            }
            [self.delegate getAllAttendees:result];
        } else {
            NSString *error = [responseDic objectForKey:@"massage"];
            [self.delegate requestFailWithError:error];
        }
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        [self.delegate requestFailWithError:requestFailed];
    }];
}


-(void)loadEventSliderImages:(NSNumber *)eventID {
    NSString *url = [NSString stringWithFormat:@"%@%@",BASE_URL,[URL getEventSliderURL]];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSDictionary *params = @{@"event_id":eventID};
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSString *responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSData *dataStream = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
        id json = [NSJSONSerialization JSONObjectWithData:dataStream options:0 error:nil];
        NSDictionary *responseDic = (NSDictionary *) json ;
        NSString *status = [NSString stringWithFormat:@"%@",[responseDic objectForKey:@"status"]];
        NSError *error ;
        if ([status isEqualToString:@"true"]) {
            NSMutableArray *sliderImagesArr = [[NSMutableArray alloc]init];
            NSArray *sliderArrJSON = (NSArray *)[responseDic objectForKey:@"slider"];
            for (NSDictionary *sliderDic in sliderArrJSON) {
                SliderModel *sliderObj = [[SliderModel alloc]initWithDictionary:sliderDic error:&error];
                if (sliderObj) {
                    [sliderImagesArr addObject:sliderObj];
                }
            }
            [self.delegate getAllSliderImages:sliderImagesArr];
        } else {
            NSString *error = [responseDic objectForKey:@"massage"];
            [self.delegate requestFailWithError:error];
        }
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        [self.delegate requestFailWithError:requestFailed];
    }];
}


-(void)loadAlPosts:(NSNumber *)userID eventID:(NSNumber *)eventID andPageNum:(NSNumber *)num andPostDate:(NSString *)date {
    NSString *url = [NSString stringWithFormat:@"%@%@",BASE_URL,[URL getAllPostsURL]];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSDictionary *params ;
    if (![date isEqualToString:@""]) {
        params = @{@"event_id":eventID,@"user_id":userID,@"page_number":num , @"post_date":date};
    } else {
        params = @{@"event_id":eventID,@"user_id":userID,@"page_number":num};
    }
    
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSString *responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSData *dataStream = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
        id json = [NSJSONSerialization JSONObjectWithData:dataStream options:0 error:nil];
        NSDictionary *responseDic = (NSDictionary *) json ;
        NSString *status = [NSString stringWithFormat:@"%@",[responseDic objectForKey:@"status"]];
        NSError *error ;
        if ([status isEqualToString:@"true"]) {
            NSMutableArray *postsArr = [[NSMutableArray alloc]init];
            NSArray *postsArrJSON = (NSArray *)[responseDic objectForKey:@"all_posts"];
            for (NSDictionary *postDic in postsArrJSON) {
                PostModel *postObj = [[PostModel alloc]initWithDictionary:postDic error:&error];
                if (postObj) {
                    [postsArr addObject:postObj];
                }
            }
            [self.delegate getPosts:postsArr];
        } else {
            NSString *error = [responseDic objectForKey:@"massage"];
            [self.delegate requestFailWithError:error];
        }
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        [self.delegate requestFailWithError:requestFailed];
    }];
}

-(void)sendPostWithContent:(NSString *)content eventID:(NSNumber *)eventID userID:(NSNumber *)userID isCheckIn:(BOOL)flag {
    NSString *url = [NSString stringWithFormat:@"%@%@",BASE_URL,[URL getAaddPostURL]];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSDictionary *params = @{@"event_id":eventID,@"user_id":userID,@"post":content , @"check_in":[NSNumber numberWithBool:flag]};
    
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSString *responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSData *dataStream = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
        id json = [NSJSONSerialization JSONObjectWithData:dataStream options:0 error:nil];
        NSDictionary *responseDic = (NSDictionary *) json ;
        NSString *status = [NSString stringWithFormat:@"%@",[responseDic objectForKey:@"status"]];
        NSError *error ;
        if ([status isEqualToString:@"true"]) {
            NSDictionary *post_data_dic = [responseDic objectForKey:@"post_data"];
            PostModel *postObj = [[PostModel alloc]initWithDictionary:post_data_dic error:&error];
            [self.delegate postSuccess:postObj];
        } else {
            NSString *error = [responseDic objectForKey:@"message"];
            [self.delegate requestFailWithError:error];
        }
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        [self.delegate requestFailWithError:requestFailed];
    }];
}

-(void)removeSessionFromAgenda:(NSNumber *)eventID sessionID:(NSNumber *)sessionID userID:(NSNumber *)userID  {
    
    NSString *url = @"http://ntam.tech/istanbul_event_service/remove_from_my_agenda.php";
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSDictionary *params = @{@"event_id":eventID,@"user_id":userID,@"session_id":sessionID};
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSString *responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSData *dataStream = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
        id json = [NSJSONSerialization JSONObjectWithData:dataStream options:0 error:nil];
        NSDictionary *responseDic = (NSDictionary *) json ;
        NSString *status = [NSString stringWithFormat:@"%@",[responseDic objectForKey:@"status"]];
        if ([status isEqualToString:@"true"]) {
            [self.delegate sessionAddedSuccessfully];
        } else {
            NSString *error = [responseDic objectForKey:@"message"];
            [self.delegate requestFailWithError:error];
        }
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        [self.delegate requestFailWithError:requestFailed];
    }];
}

-(void)addSessionToAgenda:(NSNumber *)eventID sessionID:(NSNumber *)sessionID userID:(NSNumber *)userID  {
    
    NSString *url = [NSString stringWithFormat:@"%@%@",BASE_URL,[URL getAddSessionToAgenda]];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSDictionary *params = @{@"event_id":eventID,@"user_id":userID,@"session_id":sessionID};
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSString *responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSData *dataStream = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
        id json = [NSJSONSerialization JSONObjectWithData:dataStream options:0 error:nil];
        NSDictionary *responseDic = (NSDictionary *) json ;
        NSString *status = [NSString stringWithFormat:@"%@",[responseDic objectForKey:@"status"]];
        if ([status isEqualToString:@"true"]) {
            [self.delegate sessionAddedSuccessfully];
        } else {
            NSString *error = [responseDic objectForKey:@"message"];
            [self.delegate requestFailWithError:error];
        }
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        [self.delegate requestFailWithError:requestFailed];
    }];
}

-(void)addSessionRate:(NSNumber *)rate sessionID:(NSNumber *)sessionID userID:(NSNumber *)userID {
    NSString *url = [NSString stringWithFormat:@"%@%@",BASE_URL,[URL getAddSessionRateURL]];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSDictionary *params = @{@"rate":rate,@"user_id":userID,@"session_id":sessionID};
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSString *responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSData *dataStream = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
        id json = [NSJSONSerialization JSONObjectWithData:dataStream options:0 error:nil];
        NSDictionary *responseDic = (NSDictionary *) json ;
        NSString *status = [NSString stringWithFormat:@"%@",[responseDic objectForKey:@"status"]];
        if ([status isEqualToString:@"true"]) {
            [self.delegate sessionRatingSuccessfully:rate];
        } else {
            NSString *error = [responseDic objectForKey:@"message"];
            [self.delegate requestFailWithError:error];
        }
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        [self.delegate requestFailWithError:requestFailed];
    }];
}

-(void)loadAllPolls:(NSNumber *)eventID userID:(NSNumber *)userID{
    NSString *url = [NSString stringWithFormat:@"%@%@",BASE_URL,[URL getPollsURL]];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSDictionary *params = @{@"event_id":eventID,@"user_id":userID};
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSString *responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSData *dataStream = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
        id json = [NSJSONSerialization JSONObjectWithData:dataStream options:0 error:nil];
        NSDictionary *responseDic = (NSDictionary *) json ;
        NSString *status = [NSString stringWithFormat:@"%@",[responseDic objectForKey:@"status"]];
        if ([status isEqualToString:@"true"]) {
            NSError *err ;
            NSMutableArray *pollsArr = [[NSMutableArray alloc]init];
            NSArray *allPollsJSON = (NSArray *)[responseDic objectForKey:@"all_polls"];
            for (NSDictionary *pollDic in allPollsJSON) {
                PollModel *pollObj = [[PollModel alloc]initWithDictionary:pollDic error:&err];
                [pollsArr addObject:pollObj];
            }
            [self.delegate getAllPolls:pollsArr];
        } else {
            NSString *error = [responseDic objectForKey:@"massage"];
            [self.delegate requestFailWithError:error];
        }
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        [self.delegate requestFailWithError:requestFailed];
    }];
}

-(void)loadFullAgenda:(NSNumber *)userID eventID:(NSNumber *)eventID {
    NSString *url = [NSString stringWithFormat:@"%@%@",BASE_URL,[URL getFullAgendaURL]];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSDictionary *params = @{@"event_id":eventID,@"user_id":userID};
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSString *responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSData *dataStream = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
        id json = [NSJSONSerialization JSONObjectWithData:dataStream options:0 error:nil];
        NSDictionary *responseDic = (NSDictionary *) json ;
        NSString *status = [NSString stringWithFormat:@"%@",[responseDic objectForKey:@"status"]];
        if ([status isEqualToString:@"true"]) {
            NSError *err ;
            NSMutableArray *agendaArr = [[NSMutableArray alloc]init];
            NSArray *agendaJSON = (NSArray *)[responseDic objectForKey:@"agenda"];
            for (NSDictionary *agendaDic in agendaJSON) {
                AgendaModel *agendaObj = [[AgendaModel alloc]initWithDictionary:agendaDic error:&err];
                [agendaArr addObject:agendaObj];
            }
            [self.delegate getFullAgenda:agendaArr];
        } else {
            NSString *error = [responseDic objectForKey:@"massage"];
            [self.delegate requestFailWithError:error];
        }
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        [self.delegate requestFailWithError:requestFailed];
    }];
}


-(void)loadMyAgenda:(NSNumber *)userID eventID:(NSNumber *)eventID {
    NSString *url = [NSString stringWithFormat:@"%@%@",BASE_URL,[URL getMyAgendaURL]];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSDictionary *params = @{@"event_id":eventID,@"user_id":userID};
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSString *responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSData *dataStream = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
        id json = [NSJSONSerialization JSONObjectWithData:dataStream options:0 error:nil];
        NSDictionary *responseDic = (NSDictionary *) json ;
        NSString *status = [NSString stringWithFormat:@"%@",[responseDic objectForKey:@"status"]];
        if ([status isEqualToString:@"true"]) {
            NSError *err ;
            NSMutableArray *agendaArr = [[NSMutableArray alloc]init];
            NSArray *agendaJSON = (NSArray *)[responseDic objectForKey:@"agenda"];
            for (NSDictionary *agendaDic in agendaJSON) {
                AgendaModel *agendaObj = [[AgendaModel alloc]initWithDictionary:agendaDic error:&err];
                [agendaArr addObject:agendaObj];
            }
            [self.delegate getMyAgenda:agendaArr];
        } else {
            NSString *error = [responseDic objectForKey:@"massage"];
            [self.delegate requestFailWithError:error];
        }
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        [self.delegate requestFailWithError:requestFailed];
    }];
}


-(void)loadAnnouncementTypes:(NSNumber *)eventID {
    NSString *url = [NSString stringWithFormat:@"%@%@",BASE_URL,[URL getAnnouncementTypesURL]];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSDictionary *params = @{@"event_id":eventID};
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSString *responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSData *dataStream = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
        id json = [NSJSONSerialization JSONObjectWithData:dataStream options:0 error:nil];
        NSDictionary *responseDic = (NSDictionary *) json ;
        NSString *status = [NSString stringWithFormat:@"%@",[responseDic objectForKey:@"status"]];
        if ([status isEqualToString:@"true"]) {
            NSError *err ;
            NSMutableArray *announcementTypesArr = [[NSMutableArray alloc]init];
            NSArray *announcementJSON = (NSArray *)[responseDic objectForKey:@"announcement_list"];
            for (NSDictionary *typeDic in announcementJSON) {
                AnnouncementType *announcementObj = [[AnnouncementType alloc]initWithDictionary:typeDic error:&err];
                [announcementTypesArr addObject:announcementObj];
            }
            [self.delegate getAllAnnouncementTypes:announcementTypesArr];
        } else {
            NSString *error = [responseDic objectForKey:@"massage"];
            [self.delegate requestFailWithError:error];
        }
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        [self.delegate requestFailWithError:requestFailed];
    }];
}

-(void)addPoll:(NSNumber *)eventID userID:(NSNumber *)userID question:(NSString *)question answersJson:(NSString *)json {
    
    NSString *url = [NSString stringWithFormat:@"%@%@",BASE_URL,[URL getAddPollURL]];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSDictionary *params = @{@"event_id":eventID,@"user_id":userID,@"question":question,@"add_poll":json};
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSString *responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSData *dataStream = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
        id json = [NSJSONSerialization JSONObjectWithData:dataStream options:0 error:nil];
        NSDictionary *responseDic = (NSDictionary *) json ;
        NSString *status = [NSString stringWithFormat:@"%@",[responseDic objectForKey:@"status"]];
        if ([status isEqualToString:@"true"]) {
            NSError *err ;
            NSDictionary *postDic = [responseDic objectForKey:@"post_data"];
            PostModel *postObj = [[PostModel alloc]init];
            postObj.id = [postDic objectForKey:@"id"];
            postObj.likes = @"0";
            postObj.make_like = @"0";
            PollModel *pollObj = [[PollModel alloc]initWithDictionary:[postDic objectForKey:@"poll_data"] error:&err];
            postObj.poll_data = pollObj ;
            SharedData *shared = [SharedData getSharedObject];
            pollObj.user_id = shared.user.id ;
            postObj.post = [postDic objectForKey:@"post"];
            postObj.post_date = [postDic objectForKey:@"post_date"];
            postObj.user_id = [postDic objectForKey:@"user_id"];
            postObj.user_image = [postDic objectForKey:@"user_image"];
            postObj.username = [postDic objectForKey:@"username"];
            [self.delegate pollAddedSuccessfullyWithPost:postObj];
        } else {
            NSString *error = [responseDic objectForKey:@"message"];
            [self.delegate requestFailWithError:error];
        }
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        [self.delegate requestFailWithError:requestFailed];
    }];
}


-(void)sendNotification:(NSNumber *)eventID title:(NSString *)title body:(NSString *)body isDraft:(NSNumber *)isDraft notificationTarget:(NSString *)json {
    
    NSString *url = [NSString stringWithFormat:@"%@%@",BASE_URL,[URL getSendNotificationURL]];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSDictionary *params = @{@"event_id":eventID,@"title":title,@"body":body,@"add_draft":isDraft,@"notification_target":json};
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSString *responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSData *dataStream = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
        id json = [NSJSONSerialization JSONObjectWithData:dataStream options:0 error:nil];
        NSDictionary *responseDic = (NSDictionary *) json ;
        NSString *status = [NSString stringWithFormat:@"%@",[responseDic objectForKey:@"status"]];
        if ([status isEqualToString:@"true"]) {
            [self.delegate notificationSentSuccessfully ];
        } else {
            NSString *error = [responseDic objectForKey:@"message"];
            [self.delegate requestFailWithError:error];
        }
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        [self.delegate requestFailWithError:requestFailed];
    }];
}

-(void)loadAnnouncementList:(NSNumber *)eventID userID:(NSNumber *)userID {
    NSString *url = [NSString stringWithFormat:@"%@%@",BASE_URL,[URL getAnnouncementListURL]];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSDictionary *params = @{@"event_id":eventID,@"user_id":userID};
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSString *responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSData *dataStream = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
        id json = [NSJSONSerialization JSONObjectWithData:dataStream options:0 error:nil];
        NSDictionary *responseDic = (NSDictionary *) json ;
        NSString *status = [NSString stringWithFormat:@"%@",[responseDic objectForKey:@"status"]];
        if ([status isEqualToString:@"true"]) {
            NSError *err ;
            NSMutableArray *announcementTypesArr = [[NSMutableArray alloc]init];
            NSArray *announcementJSON = (NSArray *)[responseDic objectForKey:@"sent"];
            for (NSDictionary *typeDic in announcementJSON) {
                AnnouncementType *announcementObj = [[AnnouncementType alloc]initWithDictionary:typeDic error:&err];
                [announcementTypesArr addObject:announcementObj];
            }
            [self.delegate getAllAnnouncementTypes:announcementTypesArr];
        } else {
            NSString *error = [responseDic objectForKey:@"message"];
            [self.delegate requestFailWithError:requestFailed];
        }
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        [self.delegate requestFailWithError:requestFailed];
    }];
}

-(void)editBio:(NSNumber *)eventID userID:(NSNumber *)userID bio:(NSString *)bioText {
    NSString *url = [NSString stringWithFormat:@"%@%@",BASE_URL,[URL getEditBioURL]];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSDictionary *params = @{@"event_id":eventID,@"user_id":userID, @"bio":bioText};
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSString *responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSData *dataStream = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
        id json = [NSJSONSerialization JSONObjectWithData:dataStream options:0 error:nil];
        NSDictionary *responseDic = (NSDictionary *) json ;
        NSString *status = [NSString stringWithFormat:@"%@",[responseDic objectForKey:@"status"]];
        if ([status isEqualToString:@"true"]) {
            [self.delegate bioEditedSuccessfully];
        } else {
            NSString *error = [responseDic objectForKey:@"message"];
            [self.delegate requestFailWithError:error];
        }
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        [self.delegate requestFailWithError:requestFailed];
    }];
}

-(void)editProfileImage:(NSNumber *)eventID
                 userID:(NSNumber *)userID imageContent:(NSString *)imageStr  {
    NSString *url = [NSString stringWithFormat:@"%@%@",BASE_URL,[URL getEditAccountImageURL]];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSDictionary *params = @{@"event_id":eventID,@"user_id":userID, @"image_content":imageStr };
    
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSString *responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSData *dataStream = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
        id json = [NSJSONSerialization JSONObjectWithData:dataStream options:0 error:nil];
        NSDictionary *responseDic = (NSDictionary *) json ;
        NSString *status = [NSString stringWithFormat:@"%@",[responseDic objectForKey:@"status"]];
        if ([status isEqualToString:@"true"]) {
            NSString *newURL = [responseDic objectForKey:@"image"];
            [self.delegate accountImageEditSuccessfully:newURL];
        } else {
            NSString *error = [responseDic objectForKey:@"message"];
            [self.delegate requestFailWithError:error];
        }
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        [self.delegate requestFailWithError:requestFailed];
    }];
}

-(void)editProfile:(NSNumber *)eventID userID:(NSNumber *)userID
              name:(NSString *)name
          position:(NSString *)position
           company:(NSString *)company
          location:(NSString *)location {
    
    NSString *url = [NSString stringWithFormat:@"%@%@",BASE_URL,[URL getEditProfileURL]];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSDictionary *params = @{@"event_id":eventID,@"user_id":userID, @"name":name , @"location":location ,@"position":position ,@"company":company};
    
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSString *responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSData *dataStream = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
        id json = [NSJSONSerialization JSONObjectWithData:dataStream options:0 error:nil];
        NSDictionary *responseDic = (NSDictionary *) json ;
        NSString *status = [NSString stringWithFormat:@"%@",[responseDic objectForKey:@"status"]];
        if ([status isEqualToString:@"true"]) {
            [self.delegate profileEditedSuccessfully];
        } else {
            NSString *error = [responseDic objectForKey:@"message"];
            [self.delegate requestFailWithError:error];
        }
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        [self.delegate requestFailWithError:requestFailed];
    }];
}

-(void)editSocialLinks:(NSNumber *)eventID userID:(NSNumber *)userID
               fb_link:(NSString *)fb_link
          twitter_link:(NSString *)twitter_link
         linkedin_link:(NSString *)linkedin_link {
    NSString *url = [NSString stringWithFormat:@"%@%@",BASE_URL,[URL getEditSocialLinksURL]];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSDictionary *params = @{@"event_id":eventID,@"user_id":userID, @"fb_link":fb_link , @"twitter_link":twitter_link ,@"linkedin_link":linkedin_link };
    
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSString *responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSData *dataStream = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
        id json = [NSJSONSerialization JSONObjectWithData:dataStream options:0 error:nil];
        NSDictionary *responseDic = (NSDictionary *) json ;
        NSString *status = [NSString stringWithFormat:@"%@",[responseDic objectForKey:@"status"]];
        if ([status isEqualToString:@"true"]) {
            [self.delegate socialLinksEditedSuccessfully];
        } else {
            NSString *error = [responseDic objectForKey:@"message"];
            [self.delegate requestFailWithError:error];
        }
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        [self.delegate requestFailWithError:requestFailed];
    }];
}

-(void)answerPoll:(NSNumber *)userID PollID:(NSNumber *)pID choiceID:(NSNumber *)cID {
    NSString *url = [NSString stringWithFormat:@"%@%@",BASE_URL,[URL getAnswerPollURL]];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSDictionary *params = @{@"user_id":userID,@"poll_question":pID, @"poll_choice":cID};
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSString *responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSData *dataStream = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
        id json = [NSJSONSerialization JSONObjectWithData:dataStream options:0 error:nil];
        NSDictionary *responseDic = (NSDictionary *) json ;
        NSString *status = [NSString stringWithFormat:@"%@",[responseDic objectForKey:@"status"]];
        if ([status isEqualToString:@"true"]) {
            NSArray *choicesArr = (NSArray *)[responseDic objectForKey:@"poll_rate"];
            NSMutableArray *pollChoices = [[NSMutableArray alloc]init];
            NSError *error = nil ;
            for(NSDictionary *choiceDic in choicesArr){
                Choice *choice = [[Choice alloc]initWithDictionary:choiceDic error:&error];
                [pollChoices addObject:choice];
            }
            [self.delegate getPollChoices:pollChoices];
        } else {
            NSString *error = [responseDic objectForKey:@"massage"];
            [self.delegate requestFailWithError:error];
        }
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        [self.delegate requestFailWithError:requestFailed];
    }];
}

-(void)likePost:(NSNumber *)eventID userID:(NSNumber *)userID postID:(NSNumber *)postID {
    
    NSString *url = [NSString stringWithFormat:@"%@%@",BASE_URL,[URL getLikePostURL]];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSDictionary *params = @{@"event_id":eventID,@"user_id":userID, @"post_id":postID};
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSString *responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSData *dataStream = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
        id json = [NSJSONSerialization JSONObjectWithData:dataStream options:0 error:nil];
        NSDictionary *responseDic = (NSDictionary *) json ;
        NSString *status = [NSString stringWithFormat:@"%@",[responseDic objectForKey:@"status"]];
        if ([status isEqualToString:@"true"]) {
            [self.delegate likePostSuccessfully];
        } else {
            NSString *error = [responseDic objectForKey:@"message"];
            [self.delegate requestFailWithError:error];
        }
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        [self.delegate requestFailWithError:requestFailed];
    }];
}

-(void)likeSession:(NSNumber *)userID sessionID:(NSNumber *)sessionID {
    NSString *url = [NSString stringWithFormat:@"%@%@",BASE_URL,[URL getLikeSessionURL]];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSDictionary *params = @{@"user_id":userID, @"session_id":sessionID};
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSString *responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSData *dataStream = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
        id json = [NSJSONSerialization JSONObjectWithData:dataStream options:0 error:nil];
        NSDictionary *responseDic = (NSDictionary *) json ;
        NSString *status = [NSString stringWithFormat:@"%@",[responseDic objectForKey:@"status"]];
        if ([status isEqualToString:@"true"]) {
            [self.delegate likeSessionSuccessfully];
        } else {
            NSString *error = [responseDic objectForKey:@"massage"];
            [self.delegate requestFailWithError:error];
        }
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        [self.delegate requestFailWithError:requestFailed];
    }];
}


-(void)getMessagesBtwMe:(NSNumber *)userID andAnother:(NSNumber *)anotherUser andEventID:(NSNumber *)eventID {
    NSString *url = [NSString stringWithFormat:@"%@%@",BASE_URL,[URL getMessagesBtwMeAndAnotherURL]];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSDictionary *params = @{@"event_id":eventID,@"current_user":userID, @"other_user":anotherUser};
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSString *responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSData *dataStream = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
        id json = [NSJSONSerialization JSONObjectWithData:dataStream options:0 error:nil];
        NSDictionary *responseDic = (NSDictionary *) json ;
        NSString *status = [NSString stringWithFormat:@"%@",[responseDic objectForKey:@"status"]];
        if ([status isEqualToString:@"true"]) {
            NSArray *chatMessagesJSONArr = (NSArray *)[responseDic objectForKey:@"chat_messages"];
            NSMutableArray *chatMessagesArr = [[NSMutableArray alloc]init];
            NSError *error = nil ;
            for(NSDictionary *chatMessagesDic in chatMessagesJSONArr){
                Message *message = [[Message alloc]initWithDictionary:chatMessagesDic error:&error];
                [chatMessagesArr addObject:message];
            }
            [self.delegate showChatMessages:chatMessagesArr];
        } else {
            NSString *error = [responseDic objectForKey:@"massage"];
            [self.delegate requestFailWithError:error];
        }
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        [self.delegate requestFailWithError:requestFailed];
    }];
}


-(void)loadMesssagesCount:(NSNumber *)eventID UserID:(NSNumber *)userID  {
    NSString *url = [NSString stringWithFormat:@"%@%@",BASE_URL,[URL getMessageCountURL]];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSDictionary *params = @{@"event_id":eventID,@"user_id":userID};
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSString *responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSData *dataStream = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
        id json = [NSJSONSerialization JSONObjectWithData:dataStream options:0 error:nil];
        NSDictionary *responseDic = (NSDictionary *) json ;
        NSString *status = [NSString stringWithFormat:@"%@",[responseDic objectForKey:@"status"]];
        if ([status isEqualToString:@"true"]) {
            NSString *unreadMessagesCount = [responseDic objectForKey:@"total_message"];
            [self.delegate getMessagesCounter:unreadMessagesCount.intValue];
        } else {
            NSString *error = [responseDic objectForKey:@"message"];
            [self.delegate requestFailWithError:error];
        }
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        [self.delegate requestFailWithError:requestFailed];
    }];
}

-(void)notifyMessageRead:(NSNumber *)currentUserID andAnotherUser:(NSNumber *)anotherUserID {
    NSString *url = [NSString stringWithFormat:@"%@%@",BASE_URL,[URL getReadMessageURL]];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSDictionary *params = @{@"current_user_id":currentUserID,@"other_user_id":anotherUserID};
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSString *responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSData *dataStream = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
        id json = [NSJSONSerialization JSONObjectWithData:dataStream options:0 error:nil];
        NSDictionary *responseDic = (NSDictionary *) json ;
        NSString *status = [NSString stringWithFormat:@"%@",[responseDic objectForKey:@"status"]];
        if ([status isEqualToString:@"true"]) {
        } else {
            NSString *error = [responseDic objectForKey:@"message"];
//            [self.delegate requestFailWithError:error];
        }
    } failure:^(NSURLSessionTask *operation, NSError *error) {
//        [self.delegate requestFailWithError:requestFailed];
    }];
}


-(void)sendChatMessage:(NSNumber *)eventID andSenderID:(NSNumber *)senderID andReceiverID:(NSNumber *)receiverID andMessage:(NSString *)msg andImage:(NSString *)encodedImage64  {
    
    NSString *url = [NSString stringWithFormat:@"%@%@",BASE_URL,[URL getSendChatMessageURL]];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSDictionary *params = @{@"event_id":eventID,@"sender_id":senderID, @"receiver_id":receiverID, @"message":msg, @"image":encodedImage64};
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSString *responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSData *dataStream = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
        id json = [NSJSONSerialization JSONObjectWithData:dataStream options:0 error:nil];
        NSDictionary *responseDic = (NSDictionary *) json ;
        NSString *status = [NSString stringWithFormat:@"%@",[responseDic objectForKey:@"status"]];
        if ([status isEqualToString:@"true"]) {
            [self.delegate msgSendSuccessfully];
        } else {
            NSString *error = [responseDic objectForKey:@"message"];
            [self.delegate requestFailWithError:error];
        }
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        [self.delegate requestFailWithError:requestFailed];
    }];
}

-(void)loadAllPhotos:(NSNumber *)eventID userID:(NSNumber *)userID  {
    NSString *url = [NSString stringWithFormat:@"%@%@",BASE_URL,[URL getAllPhotosURL]];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSDictionary *params = @{@"event_id":eventID,@"user_id":userID};
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSString *responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSData *dataStream = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
        id json = [NSJSONSerialization JSONObjectWithData:dataStream options:0 error:nil];
        NSDictionary *responseDic = (NSDictionary *) json ;
        NSString *status = [NSString stringWithFormat:@"%@",[responseDic objectForKey:@"status"]];
        if ([status isEqualToString:@"true"]) {
            NSError *err ;
            NSMutableArray *photosArr = [[NSMutableArray alloc]init];
            NSArray *photosArrJSON = (NSArray *)[responseDic objectForKey:@"all_photos"];
            for (NSDictionary *photoDic in photosArrJSON) {
                PhotoModel *photoObj = [[PhotoModel alloc]initWithDictionary:photoDic error:&err];
                [photosArr addObject:photoObj];
            }
            [self.delegate getAllPhotos:photosArr];
        } else {
            NSString *error = [responseDic objectForKey:@"message"];
            [self.delegate requestFailWithError:error];
        }
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        [self.delegate requestFailWithError:requestFailed];
    }];
}

-(void)likePhoto:(NSNumber *)eventID userID:(NSNumber *)userID photoID:(NSNumber *)photoID {
    NSString *url = [NSString stringWithFormat:@"%@%@",BASE_URL,[URL getLikePhotoURL]];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSDictionary *params = @{@"event_id":eventID,@"user_id":userID, @"photo_id":photoID};
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSString *responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSData *dataStream = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
        id json = [NSJSONSerialization JSONObjectWithData:dataStream options:0 error:nil];
        NSDictionary *responseDic = (NSDictionary *) json ;
        NSString *status = [NSString stringWithFormat:@"%@",[responseDic objectForKey:@"status"]];
        if ([status isEqualToString:@"true"]) {
            [self.delegate likePhotoSuccessfully];
        } else {
            NSString *error = [responseDic objectForKey:@"message"];
            [self.delegate requestFailWithError:error];
        }
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        [self.delegate requestFailWithError:requestFailed];
    }];
}

-(void)deletePhoto:(NSNumber *)photoID {
    NSString *url = [NSString stringWithFormat:@"%@%@",BASE_URL,[URL getDeletePhotoURL]];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSDictionary *params = @{@"photo_id":photoID};
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSString *responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSData *dataStream = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
        id json = [NSJSONSerialization JSONObjectWithData:dataStream options:0 error:nil];
        NSDictionary *responseDic = (NSDictionary *) json ;
        NSString *status = [NSString stringWithFormat:@"%@",[responseDic objectForKey:@"status"]];
        if ([status isEqualToString:@"true"]) {
            [self.delegate deletePhotoSuccessfully];
        } else {
            NSString *error = [responseDic objectForKey:@"message"];
            [self.delegate requestFailWithError:error];
        }
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        [self.delegate requestFailWithError:requestFailed];
    }];
}

-(void)addPhoto:(NSNumber *)eventID andUserID:(NSNumber *)userID andImageEncoded:(NSString *)imageStr {
    NSString *url = [NSString stringWithFormat:@"%@%@",BASE_URL,[URL getAddPhotoURL]];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSDictionary *params = @{@"event_id":eventID,@"user_id":userID,@"image":imageStr};
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSString *responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSData *dataStream = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
        id json = [NSJSONSerialization JSONObjectWithData:dataStream options:0 error:nil];
        NSDictionary *responseDic = (NSDictionary *) json ;
        NSString *status = [NSString stringWithFormat:@"%@",[responseDic objectForKey:@"status"]];
        if ([status isEqualToString:@"true"]) {
            NSString *imageURL = [responseDic objectForKey:@"image"];
            NSString *photoID = [responseDic objectForKey:@"photo_id"];
            [self.delegate photaAddedSuccessfullyWith:imageURL andImageID:photoID];
        } else {
            NSString *error = [responseDic objectForKey:@"message"];
            [self.delegate requestFailWithError:error];
        }
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        [self.delegate requestFailWithError:requestFailed];
    }];
}


//comments

-(void)addPhotoComment:(NSNumber *)eventID andUserID:(NSNumber *)userID andComment:(NSString *)comment andPhotoID:(NSNumber *)photoID {
    NSString *url = [NSString stringWithFormat:@"%@%@",BASE_URL,[URL getAddPhotoCommentURL]];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSDictionary *params = @{@"event_id":eventID,@"user_id":userID,@"comment":comment,@"photo_id":photoID};
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
//        NSString *responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
//        NSData *dataStream = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
//        id json = [NSJSONSerialization JSONObjectWithData:dataStream options:0 error:nil];
//        NSDictionary *responseDic = (NSDictionary *) json ;
//        NSString *status = [NSString stringWithFormat:@"%@",[responseDic objectForKey:@"status"]];
//        if ([status isEqualToString:@"true"]) {
//            NSString *imageURL = [responseDic objectForKey:@"image"];
//            NSString *photoID = [responseDic objectForKey:@"photo_id"];
//            [self.delegate photaAddedSuccessfullyWith:imageURL andImageID:photoID];
//        } else {
//            NSString *error = [responseDic objectForKey:@"message"];
//            [self.delegate requestFailWithError:error];
//        }
    } failure:^(NSURLSessionTask *operation, NSError *error) {
//        [self.delegate requestFailWithError:requestFailed];
    }];
}


-(void)addPostComment:(NSNumber *)eventID andUserID:(NSNumber *)userID andComment:(NSString *)comment andPostID:(NSNumber *)postID {
    NSString *url = [NSString stringWithFormat:@"%@%@",BASE_URL,[URL getAddPostCommentURL]];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSDictionary *params = @{@"event_id":eventID,@"user_id":userID,@"comment":comment,@"post_id":postID};
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
//        NSString *responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
//        NSData *dataStream = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
//        id json = [NSJSONSerialization JSONObjectWithData:dataStream options:0 error:nil];
//        NSDictionary *responseDic = (NSDictionary *) json ;
//        NSString *status = [NSString stringWithFormat:@"%@",[responseDic objectForKey:@"status"]];
        //        if ([status isEqualToString:@"true"]) {
        //            NSString *imageURL = [responseDic objectForKey:@"image"];
        //            NSString *photoID = [responseDic objectForKey:@"photo_id"];
        //            [self.delegate photaAddedSuccessfullyWith:imageURL andImageID:photoID];
        //        } else {
        //            NSString *error = [responseDic objectForKey:@"message"];
        //            [self.delegate requestFailWithError:error];
        //        }
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        //        [self.delegate requestFailWithError:requestFailed];
    }];
}


-(void)addSessionComment:(NSNumber *)userID andComment:(NSString *)comment andSessionID:(NSNumber *)sessionID {
    NSString *url = [NSString stringWithFormat:@"%@%@",BASE_URL,[URL getAddSessionCommentURL]];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSDictionary *params = @{@"user_id":userID,@"comment":comment,@"session_id":sessionID};
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
    } failure:^(NSURLSessionTask *operation, NSError *error) {
    }];
}

-(void)deletePollByID:(NSNumber *)pollID {
    NSString *url = [NSString stringWithFormat:@"%@%@",BASE_URL,[URL getDeletePollURL]];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSDictionary *params = @{@"poll_id":pollID};
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSString *responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSData *dataStream = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
        id json = [NSJSONSerialization JSONObjectWithData:dataStream options:0 error:nil];
        NSDictionary *responseDic = (NSDictionary *) json ;
        NSString *status = [NSString stringWithFormat:@"%@",[responseDic objectForKey:@"status"]];
        if ([status isEqualToString:@"true"]) {
        } else {
            NSString *error = [responseDic objectForKey:@"message"];
            [self.delegate requestToDeletePollFailWithError:error];
        }
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        [self.delegate requestToDeletePollFailWithError:requestFailed];
    }];
}

-(void)logoutUser:(NSNumber *)userID  {
    NSString *url = [NSString stringWithFormat:@"%@%@",BASE_URL,[URL getLogoutURL]];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSDictionary *params = @{@"user_id":userID};
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [self.delegate logoutSuccessfully];
    } failure:^(NSURLSessionTask *operation, NSError *error) {
    }];
}

-(void)loadTracksAndVenues:(NSNumber *)eventID {
    NSString *url = [NSString stringWithFormat:@"%@%@",BASE_URL,[URL getAllTracksAndVenuesURL]];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSDictionary *params = @{@"event_id":eventID};
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSString *responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSData *dataStream = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
        id json = [NSJSONSerialization JSONObjectWithData:dataStream options:0 error:nil];
        NSDictionary *responseDic = (NSDictionary *) json ;
        NSString *status = [NSString stringWithFormat:@"%@",[responseDic objectForKey:@"status"]];
        if ([status isEqualToString:@"true"]) {
            NSError *err ;
            NSMutableArray *photosArr = [[NSMutableArray alloc]init];
            SharedData *shared = [SharedData getSharedObject];
            shared.venuesArr = (NSMutableArray *)[responseDic objectForKey:@"venues"];
            shared.tracksArr = (NSMutableArray *)[responseDic objectForKey:@"tracks"];
            [self.delegate loadVenuesAndTracksSuccessfully];
        } else {
            NSString *error = [responseDic objectForKey:@"message"];
            [self.delegate requestFailWithError:error];
        }
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        [self.delegate requestFailWithError:requestFailed];
    }];
}


-(void)searchInAgendaBy:(NSNumber *)eventID andSearchKey:(NSString *)key andUserID:(NSNumber *)userID {
    NSString *url = [NSString stringWithFormat:@"%@%@",BASE_URL,[URL searchInAgendaURL]];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSDictionary *params = @{@"event_id":eventID,@"user_id":userID,@"search_string":key};
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSString *responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSData *dataStream = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
        id json = [NSJSONSerialization JSONObjectWithData:dataStream options:0 error:nil];
        NSDictionary *responseDic = (NSDictionary *) json ;
        NSString *status = [NSString stringWithFormat:@"%@",[responseDic objectForKey:@"status"]];
        if ([status isEqualToString:@"true"]) {
            NSError *err ;
            NSMutableArray *agendaArr = [[NSMutableArray alloc]init];
            NSArray *agendaJSON = (NSArray *)[responseDic objectForKey:@"agenda"];
            for (NSDictionary *agendaDic in agendaJSON) {
                AgendaModel *agendaObj = [[AgendaModel alloc]initWithDictionary:agendaDic error:&err];
                [agendaArr addObject:agendaObj];
            }
            [self.delegate getFullAgenda:agendaArr];
        } else {
            NSString *error = [responseDic objectForKey:@"massage"];
            [self.delegate requestFailWithError:error];
        }
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        [self.delegate requestFailWithError:requestFailed];
    }];
}


// Live Voting

-(void)answerQuestionWithUserID:(NSNumber *)userID questionID:(NSNumber *)qID answerID:(NSNumber *)answerID  {
    NSString *url = @"http://ntam.tech/istanbul_event_service/answer_question.php";
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSDictionary *params = @{@"question_id":qID,@"user_id":userID,@"answer_id":answerID};
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSString *responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSData *dataStream = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
        id json = [NSJSONSerialization JSONObjectWithData:dataStream options:0 error:nil];
        NSDictionary *responseDic = (NSDictionary *) json ;
        NSString *status = [NSString stringWithFormat:@"%@",[responseDic objectForKey:@"status"]];
        
        if ([status isEqualToString:@"true"]) {
            [self.delegate questionAnsweredSuccessfully];
        } else {
            NSString *error = [responseDic objectForKey:@"massage"];
            [self.delegate requestFailWithError:error];
        }
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        [self.delegate requestFailWithError:requestFailed];
    }];
}

-(void)loadQuestion:(NSNumber *)questionID {
    NSString *url = [NSString stringWithFormat:@"http://ntam.tech/istanbul_event_service/get_question.php?question_id=%@",questionID];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager POST:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSString *responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSData *dataStream = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
        id json = [NSJSONSerialization JSONObjectWithData:dataStream options:0 error:nil];
        NSDictionary *responseDic = (NSDictionary *) json ;
        NSString *status = [NSString stringWithFormat:@"%@",[responseDic objectForKey:@"status"]];
        
        if ([status isEqualToString:@"true"]) {
            NSDictionary *questionDic = (NSDictionary *)[responseDic objectForKey:@"data"];
            Question *questionObj = [[Question alloc]init];
            questionObj.body = [questionDic objectForKey:@"body"];
            questionObj.id = [questionDic objectForKey:@"id"];
            questionObj.answers = [[NSMutableArray alloc]init];
            NSArray *answersJSONArr = [questionDic valueForKey:@"answers"];
            for (NSDictionary *answerDic in answersJSONArr) {
                Answer *answer = [[Answer alloc]init];
                answer.id = [answerDic objectForKey:@"id"];
                answer.body = [answerDic  objectForKey:@"body"] ;
                [questionObj.answers addObject:answer];
            }
            [self.delegate getQuestionData:questionObj];
        } else {
            NSString *error = [responseDic objectForKey:@"massage"];
            [self.delegate requestFailWithError:error];
        }
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        [self.delegate requestFailWithError:requestFailed];
    }];
}


@end
