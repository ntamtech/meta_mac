//
//  CreatePollViewController.m
//  Events
//
//  Created by M.I.Kamashany on 2/23/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import "CreatePollViewController.h"
#import "SideBarViewController.h"
#import "UITextView+Placeholder.h"
#import "DAKeyboardControl.h"
#import "NewPollTableViewCell.h"
#import "ServiceHandler.h"
#import "Helper.h"
#import "UIViewController+Alert.h"
#import "MBProgressHUD.h"
#import "SharedData.h"
#import "IQKeyboardManager.h"

@interface CreatePollViewController ()<UITextFieldDelegate,NewPollTableViewCellDelegate,UITableViewDataSource,ServiceHandlerDelegate>
{
    UITextField *textField ;
    NSMutableArray *answers ;
    ServiceHandler *service ;
    SharedData *shared ;
}

@property (weak, nonatomic) IBOutlet UITextView *askQuestionTV;
@property (nonatomic,weak) IBOutlet UITableView *answersTableView ;

@end

@implementation CreatePollViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    answers = [[NSMutableArray alloc]init];
    shared = [SharedData getSharedObject];
    service = [[ServiceHandler alloc]init];
    service.delegate = self ;
    [self setupViews];
    [self setupKeyboard];
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:NO];
}

-(void)viewWillAppear:(BOOL)animated {
    _answersTableView.hidden = YES ;
}

-(UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent ;
}

-(IBAction)showMenu:(id)sender {
    SideBarViewController *destination = (SideBarViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"SideBarViewController"];
    [self.navigationController pushViewController:destination animated:YES];
}

-(IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


-(void)setupViews {
    NSDictionary *attrs = @ {
    NSFontAttributeName: [UIFont systemFontOfSize:13],
    };
    self.askQuestionTV.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Ask a question..." attributes:attrs];
    self.askQuestionTV.placeholderColor = [UIColor lightGrayColor];
    
    _answersTableView.estimatedRowHeight = 66 ;
    _answersTableView.rowHeight = UITableViewAutomaticDimension ;
    
}

-(void)setupKeyboard {
    UIToolbar *toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0.0f,
                                                                     self.view.bounds.size.height - 40.0f,
                                                                     self.view.bounds.size.width,
                                                                     40.0f)];
    toolBar.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleWidth;
    [self.view addSubview:toolBar];
    
    textField = [[UITextField alloc] initWithFrame:CGRectMake(10.0f,
                                                                           6.0f,
                                                                           toolBar.bounds.size.width - 20.0f - 68.0f,
                                                                           30.0f)];
    textField.borderStyle = UITextBorderStyleRoundedRect;
    textField.returnKeyType = UIReturnKeyDone ;
    textField.delegate = self ;
    textField.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    [toolBar addSubview:textField];
    
    UIButton *sendButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    sendButton.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
    [sendButton setTitle:@"Add" forState:UIControlStateNormal];
    [sendButton addTarget:self action:@selector(addQuestion:) forControlEvents:UIControlEventTouchDown];
    sendButton.frame = CGRectMake(toolBar.bounds.size.width - 68.0f,
                                  6.0f,
                                  58.0f,
                                  29.0f);
    [toolBar addSubview:sendButton];
    
    
    self.view.keyboardTriggerOffset = toolBar.bounds.size.height;

    [self.view addKeyboardPanningWithFrameBasedActionHandler:^(CGRect keyboardFrameInView, BOOL opening, BOOL closing) {
        /*
         Try not to call "self" inside this block (retain cycle).
         But if you do, make sure to remove DAKeyboardControl
         when you are done with the view controller by calling:
         [self.view removeKeyboardControl];
         */
        
        CGRect toolBarFrame = toolBar.frame;
        toolBarFrame.origin.y = keyboardFrameInView.origin.y - toolBarFrame.size.height;
        toolBar.frame = toolBarFrame;
    } constraintBasedActionHandler:nil];
}

-(IBAction)addQuestion:(id)sender {
    if (![textField.text isEqualToString:@""]) {
        [answers insertObject:@{@"choice":textField.text} atIndex:0];
        _answersTableView.hidden = NO ;
        textField.text = @"" ;
        [_answersTableView reloadData];
    }
}


#pragma mark - UITableView Delegate & DataSource


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return answers.count  ;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellID = @"newPollCell";
    NewPollTableViewCell *cell = (NewPollTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell == nil) {
        cell =[[NewPollTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    cell.delegate = self ;
    cell.questionLabel.text = [[answers objectAtIndex:indexPath.row] objectForKey:@"choice"];
    cell.cellIndexPath = indexPath ;
    tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    return cell ;
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    if(textField.returnKeyType == UIReturnKeyDone) {
        [textField resignFirstResponder];
    }
    return YES;
}

#pragma mark - NewPollTableViewCellDelegate

-(void)deletePollByIndexPath:(NSIndexPath *)index {
   [self deleteRowAtTableViewAtIndex:index andCompletionHandler:^{
       [self.answersTableView reloadData];
       if (answers.count == 0) {
           _answersTableView.hidden = YES ;
       }
   }];
}

-(void)deleteRowAtTableViewAtIndex:(NSIndexPath *)indexPath andCompletionHandler:(void (^)(void))completionHandler {
    [answers removeObjectAtIndex:indexPath.row];
    [self.answersTableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    completionHandler();
}

#pragma mark - actions 

-(IBAction)addPoll:(id)sender {
    if([_askQuestionTV.text isEqualToString:@""]){
        [self showAlert:@"Write Poll Question first"];
    }else{
        if(answers.count == 0){
            [self showAlert:@"Enter Poll Choices first"];
        }else{
            if (![Helper checkInternet]) {
                [self showAlert:@"No Internet Connection !"];
            } else {
                NSError *error = nil ;
                NSDictionary *dic = @{@"choices":answers};
                NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dic
                                                                   options:NSJSONWritingPrettyPrinted error:&error];
                NSString  *answersJSON = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
                
                [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                [service addPoll:[NSNumber numberWithInt:1] userID:[NSNumber numberWithInt:[shared.user.id intValue]] question:_askQuestionTV.text answersJson:answersJSON];
            }
        }
    }
}

-(void)pollAddedSuccessfullyWithPost:(PostModel *)post  {
    [shared.postsArr insertObject:post atIndex:0];
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)requestFailWithError:(NSString *)err {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [self showAlert:err];
}
@end
