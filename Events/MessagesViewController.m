//
//  MessagesViewController.m
//  Events
//
//  Created by M.I.Kamashany on 2/12/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import "MessagesViewController.h"
#import "MessageTableViewCell.h"
#import "ServiceHandler.h"
#import "Helper.h"
#import "UIViewController+Alert.h"
#import "SharedData.h"
#import "MBProgressHUD.h"
#import "Message.h"
#import "UIImageView+WebCache.h"
#import "ChattingViewController.h"

@interface MessagesViewController ()<ServiceHandlerDelegate,UITableViewDelegate,UITableViewDataSource>
{
    ServiceHandler *service ;
    SharedData *shared ;
    NSMutableArray *allMessagesArr ;
    UIButton *refreshBtn ;
}

@property (nonatomic,weak) IBOutlet UITableView *messagesTableView ;
@property (nonatomic,weak) IBOutlet UILabel *noMessagesLabel ;

@end

@implementation MessagesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    service = [[ServiceHandler alloc]init];
    service.delegate = self ;
    shared = [SharedData getSharedObject];
    allMessagesArr = [[NSMutableArray alloc]init];
    _messagesTableView.hidden = YES ;
    _noMessagesLabel.hidden = YES ;
}

-(UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent ;
}

-(void)viewWillAppear:(BOOL)animated {
    if (allMessagesArr.count == 0) {
        if (![Helper checkInternet]) {
            NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:@"allMessages"];
            if (data != nil) {
                shared.allMessages = [NSKeyedUnarchiver unarchiveObjectWithData:data];
                allMessagesArr = shared.allMessages ;
                if (allMessagesArr.count > 0) {
                    _messagesTableView.hidden = NO ;
                }
            }
        }else{
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            [service loadAllMessages:[NSNumber numberWithInt:[shared.user.id intValue]] eventID:[NSNumber numberWithInt:1]];
        }
    } else {
        allMessagesArr = shared.allMessages ;
        _noMessagesLabel.hidden = YES ;
        _messagesTableView.hidden = NO ;
        [_messagesTableView reloadData];
    }
    
}

#pragma mark - ServiceHandlerDelegate 

-(void)getAllMessages:(NSMutableArray *)messages {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    if (messages.count > 0) {
        [self hideTableView:NO];
        _noMessagesLabel.hidden = YES ;
        shared.allMessages = messages ;
        [allMessagesArr addObjectsFromArray:messages] ;
        [_messagesTableView reloadData];
        
    } else {
        [self hideTableView:YES];
        _noMessagesLabel.hidden = NO ;
        [_messagesTableView reloadData];
    }
}

-(void)requestFailWithError:(NSString *)err {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [self hideTableView:YES];
    _noMessagesLabel.hidden = YES ;
    [self createRefreshBtn:err enabled:NO];
}
#pragma mark -TableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return allMessagesArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellID = @"messageCell";
    MessageTableViewCell *cell = (MessageTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell == nil) {
        cell =[[MessageTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    Message *selectedMessage = [allMessagesArr objectAtIndex:indexPath.row];
    if ([selectedMessage.unread_meassage isEqualToString:@"0"]) {
        cell.unreadMessagesCountLabel.hidden = YES ;
    }else{
        cell.unreadMessagesCountLabel.hidden = NO ;
        cell.unreadMessagesCountLabel.text = selectedMessage.unread_meassage ;
        cell.unreadMessagesCountLabel.layer.masksToBounds = YES ;
        cell.unreadMessagesCountLabel.layer.cornerRadius = 5 ;
    }
    [cell.messageIcon setShowActivityIndicatorView:YES];
    [cell.messageIcon setIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [cell.messageIcon sd_setImageWithURL:[NSURL URLWithString:selectedMessage.image] placeholderImage:[UIImage imageNamed:@"profile-image"]];
    cell.NameLabel.text = selectedMessage.name ;
    cell.messageLabel.text = selectedMessage.message ;
    NSString *time = [[selectedMessage.message_time componentsSeparatedByString:@" "] objectAtIndex:1];
    cell.messageDateLabel.text = [NSString stringWithFormat:@"%@ %@",[[selectedMessage.message_time componentsSeparatedByString:@" "] objectAtIndex:0],[Helper detectPMAM:time]];
    return cell ;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    Message *selectedMessage = [allMessagesArr objectAtIndex:indexPath.row];
    ChattingViewController *destination = (ChattingViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"ChattingViewController"];
    destination.anotherUserID = selectedMessage.id ;
    destination.anotherUserImageURL = selectedMessage.image ;
    destination.messagesArr = selectedMessage.message_details ;
    destination.selectedMsgNum = indexPath.row ;
    if ([selectedMessage.unread_meassage isEqualToString:@"0"]) {
        destination.isThereNewMessages = NO;
    }else{
        destination.isThereNewMessages = YES;
    }
    [self.navigationController pushViewController:destination animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 101 ;
}


-(IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)createRefreshBtn:(NSString *)title enabled:(BOOL)enabled{
    refreshBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [refreshBtn setTitle:title forState:UIControlStateNormal];
    refreshBtn.enabled = enabled ;
//    refreshBtn.titleLabel.textColor = [UIColor darkGrayColor];
    refreshBtn.center = self.view.center ;
    [refreshBtn addTarget:self
                   action:@selector(refresh:)
         forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:refreshBtn];
    [self.view bringSubviewToFront:refreshBtn];
}

-(IBAction)refresh:(id)sender {
    if ([Helper checkInternet]) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [refreshBtn removeFromSuperview];
        [service loadAllMessages:[NSNumber numberWithInt:[shared.user.id intValue]] eventID:[NSNumber numberWithInt:1]];
    }
}

-(void)hideTableView:(BOOL)isHidden {
    _messagesTableView.hidden = isHidden ;
}



//- (IBAction)composeMessage:(id)sender {
//    
//}

@end
