//
//  AnnouncementTypeTableViewCell.h
//  Events
//
//  Created by M.I.Kamashany on 3/7/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AnnouncementTypeTableViewCell : UITableViewCell

@property (nonatomic,weak) IBOutlet UIImageView *checkBoxImageView ;
@property (nonatomic,weak) IBOutlet UILabel *typeLabel ;
@property (nonatomic,assign) BOOL isChecked ;


@end
