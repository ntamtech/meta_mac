//
//  WritePostViewController.m
//  Events
//
//  Created by M.I.Kamashany on 2/28/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import "WritePostViewController.h"
#import "UITextView+Placeholder.h"
#import "Helper.h"
#import "UIView+Toast.h"
//#import <UITextView+Placeholder/UITextView+Placeholder.h>

@interface WritePostViewController ()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end

@implementation WritePostViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.postBtn.layer.cornerRadius = 5.0 ;
    self.postBtn.layer.masksToBounds = YES;
}

-(void)viewWillAppear:(BOOL)animated {
    if (self.isWritePost) {
        self.titleLabel.text = @"Write Post" ;
        self.contentTV.placeholder = @"Write post body..." ;
        [self.postBtn setTitle:@"Post" forState:UIControlStateNormal];
    } else {
        self.titleLabel.text = @"Write Comment" ;
        self.navigationItem.title = @"Write Comment";
        self.contentTV.placeholder = @"Write your comment..." ;
        [self.postBtn setTitle:@"Comment" forState:UIControlStateNormal];
    }
    self.contentTV.placeholderColor = [UIColor lightGrayColor];
    self.contentTV.layer.cornerRadius = 8 ;
    self.contentTV.layer.masksToBounds = YES ;
    self.contentTV.layer.borderWidth = 1 ;
    self.contentTV.layer.borderColor = [UIColor darkGrayColor].CGColor;
}


- (IBAction)Post:(id)sender {
    if (![Helper checkInternet]) {
        UIWindow* window = [[UIApplication sharedApplication] keyWindow];
        [window makeToast:@"No Internet Connection!"
                    duration:1
                    position:CSToastPositionBottom];
    }else{
         if (self.isWritePost) {
             if ([self.contentTV.text isEqualToString:@""]) {
                 [self.view makeToast:@"Please write post content."
                             duration:1
                             position:CSToastPositionBottom];
             } else {
                 [self dismissViewControllerAnimated:YES completion:nil];
                 [self.delegate dismissWithPostDetails:self.contentTV.text];
             }
         }else{
             if ([self.contentTV.text isEqualToString:@""]) {
                 [self.view makeToast:@"Please write comment content."
                             duration:1
                             position:CSToastPositionBottom];
             } else {
                 [self dismissViewControllerAnimated:YES completion:nil];
                 [self.delegate dismissWithCommentDetails:self.contentTV.text andIndexPath:self.selectedIndex];
             }
         }
    }
}

- (IBAction)cancel:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
