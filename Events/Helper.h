//
//  Helper.h
//  iJobs
//
//  Created by M.I.Kamashany on 12/11/16.
//  Copyright © 2016 NTAMNTAMTech. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Reachability.h"

@interface Helper : NSObject

+(BOOL) checkInternet ;
+(BOOL)validateEmail:(NSString *)emailStr ;
+(NSString *)detectPMAM:(NSString *)time ;

@end
