//
//  TwitterViewController.m
//  Events
//
//  Created by M.I.Kamashany on 2/23/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//


#import "TwitterViewController.h"
#import "Helper.h"
#import "MBProgressHUD.h"
#import "SharedData.h"
#import "Constants.h"


@interface TwitterViewController ()<UIWebViewDelegate>
{
    SharedData *shared ;
}
@property (weak, nonatomic) IBOutlet UIWebView *tagWebView;
@property (weak, nonatomic) IBOutlet UILabel *errorLabel;

@end

@implementation TwitterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    shared = [SharedData getSharedObject];
}


-(void)viewWillAppear:(BOOL)animated {
    if (![Helper checkInternet]) {
        self.tagWebView.hidden = YES ;
        self.errorLabel.hidden = NO ;
    }else{
        self.errorLabel.hidden = YES ;
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        NSString *searchKey = [shared.about.event_tag stringByReplacingOccurrencesOfString:@" " withString:@""];
        NSString *fullURL = [NSString stringWithFormat:@"%@%@%@",TWITTER1,searchKey,TWITTER2];
        [self.tagWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:fullURL]]];
    }
}


- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    self.tagWebView.hidden = YES ;
    self.errorLabel.hidden = NO ;
}

-(IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent ;
}
@end
