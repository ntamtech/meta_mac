//
//  SideBarViewController.m
//  Events
//
//  Created by M.I.Kamashany on 2/9/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import "SideBarViewController.h"
#import "SideBarCollectionViewCell.h"
#import "AboutViewController.h"
#import "SettingsViewController.h"
#import "SpeakersViewController.h"
#import "MessagesViewController.h"
#import "LogisticsViewController.h"
#import "AdminViewController.h"
#import "PollsViewController.h"
#import "TwitterViewController.h"
#import "AttendeesViewController.h"
#import "AgendaControllerViewController.h"
#import "AnnouncementListViewController.h"
#import "PhotosViewController.h"
#import "NewsFeedsViewController.h"
#import "SharedData.h"
#import "ServiceHandler.h"
#import "Helper.h"
#import "LoginViewController.h"
#import <FirebaseDatabase/FirebaseDatabase.h>
#import "Answer.h"
#import "Constants.h"
#import "WebViewViewController.h"
#import "ProfileViewController.h"

@interface SideBarViewController ()<ServiceHandlerDelegate>
{
    NSArray *collectionIcons ;
    NSArray *collectionTitles ;
    SharedData *sharedData ;
    ServiceHandler *service ;
}

@property (nonatomic,weak) IBOutlet UICollectionView *sideBarCollectionView ;

@end

@implementation SideBarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    collectionIcons = @[@"vector-smart-object11",
                        @"notebook",
                        @"announcements",
                        @"Menu-live-vote-icon-EC",
                        @"vector-smart-object5",
                        @"envelope",
                        @"GeneralInfo-ic",
                        @"LeaderShipPrinciples-ic",
                        @"FerringPhilosophy-ic",
                        @"Dinner-ic",
                        @"profileMenu-ic",
                        @"vector-smart-object8"];
    collectionTitles = @[@"Attendees",@"Agenda",@"Announcements",@"Voting",@"Photos",@"Messages",@"General Information",@"Leadership Principal",@"Ferring Philosophy",@"Dinners",@"Profile",@"Admin"];
    sharedData = [SharedData getSharedObject];
    service = [[ServiceHandler alloc]init];
    service.delegate = self;
}

-(void)viewWillAppear:(BOOL)animated{
    if ([Helper checkInternet]) {
        [service loadMesssagesCount:[NSNumber numberWithInt:1] UserID:[NSNumber numberWithInt:[sharedData.user.id intValue]]];
    }
    [_sideBarCollectionView reloadData];
}

#pragma mark- ServiceHandlerDelegate

-(void)getMessagesCounter:(int)count {
    [_sideBarCollectionView reloadData];
    sharedData.unreadMessagesCount = [NSNumber numberWithInt:count];
}

-(void)requestFailWithError:(NSString *)err {
    if([err isEqualToString:@"User Not exist"]){
        [SharedData deleteAllCachedData];
        if ([Helper checkInternet]) {
            [service logoutUser:[NSNumber numberWithInt:[sharedData.user.id intValue]]];
        }
        LoginViewController *destination = (LoginViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
        [self.navigationController pushViewController:destination animated:YES];
    }
}

-(UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent ;
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 12;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *reuseIdentifier = @"sideBarCell";
    SideBarCollectionViewCell *cell = (SideBarCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    cell.sideBarIcon.image = [UIImage imageNamed:[collectionIcons objectAtIndex:indexPath.row]];
    cell.sideBarIcon.tintColor = [UIColor whiteColor];
    cell.sideBarTitle.text = [collectionTitles objectAtIndex:indexPath.row];
    if (indexPath.row == 5) {
        if (sharedData.unreadMessagesCount != nil) {
            if (sharedData.unreadMessagesCount.intValue > 0) {
                cell.messageCountLabel.hidden = NO ;
                cell.messageCountLabel.text = [NSString stringWithFormat:@"%@",sharedData.unreadMessagesCount];
                cell.messageCountLabel.layer.masksToBounds = YES ;
                cell.messageCountLabel.layer.cornerRadius = 5;
            }else{
                cell.messageCountLabel.hidden = YES ;
            }
        } else {
            cell.messageCountLabel.hidden = YES ;
        }
        
    }else{
        cell.messageCountLabel.hidden = YES ;
    }
    if (indexPath.row == 11) {
        if ([sharedData.user.admin isEqualToString:@"0"]) {
            cell.hidden = YES;
        } else {
            cell.hidden = NO;
        }
    }else{
        cell.hidden = NO;
    }
    
//    if(IS_IPHONE_5){
//        cell.rightConstraint.constant = -10 ;
//        cell.leftConstraint.constant = 10 ;
//        cell.bottomConstraint.constant = -10 ;
//        cell.topConstraint.constant = 10 ;
//        [self.view layoutIfNeeded];
//    }else{
//        cell.rightConstraint.constant = -20 ;
//        cell.leftConstraint.constant = 20 ;
//        cell.bottomConstraint.constant = -20 ;
//        cell.topConstraint.constant = 20 ;
//        [cell layoutIfNeeded];
//    }
//    cell.permissionImageView.image = [UIImage imageNamed:@"circle"];
//    cell.permissionIconImageView.image = [UIImage imageNamed:[icons objectAtIndex:indexPath.row]];
//    cell.permissionTitleLabel.text = [titles objectAtIndex:indexPath.row] ;
//    cell.layer.borderColor = [UIColor clearColor].CGColor;
    //    cell.numOfJobsLabel.text = [NSString stringWithFormat:@"%@ وظيفة متاحة",cat.];
    return cell;
}


#pragma mark <UICollectionViewDelegate>

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat screenWidth = self.sideBarCollectionView.bounds.size.width;
    //    if(IS_IPHONE_5){
    float cellWidth = screenWidth /3.0 -10; //Replace the divisor with the column count requirement. Make sure to have it in float.
    CGSize size = CGSizeMake(cellWidth, cellWidth);
    return size;
    //    }else{
    //        float cellWidth = screenWidth /2.0 - 60; //Replace the divisor with the column count requirement. Make sure to have it in float.
    //        CGSize size = CGSizeMake(cellWidth, cellWidth);
    //        return size;
    //    }
}

//- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
//    return UIEdgeInsetsMake(0,25, 0, 0);
//}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    UIViewController *destination ;
    switch (indexPath.row) {
        case 0:
            destination = (AttendeesViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"AttendeesViewController"];
            [self.navigationController pushViewController:destination animated:YES];
            break;
        case 1:{
                AgendaControllerViewController *AgendaDestination = (AgendaControllerViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"AgendaControllerViewController"];
                [self.navigationController pushViewController:AgendaDestination animated:YES];
            }
            break;
        case 2:
            destination = (AnnouncementListViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"AnnouncementListViewController"];
            [self.navigationController pushViewController:destination animated:YES];
            break;
        case 3:
            destination = (PollsViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"PollsViewController"];
            [self.navigationController pushViewController:destination animated:YES];
            break;
        case 4:
            destination = (PhotosViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"PhotosViewController"];
            [self.navigationController pushViewController:destination animated:YES];
            break;
        case 5:
            destination = (MessagesViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"MessagesViewController"];
            [self.navigationController pushViewController:destination animated:YES];
            break;
        case 6:{
                WebViewViewController *destination = (WebViewViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"WebViewViewController"];
                destination.selectedType = 0 ;
                    [self.navigationController pushViewController:destination animated:YES];
            }
            break;
        case 7:{
                WebViewViewController *destination = (WebViewViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"WebViewViewController"];
                destination.selectedType = 1 ;
                [self.navigationController pushViewController:destination animated:YES];
            }
            break;
        case 8:{
                WebViewViewController *destination = (WebViewViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"WebViewViewController"];
                destination.selectedType = 2 ;
                [self.navigationController pushViewController:destination animated:YES];
            }
            break;
        case 9:{
                WebViewViewController *destination = (WebViewViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"WebViewViewController"];
                destination.selectedType = 3 ;
                [self.navigationController pushViewController:destination animated:YES];
            }
            break;
        case 10:
            destination = (ProfileViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
            [self.navigationController pushViewController:destination animated:YES];
            break;
        case 11:
            destination = (AdminViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"AdminViewController"];
            [self.navigationController pushViewController:destination animated:YES];
            break;
        default:
            break;
    }
}

-(IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
