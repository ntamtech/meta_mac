//
//  SliderModel.m
//  Events
//
//  Created by M.I.Kamashany on 2/20/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import "SliderModel.h"

@implementation SliderModel


- (void)encodeWithCoder:(NSCoder *)encoder {
    //Encode properties, other class variables, etc
    [encoder encodeObject:self.id forKey:@"id"];
    [encoder encodeObject:self.image forKey:@"image"];
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        //decode properties, other class vars
        self.id= [decoder decodeObjectForKey:@"id"];
        self.image =[decoder decodeObjectForKey:@"image"];
    }
    return self;
}


@end
