//
//  RegisterationViewController.m
//  Events
//
//  Created by M.I.Kamashany on 10/14/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import "RegisterationViewController.h"
#import "SharedData.h"
#import "ServiceHandler.h"
#import "MBProgressHUD.h"
#import "Helper.h"
#import "UIViewController+Alert.h"
#import "NewsFeedsViewController.h"
#import "SideBarViewController.h"

@interface RegisterationViewController ()<ServiceHandlerDelegate,UITextFieldDelegate> {
    ServiceHandler *service ;
    SharedData *shared ;
}

@property (weak, nonatomic) IBOutlet UITextField *hospitalNameTF;
@property (weak, nonatomic) IBOutlet UITextField *emailTF;
@property (weak, nonatomic) IBOutlet UITextField *countryNameTF;
@property (weak, nonatomic) IBOutlet UITextField *mobileTF;
@property (weak, nonatomic) IBOutlet UIButton *addPersonalInfoBtn;
- (IBAction)addPersonalInfo:(id)sender;


@end

@implementation RegisterationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.addPersonalInfoBtn.layer.masksToBounds = YES ;
    self.addPersonalInfoBtn.layer.cornerRadius = 25 ;
    
    service = [[ServiceHandler alloc]init];
    service.delegate = self ;
    shared = [SharedData getSharedObject];
}

- (IBAction)addPersonalInfo:(id)sender {
    if ([Helper checkInternet]) {
        if ([_hospitalNameTF.text isEqualToString:@""]) {
            [self showAlert:@"Enter Hospital name First"];
        }else{
            if ([_countryNameTF.text isEqualToString:@""]) {
                [self showAlert:@"Enter Country First"];
            }else{
                if ([_mobileTF.text isEqualToString:@""]) {
                    [self showAlert:@"Enter Mobile First"];
                }else{
                    if ([_emailTF.text isEqualToString:@""]) {
                        [self showAlert:@"Enter Email First"];
                    }else{
                        if ([Helper validateEmail:_emailTF.text]) {
                            [self.view endEditing:YES];
                            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                            [service completeData:_emailTF.text country:_countryNameTF.text hospital:_hospitalNameTF.text mobile:_mobileTF.text userID:[NSNumber numberWithInt:[shared.user.id intValue]]];
                        }else{
                            [self showAlert:@"Enter Valid Email Please"];
                        }
                        
                    }
                }
            }
        }
    } else {
        [self showAlert:@"No Internet Connection!"];
    }
    
}

-(void)completeDataSuccess {
    shared.user.email = _emailTF.text;
    shared.user.hospitalName = _hospitalNameTF.text;
    shared.user.mobile = _mobileTF.text;
    shared.user.countryName = _countryNameTF.text;
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    SideBarViewController *destination = [self.storyboard instantiateViewControllerWithIdentifier:@"SideBarViewController"];
    [self.navigationController pushViewController:destination animated:YES];
}


-(void)requestFailWithError:(NSString *)err {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [self showAlert:err];
}



@end
