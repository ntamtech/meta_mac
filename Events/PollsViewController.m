//
//  PollsViewController.m
//  Events
//
//  Created by M.I.Kamashany on 2/23/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import "PollsViewController.h"
#import "AnswerViewTableViewCell.h"
#import "Helper.h"
#import "ServiceHandler.h"
#import "PollModel.h"
#import "MBProgressHUD.h"
#import "PollTableViewCell.h"
#import "SharedData.h"
#import "AnswerViewController.h"
#import "UIView+Toast.h"
#import "SharedData.h"
#import "Answer.h"
#import <FirebaseDatabase/FirebaseDatabase.h>
#import "Constants.h"


@interface PollsViewController ()<UITableViewDelegate,UITableViewDataSource,ServiceHandlerDelegate>
{
    SharedData *shared ;
    NSString *selectedAnswer ;
    int selectedAnswerIndex ;
    NSUserDefaults *userDefault ;
    NSString *questionKey ;
    ServiceHandler *service ;
    Question *votedQuestion ;
}

- (IBAction)submit:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *submitBtn ;
@property (weak, nonatomic) IBOutlet UILabel *noActiveQuestionsLabel ;
@property (weak, nonatomic) IBOutlet UILabel *questionTitleLabel ;
@property (weak, nonatomic) IBOutlet UITableView *questionTableView ;

@end

@implementation PollsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // this number is the default which means no selected answer.
    selectedAnswerIndex = 100 ;
    shared = [SharedData getSharedObject];
    self.questionTableView.separatorStyle = UITableViewCellSeparatorStyleNone ;
    userDefault = [NSUserDefaults standardUserDefaults];
    self.questionTableView.hidden = YES;
    self.questionTitleLabel.hidden = YES;
    self.submitBtn.hidden = YES ;
    self.noActiveQuestionsLabel.hidden = YES;
    service = [[ServiceHandler alloc]init];
    service.delegate = self;
    self.questionTableView.estimatedRowHeight = 54.0 ;
    self.questionTableView.rowHeight = UITableViewAutomaticDimension;
}

-(void)viewWillAppear:(BOOL)animated {
    [self getVotedQuestion];
}

-(void)hideAllViews {
    self.questionTableView.hidden = YES;
    self.questionTitleLabel.hidden = YES;
    self.submitBtn.hidden = YES ;
    self.noActiveQuestionsLabel.hidden = NO;
}

-(UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent ;
}

#pragma mark - actions

-(IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - UITableView Delegate & DataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (votedQuestion != nil) {
        return  votedQuestion.answers.count;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellID = @"answerViewCell";
    AnswerViewTableViewCell *cell = (AnswerViewTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell == nil) {
        cell =[[AnswerViewTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    Answer *answerObj = [votedQuestion.answers objectAtIndex:indexPath.row];
    cell.answerLabel.text = answerObj.body;
    if ([selectedAnswer isEqualToString:answerObj.body]){
        cell.iconImageView.image = [UIImage imageNamed:@"Checkbox-10"];
    }else{
       cell.iconImageView.image = [UIImage imageNamed:@"rectangle-10"];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone ;
    cell.backgroundColor = [UIColor clearColor];
    tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    return cell ;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    Answer *answerObj = [votedQuestion.answers objectAtIndex:indexPath.row];
    if (selectedAnswer != nil) {
        if ([selectedAnswer isEqualToString:answerObj.body]){
            selectedAnswer = nil ;
            selectedAnswerIndex = 100 ;
        }else{
            selectedAnswer = answerObj.body ;
            selectedAnswerIndex = (int)indexPath.row ;
        }
    }else{
        selectedAnswer = answerObj.body ;
        selectedAnswerIndex = (int)indexPath.row ;
    }
    [_questionTableView reloadData];
}


-(void)loadQuestionByID:(NSString *)questionID {
    int quetionNumber = [questionID integerValue];
    [service loadQuestion:[NSNumber numberWithInt:quetionNumber]];
}

-(void)getQuestionData:(Question *)question {
    
    votedQuestion = question;
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    self.questionTitleLabel.text = votedQuestion.body;
    self.noActiveQuestionsLabel.hidden = YES;
    self.questionTitleLabel.hidden = NO;
    self.questionTableView.hidden = NO;
    self.submitBtn.hidden = NO;
    [self.questionTableView reloadData];
}

- (IBAction)submit:(id)sender {
    if (selectedAnswerIndex == 100) {
        [self.view makeToast:@"Please select answer first."
                             duration:1.0
                             position:CSToastPositionCenter];
    }else{
        if ([Helper checkInternet]) {
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            Answer *selectedAnswer = [votedQuestion.answers objectAtIndex:selectedAnswerIndex];
            int answerID = [selectedAnswer.id integerValue];
            int questionID = [votedQuestion.id integerValue];
            int userID = [shared.user.id integerValue];
            [service answerQuestionWithUserID:[NSNumber numberWithInt:userID] questionID:[NSNumber numberWithInt:questionID] answerID:[NSNumber numberWithInt:answerID]];
        }else{
            [self.view makeToast:@"No Internet Connection!"
                                 duration:1.0
                                 position:CSToastPositionCenter];
        }
    }
}

-(void)questionAnsweredSuccessfully {
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    self.questionTitleLabel.hidden = YES ;
    self.questionTableView.hidden = YES;
    self.submitBtn.hidden = YES;
    self.noActiveQuestionsLabel.text = @"Thanks for your voting";
    self.noActiveQuestionsLabel.hidden = NO;
    [NSUserDefaults.standardUserDefaults setValue:votedQuestion.id forKey:QUESTION_KEY];
    [self increaseCounterInFirebase];
}

-(void)requestFailWithError:(NSString *)err {
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    [self.view makeToast:err
                duration:1.0
                position:CSToastPositionCenter];
}

-(void) increaseCounterInFirebase {
    FIRDatabaseReference *ref = [[FIRDatabase database] reference];
    FIRDatabaseReference *childrRef = [[ref child:@"answers"] childByAutoId];
    [childrRef setValue:@{@"id":[NSNumber numberWithInt:10]}];
//    [ref setValue:[NSNumber numberWithInt:10] forKey:@"id"];
}
//-(void)addAnswer:(int)selectedAnswer {
//    if (selectedAnswerIndex != 100 ){
//        if ([Helper checkInternet]) {
//            Answer *selectedAnswerObj = shared.activatedQuestion.answers[selectedAnswerIndex];
//            FIRDatabaseReference *ref = [[FIRDatabase database] reference];
//            [[[[[[ref child:@"questions"] child:shared.activatedQuestion.key] child:@"answers"] child:[NSString stringWithFormat:@"%i",selectedAnswerIndex]] child:@"votes"] setValue:[NSNumber numberWithInt:selectedAnswerObj.votes+1]];
//            [[ref child:@"users"] updateChildValues:@{shared.user.id:shared.activatedQuestion.key}];
//            [userDefault setValue:shared.activatedQuestion.key forKey:QUESTION_KEY];
//            [self hideAllViews];
//        }else{
//            [self.view makeToast:@"No Internet Connection!"
//                        duration:1.0
//                        position:CSToastPositionCenter];
//        }
//
//    }else{
//        [self.view makeToast:@"Please select answer first."
//                    duration:1.0
//                    position:CSToastPositionCenter];
//    }
//}



-(void)getVotedQuestion {
    if ([Helper checkInternet]) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        FIRDatabaseReference *ref = [[FIRDatabase database] reference];
        [[ref child:@"current_question_id"] observeEventType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
            NSString *cachedQuestionID = [[NSUserDefaults standardUserDefaults] valueForKey:QUESTION_KEY];
            NSString *newQuestionID = [NSString stringWithFormat:@"%@",snapshot.value];
            if ([cachedQuestionID isEqualToString:newQuestionID]) {
                [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                self.questionTitleLabel.hidden = YES;
                self.questionTableView.hidden = YES;
                self.noActiveQuestionsLabel.hidden = NO;
            } else {
                [MBProgressHUD showHUDAddedTo:self.view animated:YES] ;
                self.noActiveQuestionsLabel.hidden = YES;
                self.questionTableView.hidden = YES;
                [self loadQuestionByID:newQuestionID];
            }
        }];
    }else{
        self.noActiveQuestionsLabel.text = @"No Internet Connection";
    }
}

@end
