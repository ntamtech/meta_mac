//
//  NewsFeedsHeader.h
//  Events
//
//  Created by M.I.Kamashany on 4/12/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KIImagePager.h"

@protocol NewsFeedsHeaderDelegate <NSObject>

@optional

-(void)completeShow:(int)flag ;

@end

@interface NewsFeedsHeader : UIView

@property (weak,nonatomic) id<NewsFeedsHeaderDelegate> delegate ;
@property (weak, nonatomic) IBOutlet KIImagePager *imagePager;

- (IBAction)showTwitterAccount:(id)sender;
- (IBAction)showLinkedINAccount:(id)sender;

@end
