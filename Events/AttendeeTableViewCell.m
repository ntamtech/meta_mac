//
//  AttendeeTableViewCell.m
//  Events
//
//  Created by M.I.Kamashany on 2/26/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import "AttendeeTableViewCell.h"

@implementation AttendeeTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)sendMessge:(id)sender {
    [self.delegate sendMessageToAttendee:self.selectedAttendee];
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        NSArray *nibs = [[NSBundle mainBundle] loadNibNamed:@"AttendeeTableViewCell" owner:self options:nil];
        self = [nibs objectAtIndex:0];
    }
    self.attendeeImageView.layer.cornerRadius = 24 ;
    self.attendeeImageView.layer.masksToBounds = YES ;
    return self ;
}
@end
