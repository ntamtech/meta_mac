//
//  FullAgendaTableViewCell.h
//  Events
//
//  Created by M.I.Kamashany on 3/6/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol  FullAgendaTableViewCellDelegate <NSObject>

@optional
-(void)addToAgenda:(NSIndexPath *)index ;

@end

@interface FullAgendaTableViewCell : UITableViewCell

@property (nonatomic,weak) id<FullAgendaTableViewCellDelegate> delegate ;
@property (weak, nonatomic) IBOutlet UILabel *sessionDescLabel;
@property (weak, nonatomic) IBOutlet UILabel *calenderLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (strong, nonatomic) NSIndexPath *indexPath ;
@property (weak, nonatomic) IBOutlet UIButton *addToAgendaBtn;
@property (weak, nonatomic) IBOutlet UILabel *speakerNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *speakerFixedImageView;

-(IBAction)addToAgenda:(id)sender ;
@end
