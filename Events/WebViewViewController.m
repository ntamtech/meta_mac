//
//  WebViewViewController.m
//  Events
//
//  Created by M.I.Kamashany on 4/10/18.
//  Copyright © 2018 NtamTech. All rights reserved.
//


#import "ServiceHandler.h"
#import "MBProgressHUD.h"
#import "Helper.h"
#import "WebViewViewController.h"
#import "UIImageView+WebCache.h"
#import "SharedData.h"

@interface WebViewViewController ()<ServiceHandlerDelegate,UIWebViewDelegate> {
    ServiceHandler *service ;
    UIButton *refreshBtn ;
    SharedData *shared ;
}

@property (nonatomic,weak) IBOutlet UIWebView *contentWebView ;
@property (nonatomic,weak) IBOutlet UILabel *titleLabel ;
@property (nonatomic,weak) IBOutlet UIImageView *imageView ;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *webViewTopConstraint;

@end

@implementation WebViewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    service = [[ServiceHandler alloc]init];
    service.delegate = self ;
    self.contentWebView.hidden = true ;
    self.imageView.hidden = true;
    shared = [SharedData getSharedObject];
    self.contentWebView.delegate = self;
    [self setTitleOfNavigationBar:self.selectedType];
}

-(void) setTitleOfNavigationBar:(ContentType) type {
    switch (type) {
        case 0:
            self.titleLabel.text = @"General Information" ;
            break;
        case 1:
            self.titleLabel.text = @"Leadership Principal" ;
            self.webViewTopConstraint.constant = 0 ;
            break;
        case 2:
            self.titleLabel.text = @"Ferring Philosophy" ;
            self.webViewTopConstraint.constant = 0 ;
            break;
        case 3:
            self.titleLabel.text = @"Dinners" ;
            break;
        default:
            break;
    }
}

-(UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent ;
}

-(void)viewWillAppear:(BOOL)animated {
    if (![Helper checkInternet]) {
        [self createRefreshBtn:@"Check Your Internet Connection, Press to try again." enabled:YES];
    }else{
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [service loadWebData:self.selectedType];
    }
}

#pragma mark -ServiceHandlerDelegate

-(void)getWebData:(WebData *)data {
    
    if (self.selectedType == 1 || self.selectedType == 2){
        self.imageView.hidden = true;
        [self.contentWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",data.pdf]]]];
    }else{
        self.contentWebView.hidden = false;
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        self.imageView.hidden = false;
        [self.imageView sd_setImageWithURL:[NSURL URLWithString:data.image] placeholderImage:nil];
        [self.contentWebView loadHTMLString:data.body baseURL:nil];
    }
}

-(void)getAboutData:(About *)data {
    [self.contentWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",data.pdf_file]]]];
//    [self.contentWebView loadHTMLString:data.content baseURL:nil];
}

-(void)requestFailWithError:(NSString *)err {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    self.contentWebView.hidden = true;
    [self createRefreshBtn:err enabled:NO];
}

-(void)createRefreshBtn:(NSString *)title enabled:(BOOL)enabled{
    refreshBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    refreshBtn.frame = CGRectMake(0, self.view.frame.size.height/2, self.view.frame.size.width, 25);
    [refreshBtn setTitle:title forState:UIControlStateNormal];
    refreshBtn.enabled = enabled ;
    refreshBtn.titleLabel.textColor = [UIColor lightGrayColor];
    [refreshBtn addTarget:self
                   action:@selector(refresh:)
         forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:refreshBtn];
    [self.view bringSubviewToFront:refreshBtn];
}

-(IBAction)refresh:(id)sender {
    if ([Helper checkInternet]) {
        [refreshBtn removeFromSuperview];
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [service loadWebData:self.selectedType];
    }
}

-(IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

//MARK: UIWebViewDelegate methods

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    self.contentWebView.hidden = false;
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}
@end
