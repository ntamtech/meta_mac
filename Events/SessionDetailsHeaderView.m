//
//  SessionDetailsHeaderView.m
//  Events
//
//  Created by M.I.Kamashany on 2/27/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import "SessionDetailsHeaderView.h"

@interface SessionDetailsHeaderView ()

@end


@implementation SessionDetailsHeaderView

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        NSArray *nibs = [[NSBundle mainBundle] loadNibNamed:@"SessionDetailsHeaderView" owner:self options:nil];
        self = [nibs objectAtIndex:0];
    }
    return self ;
}

- (IBAction)addToAgenda:(id)sender {
    [self.delegate addSessionToAgendaWithSessionID:[NSNumber numberWithInt:[self.selectedSession.id intValue]]];
}

@end
