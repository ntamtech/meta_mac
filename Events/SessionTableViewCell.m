//
//  SessionTableViewCell.m
//  Events
//
//  Created by M.I.Kamashany on 2/13/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import "SessionTableViewCell.h"

@implementation SessionTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        NSArray *nibs = [[NSBundle mainBundle] loadNibNamed:@"SessionTableViewCell" owner:self options:nil];
        self = [nibs objectAtIndex:0];
    }
    self.someView.layer.masksToBounds = YES ;
    self.someView.layer.cornerRadius = 4 ;
    return self;
}

@end
