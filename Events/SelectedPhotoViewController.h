//
//  SelectedPhotoViewController.h
//  Events
//
//  Created by M.I.Kamashany on 3/25/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectedPhotoViewController : UIViewController

@property (nonatomic,strong) NSIndexPath *selectedPhotoNumber ;

@end
