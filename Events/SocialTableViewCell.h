//
//  SocialTableViewCell.h
//  Events
//
//  Created by M.I.Kamashany on 3/13/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SocialTableViewCell : UITableViewCell

@property (nonatomic,weak) IBOutlet UILabel *linkLabel ;
@property (nonatomic,weak) IBOutlet UIImageView *iconImageView ;

@end
