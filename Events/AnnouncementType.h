//
//  AnnouncementType.h
//  Events
//
//  Created by M.I.Kamashany on 3/8/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"

@interface AnnouncementType : JSONModel

@property (nonatomic) NSString<Optional> *id ;
@property (nonatomic,strong) NSString<Optional> *name ;

@property (nonatomic,strong) NSString<Optional> *title ;
@property (nonatomic,strong) NSString<Optional> *body ;
@property (nonatomic,strong) NSString<Optional> *notification_time ;

@end
