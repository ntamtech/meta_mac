//
//  UIImageView+RoundedImage.h
//  Events
//
//  Created by M.I.Kamashany on 2/12/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (RoundedImage)

-(void)setRoundedWithCornerRadius:(float )corner ;
@end
