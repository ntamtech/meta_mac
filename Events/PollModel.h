//
//  PollModel.h
//  Events
//
//  Created by M.I.Kamashany on 3/2/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"

@protocol Choice ;


@interface PollModel : JSONModel

@property (nonatomic) NSString<Optional> *id ;
@property (nonatomic,strong)    NSString<Optional> *user_id ;
@property (nonatomic,strong)    NSString<Optional> *username ;
@property (nonatomic,strong)    NSString<Optional> *question ;
@property (nonatomic,strong)    NSString<Optional> *user_answer ;
@property (nonatomic,strong)    NSMutableArray<Choice> *choices ;

@end

@interface Choice : JSONModel

@property (nonatomic) NSString<Optional> *id ;
@property (nonatomic,strong)    NSString<Optional> *choice ;
@property (nonatomic,strong)    NSString<Optional> *answer_rate ;

@end
