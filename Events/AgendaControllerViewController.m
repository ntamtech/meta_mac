//
//  AgendaControllerViewController.m
//  Events
//
//  Created by M.I.Kamashany on 3/6/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import "AgendaControllerViewController.h"
#import "SideBarViewController.h"
#import "MBProgressHUD.h"
#import "ServiceHandler.h"
#import "Helper.h"
#import "SharedData.h"
#import "FullAgendaViewController.h"
#import "MyAgendaViewController.h"
#import "AgendaModel.h"
#import "SearchInAgendaViewController.h"
#import <STPopup/STPopup.h>

@interface AgendaControllerViewController ()<ServiceHandlerDelegate>
{
    ServiceHandler *service ;
    SharedData *shared ;
    BOOL isSeen ;
}
@property (nonatomic,weak) IBOutlet UIButton *fullAgendaBtn ;
@property (nonatomic,weak) IBOutlet UIButton *myAgendaBtn ;
@property (weak, nonatomic) IBOutlet UIButton *searchBtn;

@property (nonatomic,weak) IBOutlet UIView *fullAgendaView ;
@property (nonatomic,weak) IBOutlet UIView *myAgendaView ;

-(IBAction)showFullAgenda:(id)sender ;
-(IBAction)showMyAgenda:(id)sender ;
- (IBAction)search:(id)sender;

@end

@implementation AgendaControllerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _myAgendaBtn.layer.cornerRadius = 10 ;
    _myAgendaBtn.layer.masksToBounds = YES ;
    _fullAgendaBtn.layer.cornerRadius = 10 ;
    _fullAgendaBtn.layer.masksToBounds = YES ;
    
    _fullAgendaBtn.backgroundColor = [UIColor colorWithRed:64.0/255 green:94.0/255 blue:155.0/255 alpha:1];
    _myAgendaBtn.backgroundColor = [UIColor clearColor];
    
    _fullAgendaView.alpha = 1 ;
    _myAgendaView.alpha = 0 ;
    
    service = [[ServiceHandler alloc]init];
    service.delegate = self ;
    shared = [SharedData getSharedObject];
    isSeen = NO ;
}

-(UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent ;
}

-(void)viewWillAppear:(BOOL)animated {
    if (!isSeen) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    }
}

-(void)viewDidAppear:(BOOL)animated {
    if (!isSeen) {
        if ([Helper checkInternet]) {
            [service loadFullAgenda:[NSNumber numberWithInt:[shared.user.id intValue]] eventID:[NSNumber numberWithInt:1]];
        } else {
            //get cached attendees it exists
            if (shared.allAgenda == nil) {
                NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:@"allAgenda"];
                if (data != nil) {
                    shared.allAgenda = [NSKeyedUnarchiver unarchiveObjectWithData:data];
                    [self getFullAgenda:shared.allAgenda];
                }else{
                     [MBProgressHUD hideHUDForView:self.view animated:YES];
                }
            }else{
                [self getFullAgenda:shared.allAgenda];
            }
        }
    }
}

-(void)getFullAgenda:(NSMutableArray *)fullAgendaArr {
    isSeen = YES ;
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    shared.allAgenda = fullAgendaArr ;
    
    FullAgendaViewController *fullAgendaVC = [self.storyboard instantiateViewControllerWithIdentifier:@"FullAgendaViewController"];
    
    fullAgendaVC.view.frame = CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height) ;
    [_fullAgendaView addSubview:fullAgendaVC.view];
    _fullAgendaView.backgroundColor = [UIColor lightGrayColor];
    [self addChildViewController:fullAgendaVC];
    [fullAgendaVC didMoveToParentViewController:self];
}

-(IBAction)showFullAgenda:(id)sender {
    _fullAgendaBtn.backgroundColor = [UIColor colorWithRed:64.0/255 green:94.0/255 blue:155.0/255 alpha:1];
    _myAgendaBtn.backgroundColor = [UIColor clearColor];
    
    _fullAgendaView.alpha = 1 ;
    _myAgendaView.alpha = 0 ;
}


-(IBAction)showMyAgenda:(id)sender {
    _myAgendaBtn.backgroundColor = [UIColor colorWithRed:64.0/255 green:94.0/255 blue:155.0/255 alpha:1];
    _fullAgendaBtn.backgroundColor = [UIColor clearColor];
    
    _fullAgendaView.alpha = 0 ;
    _myAgendaView.alpha = 1 ;
    
    NSMutableArray *myAgendaArr = [[NSMutableArray alloc]init];
    for (int i = 0; i <shared.allAgenda.count; i++) {
        AgendaModel *agenda = [shared.allAgenda objectAtIndex:i];
        for (Session *selectedSession in agenda.sessions) {
            if ([selectedSession.add_to_agenda isEqualToString:@"1"]) {
                [myAgendaArr addObject:agenda];
                break ;
            }
        }
    }
    
    shared.myAgenda = myAgendaArr ;
    MyAgendaViewController *myAgendaVC = [self.storyboard instantiateViewControllerWithIdentifier:@"MyAgendaViewController"];
    
    myAgendaVC.view.frame = self.view.frame ;
    [_myAgendaView addSubview:myAgendaVC.view];
    [self addChildViewController:myAgendaVC];
    [myAgendaVC didMoveToParentViewController:self];
}

- (IBAction)search:(id)sender {
    SearchInAgendaViewController *destination = [self.storyboard instantiateViewControllerWithIdentifier:@"SearchInAgendaViewController"];
    destination.contentSizeInPopup = CGSizeMake(self.view.bounds.size.width-30, 350);
    STPopupController *popupController = [[STPopupController alloc] initWithRootViewController:destination];
    popupController.containerView.layer.cornerRadius = 6.0 ;
    popupController.navigationBarHidden = YES ;
    [popupController presentInViewController:self];
}


-(IBAction)showMenu:(id)sender {
    SideBarViewController *destination = (SideBarViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"SideBarViewController"];
    [self.navigationController pushViewController:destination animated:YES];
}

-(IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

//- (void) displayContentController: (UIViewController*)content Container:(UIView *)view {
//    [self addChildViewController:content];
//    content.view.frame = [self ];
//    [self.view addSubview:self.curren;
//    [content didMoveToParentViewController:self];
//}
@end
