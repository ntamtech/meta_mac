//
//  SpeakersViewController.m
//  Events
//
//  Created by M.I.Kamashany on 2/11/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import "SpeakersViewController.h"
#import "SpeakerTableViewCell.h"
#import "SpeakerDetailsViewController.h"
#import "ServiceHandler.h"
#import "Helper.h"
#import "SharedData.h"
#import "MBProgressHUD.h"
#import "Speaker.h"
#import "UIImageView+WebCache.h"
#import "SharedData.h"
#import "UIView+Toast.h"

@interface SpeakersViewController ()<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate,UISearchDisplayDelegate,ServiceHandlerDelegate>
{
    UISearchDisplayController *searchDisplayController ;
    NSMutableArray *allSpeakersArr ;
    NSArray *searchResults;
    ServiceHandler *service ;
    UIButton *refreshBtn ;
    int flag ;
    SharedData *shared ;
}

@property (strong, nonatomic) UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UITableView *speakerTableView;
@property (weak, nonatomic) IBOutlet UILabel *noSpeakerLabel;

@end

@implementation SpeakersViewController

- (void)viewDidLoad {
        [super viewDidLoad];
    
    //Start
    self.searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 20, 320, 44)];
    self.searchBar.placeholder = @"Search by name";
    self.searchBar.delegate = self;
    self.searchBar.showsCancelButton = YES;
    searchDisplayController = [[UISearchDisplayController alloc] initWithSearchBar:self.searchBar contentsController:self];
    searchDisplayController.delegate = self;
    searchDisplayController.searchResultsDataSource = self;
    searchDisplayController.searchResultsDelegate = self;
    searchDisplayController.searchResultsTableView.separatorStyle = UITableViewCellSelectionStyleNone ;
    
    [self.view addSubview:self.searchBar];
    self.searchBar.hidden = YES;
    [searchDisplayController setActive:NO animated:NO];
    
    service = [[ServiceHandler alloc]init];
    service.delegate = self ;
    shared = [SharedData getSharedObject];
    allSpeakersArr = [[NSMutableArray alloc]init];
    _speakerTableView.hidden = YES ;
    _noSpeakerLabel.hidden = YES ;
    flag = 0 ;
}

-(UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent ;
}

-(void)viewWillAppear:(BOOL)animated {
    
    if (flag == 0) {
        
        [self hideTableView:YES];
        if (![Helper checkInternet]) {
            //get cached speakers if it exists
            NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:@"allSpeakersArr"];
            if (data != nil) {
                shared.allSpeakersArr = [NSKeyedUnarchiver unarchiveObjectWithData:data];
                if (shared.allSpeakersArr.count > 1) {
                    [self hideTableView:NO];
                    _noSpeakerLabel.hidden = YES ;
                    [allSpeakersArr addObjectsFromArray:shared.allSpeakersArr] ;
                } else {
                    [self hideTableView:YES];
                    _noSpeakerLabel.hidden = NO ;
                }
            }else{
                
            }
            [self.view makeToast:@"No Internet Connection!"
                        duration:1.5
                        position:CSToastPositionBottom];

//            [self createRefreshBtn:@"No Internet Connection, Try again" enabled:YES];
        }else{
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            [service loadAllSpeakers:[NSNumber numberWithInt:1] UserID:[NSNumber numberWithInt:[shared.user.id intValue]]];
        }
    }else{
        [self searchBarCancelButtonClicked:_searchBar];
        [_speakerTableView reloadData];
    }
}

#pragma mark - ServiceHandlerDelegate

-(void)getAllSpeakers:(NSMutableArray *)speakers {
    if (speakers.count == 0) {
        self.navigationItem.rightBarButtonItem = nil ;
    }
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    if (speakers.count > 0) {
        [self hideTableView:NO];
        _noSpeakerLabel.hidden = YES ;
        [allSpeakersArr addObjectsFromArray:speakers] ;
        shared.allSpeakersArr = allSpeakersArr ;
        [_speakerTableView reloadData];
    } else {
        [self hideTableView:YES];
        _noSpeakerLabel.hidden = NO ;
    }
}


-(void)requestFailWithError:(NSString *)err {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [self hideTableView:YES];
    _noSpeakerLabel.hidden = YES ;
    [self createRefreshBtn:err enabled:NO];
}

#pragma mark -TableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == searchDisplayController.searchResultsTableView) {
        return [searchResults count];
    } else {
        return allSpeakersArr.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellID = @"speakerCell";
    SpeakerTableViewCell *cell = (SpeakerTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell == nil) {
        cell =[[SpeakerTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    if (tableView == searchDisplayController.searchResultsTableView) {
        Speaker *selectedSpeaker = [searchResults objectAtIndex:indexPath.row];
        [cell.speakerIcon setShowActivityIndicatorView:YES];
        [cell.speakerIcon setIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [cell.speakerIcon sd_setImageWithURL:[NSURL URLWithString:selectedSpeaker.image] placeholderImage:[UIImage imageNamed:@"profile-image"]];
        cell.speakerNameLabel.text = [NSString stringWithFormat:@"%@",selectedSpeaker.name] ;
        cell.speakerJobDesc.text = selectedSpeaker.position ;
    } else {
        Speaker *selectedSpeaker = [allSpeakersArr objectAtIndex:indexPath.row];
        [cell.speakerIcon setShowActivityIndicatorView:YES];
        [cell.speakerIcon setIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [cell.speakerIcon sd_setImageWithURL:[NSURL URLWithString:selectedSpeaker.image] placeholderImage:[UIImage imageNamed:@"profile-image"]];
        cell.speakerNameLabel.text = [NSString stringWithFormat:@"%@",selectedSpeaker.name] ;
        cell.speakerJobDesc.text = selectedSpeaker.position ;
    }
    
    return cell ;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 101 ;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    flag = 1 ;
    if (tableView == searchDisplayController.searchResultsTableView) {
        SpeakerDetailsViewController *destination = (SpeakerDetailsViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"SpeakerDetailsViewController"];
        Speaker *selectedSpeaker = [searchResults objectAtIndex:indexPath.row];
        int index = 0 ;
        for (Speaker *speaker in allSpeakersArr) {
            if ([selectedSpeaker.id isEqualToString:speaker.id]) {
                destination.selectedSpeaker = speaker ;
                destination.selectedSpeakerIndex = index ;
                break ;
            }
            index ++ ;
        }
        self.searchBar.hidden = YES;
        [self.searchBar resignFirstResponder];
//        searchDisplayController.searchResultsTableView.hidden = YES ;
        [self.navigationController pushViewController:destination animated:YES];
    }else{
        SpeakerDetailsViewController *destination = (SpeakerDetailsViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"SpeakerDetailsViewController"];
        destination.selectedSpeaker = [allSpeakersArr objectAtIndex:indexPath.row];
        destination.selectedSpeakerIndex = indexPath.row ;
        [self.navigationController pushViewController:destination animated:YES];
    }
}

- (IBAction)show:(id)sender {
    self.searchBar.hidden = NO;
    [self.searchBar becomeFirstResponder];
    [searchDisplayController setActive:YES animated:YES];
}

- (IBAction)searchAboutSpeakers:(id)sender {
    self.searchBar.hidden = NO;
    [self.searchBar becomeFirstResponder];
    [searchDisplayController setActive:YES animated:YES];
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    self.searchBar.hidden = YES;
    [self.searchBar resignFirstResponder];
    searchDisplayController.searchResultsTableView.hidden = YES ;
    _speakerTableView.hidden = NO ;
    [searchDisplayController setActive:NO animated:YES];
}

-(void)searchBarTextDidEndEditing:(UISearchBar *)searchBar{
    if ([searchBar.text isEqualToString:@""]) {
        self.searchBar.hidden = YES;
        [self.searchBar resignFirstResponder];
        searchDisplayController.searchResultsTableView.hidden = YES ;
        _speakerTableView.hidden = NO ;
        [searchDisplayController setActive:NO animated:YES];
    }
   
}


-(IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)createRefreshBtn:(NSString *)title enabled:(BOOL)enabled{
    refreshBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [refreshBtn setTitle:title forState:UIControlStateNormal];
    refreshBtn.enabled = enabled ;
    refreshBtn.titleLabel.textColor = [UIColor redColor];
    refreshBtn.frame = CGRectMake(0, self.view.frame.size.height/2, self.view.frame.size.width, 25);
    [refreshBtn addTarget:self
                   action:@selector(refresh:)
         forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:refreshBtn];
    [self.view bringSubviewToFront:refreshBtn];
}

-(IBAction)refresh:(id)sender {
    if ([Helper checkInternet]) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [refreshBtn removeFromSuperview];
        [service loadAllSpeakers:[NSNumber numberWithInt:1] UserID:[NSNumber numberWithInt:[shared.user.id intValue]]];
    }
}

-(void)hideTableView:(BOOL)isHidden {
    _speakerTableView.hidden = isHidden ;
}


- (void)searchDisplayControllerDidBeginSearch:(UISearchDisplayController *)controller {
    controller.searchResultsTableView.backgroundColor = [UIColor clearColor];
}



- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope {
    if ([searchText isEqualToString:@""]) {
        searchText = @"%&&&&&&" ;
        NSPredicate *resultPredicate = [NSPredicate
                                        predicateWithFormat:@"name != %@",
                                        searchText];
        searchResults = [allSpeakersArr filteredArrayUsingPredicate:resultPredicate];
//        searchDisplayController.searchResultsTableView.hidden = NO ;
        _speakerTableView.hidden = YES ;
    }else{
        NSPredicate *resultPredicate = [NSPredicate
                                        predicateWithFormat:@"name contains[c] %@",
                                        searchText];
        searchResults = [allSpeakersArr filteredArrayUsingPredicate:resultPredicate];
        _speakerTableView.hidden = YES ;
    }
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller
shouldReloadTableForSearchString:(NSString *)searchString {
    [self filterContentForSearchText:searchString
                               scope:[[searchDisplayController.searchBar scopeButtonTitles]
                                      objectAtIndex:[searchDisplayController.searchBar
                                                     selectedScopeButtonIndex]]];
    
    return YES;
}

@end
