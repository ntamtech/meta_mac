//
//  Question.h
//  Events
//
//  Created by M.I.Kamashany on 10/12/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Question : NSObject

@property (nonatomic) NSString *body;
@property (nonatomic) NSString *id;
@property (nonatomic,strong) NSMutableArray *answers ;


@end
