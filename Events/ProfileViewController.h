
//
//  ProfileViewController.h
//  Events
//
//  Created by M.I.Kamashany on 2/28/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AttendeeModel.h"

@interface ProfileViewController : UIViewController <UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,weak) IBOutlet AttendeeModel *attendee ;
@property (nonatomic,weak) IBOutlet UITableView *socialLinksTableView ;
@property (nonatomic,weak) IBOutlet UIImageView *userImageView ;
@property (nonatomic,weak) IBOutlet UILabel *bioLabel ;
@property (nonatomic,weak) IBOutlet UILabel *nameLabel ;
@property (nonatomic,weak) IBOutlet UILabel *positionLabel ;
@property (nonatomic,weak) IBOutlet UILabel *locationLabel ;

@property (nonatomic,weak) IBOutlet UIButton *socialLinksBtn ;
@property (nonatomic,weak) IBOutlet UIButton *bioBtn ;
@property (nonatomic,weak) IBOutlet UIButton *editBioBtn ;
@property (nonatomic,weak) IBOutlet UIButton *personalDataBtn ;

-(IBAction)editSocialLinks:(id)sender ;
-(IBAction)editPersonalData:(id)sender ;
-(IBAction)addNewBio:(id)sender ;
@end
