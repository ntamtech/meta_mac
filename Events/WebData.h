//
//  WebData.h
//  Events
//
//  Created by M.I.Kamashany on 4/10/18.
//  Copyright © 2018 NtamTech. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"
#import "WebContent.h"

@interface WebData : JSONModel

@property (nonatomic,strong)  NSString<Optional> *body ;
@property (nonatomic,strong)  NSString<Optional> *image ;
@property (nonatomic,strong)  NSString<Optional> *pdf ;

@end
