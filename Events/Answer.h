//
//  Answer.h
//  Events
//
//  Created by M.I.Kamashany on 10/15/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Answer : NSObject

@property (nonatomic,strong) NSString *id ;
@property (nonatomic,strong) NSString *body ;

@end
