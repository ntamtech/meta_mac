//
//  SettingsTableViewCell.h
//  Events
//
//  Created by M.I.Kamashany on 2/11/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *settingsTitleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *settingsIcon;
@end
