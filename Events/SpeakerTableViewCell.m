//
//  SpeakerTableViewCell.m
//  Events
//
//  Created by M.I.Kamashany on 2/12/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import "SpeakerTableViewCell.h"
#import "UIImageView+RoundedImage.h"

@implementation SpeakerTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        NSArray *nibs = [[NSBundle mainBundle] loadNibNamed:@"SpeakerTableViewCell" owner:self options:nil];
        self = [nibs objectAtIndex:0];
        [self.speakerIcon setRoundedWithCornerRadius:27.5];
    }
    return self;
}

@end
