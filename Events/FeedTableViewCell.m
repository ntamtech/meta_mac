//
//  FeedTableViewCell.m
//  Events
//
//  Created by M.I.Kamashany on 2/14/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import "FeedTableViewCell.h"
@interface FeedTableViewCell ()

@property (weak, nonatomic) IBOutlet UIView *borderedView;
@end

@implementation FeedTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        NSArray *nibs = [[NSBundle mainBundle] loadNibNamed:@"FeedTableViewCell" owner:self options:nil];
        self = [nibs objectAtIndex:0];
    }
    self.FeedOwnerImageView.layer.cornerRadius = 22 ;
    self.FeedOwnerImageView.layer.masksToBounds = YES ;
    self.borderedView.layer.borderWidth = 1;
    self.borderedView.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    self.borderedView.layer.cornerRadius = 5 ;
    self.borderedView.layer.masksToBounds = YES ;
    return self;
}


- (IBAction)makeLike:(id)sender {
    [self.delegate likePost:self.selectedIndexPath];
}

- (IBAction)addComment:(id)sender {
    [self.delegate showCommentsAtIndex:self.selectedIndexPath];
}

- (IBAction)showPollDetails:(id)sender {
    [self.delegate showPollBy:self.selectedIndexPath];
}
@end
