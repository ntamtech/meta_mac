//
//  ServiceHandler.h
//  Ntam Care
//
//  Created by M.I.Kamashany on 1/16/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "About.h"
#import "User.h"
#import "Message.h"
#import "PostModel.h"
#import "WebData.h"
#import "Constants.h"
#import "Question.h"

@protocol  ServiceHandlerDelegate <NSObject>

@optional
-(void)loginSuccess ;
-(void)completeDataSuccess ;
-(void)getWebData:(WebData *)data ;
-(void)getAboutData:(About *)data ;
-(void)getQuestionData:(Question *)question ;
-(void)getFullAgenda:(NSMutableArray *)fullAgendaArr ;
-(void)getMyAgenda:(NSMutableArray *)myAgendaArr ;
-(void)getAllMessages:(NSMutableArray *)messages ;
-(void)getAllAnnouncementTypes:(NSMutableArray *)types ;
-(void)getAllPolls:(NSMutableArray *)polls ;
-(void)pollAddedSuccessfullyWithPost:(PostModel *)post ;
-(void)getAllSpeakers:(NSMutableArray *)speakers ;
-(void)getAllAttendees:(NSMutableDictionary *)attendees ;
-(void)getAllSliderImages:(NSMutableArray *)sliderImages ;
-(void)getAllPhotos:(NSMutableArray *)photos ;
-(void)likePhotoSuccessfully ;
-(void)photaAddedSuccessfullyWith:(NSString *)imgURL andImageID:(NSString *)imgID ;
-(void)deletePhotoSuccessfully ;
-(void)getPosts:(NSMutableArray *)posts ;
-(void)postSuccess:(PostModel *)post ;
-(void)sessionAddedSuccessfully ;
-(void)notificationSentSuccessfully ;
-(void)sessionRatingSuccessfully:(NSNumber *)rate ;
-(void)bioEditedSuccessfully ;
-(void)likePostSuccessfully ;
-(void)likeSessionSuccessfully ;
-(void)profileEditedSuccessfully ;
-(void)logoutSuccessfully ;
-(void)accountImageEditSuccessfully:(NSString *)newURL ;
-(void)socialLinksEditedSuccessfully ;
-(void)getPollChoices:(NSMutableArray *)choices ;
-(void)showChatMessages:(NSMutableArray *)chatMessages ;
-(void)msgSendSuccessfully ;
-(void)getMessagesCounter:(int)count ;
-(void)loadVenuesAndTracksSuccessfully ;
-(void)questionAnsweredSuccessfully ;

@optional
-(void)requestFailWithError:(NSString *)err ;
-(void)requestToDeletePollFailWithError:(NSString *)err ;

@end


@interface ServiceHandler : NSObject

@property(nonatomic,weak) id<ServiceHandlerDelegate> delegate ;

-(void)loadAboutData ;
-(void)loadWebData:(ContentType)type  ;
-(void)loginByEmail:(NSString *)email code:(NSString *)code registerID:(NSString *)registerID eventID:(NSNumber *)eventID ;
-(void)completeData:(NSString *)email country:(NSString *)country hospital:(NSString *)hospital mobile:(NSString *)mobile userID:(NSNumber *)userID;
-(void)loadFullAgenda:(NSNumber *)userID eventID:(NSNumber *)eventID ;
-(void)loadMyAgenda:(NSNumber *)userID eventID:(NSNumber *)eventID ;
-(void)loadAllMessages:(NSNumber *)userID eventID:(NSNumber *)eventID ;
-(void)loadMesssagesCount:(NSNumber *)eventID UserID:(NSNumber *)userID  ;
-(void)notifyMessageRead:(NSNumber *)currentUserID andAnotherUser:(NSNumber *)anotherUserID ;
-(void)loadAllSpeakers:(NSNumber *)eventID UserID:(NSNumber *)userID;
-(void)loadAllAttendees:(NSNumber *)eventID ;
-(void)loadAnnouncementTypes:(NSNumber *)eventID ;
-(void)loadAnnouncementList:(NSNumber *)eventID userID:(NSNumber *)userID  ;
-(void)loadAllPolls:(NSNumber *)eventID userID:(NSNumber *)userID;
-(void)addPoll:(NSNumber *)eventID userID:(NSNumber *)userID question:(NSString *)question answersJson:(NSString *)json ;
-(void)deletePollByID:(NSNumber *)pollID ;
-(void)loadEventSliderImages:(NSNumber *)eventID ;
-(void)loadAlPosts:(NSNumber *)userID eventID:(NSNumber *)eventID andPageNum:(NSNumber *)num andPostDate:(NSString *)date;
-(void)sendPostWithContent:(NSString *)content eventID:(NSNumber *)eventID userID:(NSNumber *)userID isCheckIn:(BOOL)flag;

-(void)addSessionToAgenda:(NSNumber *)eventID sessionID:(NSNumber *)sessionID userID:(NSNumber *)userID ;
-(void)removeSessionFromAgenda:(NSNumber *)eventID sessionID:(NSNumber *)sessionID userID:(NSNumber *)userID ;
-(void)addSessionRate:(NSNumber *)rate sessionID:(NSNumber *)sessionID userID:(NSNumber *)userID ;
-(void)sendNotification:(NSNumber *)eventID title:(NSString *)title body:(NSString *)body isDraft:(NSNumber *)isDraft notificationTarget:(NSString *)json ;

// profile
-(void)editBio:(NSNumber *)eventID userID:(NSNumber *)userID bio:(NSString *)bioText ;
-(void)editProfileImage:(NSNumber *)eventID userID:(NSNumber *)userID imageContent:(NSString *)imageStr ;
-(void)editProfile:(NSNumber *)eventID userID:(NSNumber *)userID
                name:(NSString *)name
                position:(NSString *)position
                company:(NSString *)company
                location:(NSString *)location ;
-(void)editSocialLinks:(NSNumber *)eventID userID:(NSNumber *)userID
              fb_link:(NSString *)fb_link
          twitter_link:(NSString *)twitter_link
         linkedin_link:(NSString *)linkedin_link ;


-(void)answerPoll:(NSNumber *)userID PollID:(NSNumber *)pID choiceID:(NSNumber *)cID ;

-(void)likePost:(NSNumber *)eventID userID:(NSNumber *)userID postID:(NSNumber *)postID ;
-(void)likeSession:(NSNumber *)userID sessionID:(NSNumber *)sessionID ;
-(void)getMessagesBtwMe:(NSNumber *)userID andAnother:(NSNumber *)anotherUser andEventID:(NSNumber *)eventID;
-(void)sendChatMessage:(NSNumber *)eventID andSenderID:(NSNumber *)senderID andReceiverID:(NSNumber *)receiverID andMessage:(NSString *)msg andImage:(NSString *)encodedImage64 ;
-(void)loadAllPhotos:(NSNumber *)eventID userID:(NSNumber *)userID  ;
-(void)likePhoto:(NSNumber *)eventID userID:(NSNumber *)userID photoID:(NSNumber *)photoID ;
-(void)deletePhoto:(NSNumber *)photoID ;
-(void)addPhoto:(NSNumber *)eventID andUserID:(NSNumber *)userID andImageEncoded:(NSString *)imageStr ;

//comments
-(void)addPhotoComment:(NSNumber *)eventID andUserID:(NSNumber *)userID andComment:(NSString *)comment andPhotoID:(NSNumber *)photoID ;
-(void)addPostComment:(NSNumber *)eventID andUserID:(NSNumber *)userID andComment:(NSString *)comment andPostID:(NSNumber *)postID ;
-(void)addSessionComment:(NSNumber *)userID andComment:(NSString *)comment andSessionID:(NSNumber *)sessionID ;

-(void)logoutUser:(NSNumber *)userID  ;

//techne Summit
-(void)loadTracksAndVenues:(NSNumber *)eventID ;
-(void)searchInAgendaBy:(NSNumber *)eventID andSearchKey:(NSString *)key andUserID:(NSNumber *)userID;

// Live Voting
-(void)answerQuestionWithUserID:(NSNumber *)userID questionID:(NSNumber *)qID answerID:(NSNumber *)answerID ;
-(void)loadQuestion:(NSNumber *)questionID ;

@end
