//
//  PaddingLabel.m
//  Messages
//
//  Created by M.I.Kamashany on 3/27/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import "PaddingLabel.h"

@implementation PaddingLabel


- (void)drawRect:(CGRect)rect {
    if (_isMe) {
        UIEdgeInsets insets = {0, 3, 0, 39};
        [super drawTextInRect:UIEdgeInsetsInsetRect(rect, insets)];
    } else {
        UIEdgeInsets insets = {0, 39, 0, 3};
        [super drawTextInRect:UIEdgeInsetsInsetRect(rect, insets)];
    }
}

@end
