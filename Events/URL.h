//
//  URL.h
//  iJobs
//
//  Created by M.I.Kamashany on 12/11/16.
//  Copyright © 2016 NTAMNTAMTech. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface URL : NSObject

+(NSString *)getAboutAppURL;
+(NSString *)getLoginURL;
+(NSString *)getSignupURL;
+(NSString *)getAllMessagesURL;
+(NSString *)getAllSpeakersURL ;
+(NSString *)getEventSliderURL ;
+(NSString *)getAllPostsURL ;
+(NSString *)getAttendeesURL ;
+(NSString *)getAaddPostURL ;
+(NSString *)getAddSessionToAgenda ;
+(NSString *)getAddSessionRateURL ;
+(NSString *)getPollsURL ;
+(NSString *)getFullAgendaURL ;
+(NSString *)getMyAgendaURL ;
+(NSString *)getAnnouncementTypesURL ;
+(NSString *)getAddPollURL ;
+(NSString *)getDeletePollURL ;
+(NSString *)getSendNotificationURL ;
+(NSString *)getAnnouncementListURL ;
+(NSString *)getEditBioURL ;
+(NSString *)getAnswerPollURL ;
+(NSString *)getEditProfileURL ;
+(NSString *)getEditAccountImageURL ;
+(NSString *)getEditSocialLinksURL ;
+(NSString *)getLikePostURL ;
+(NSString *)getLikeSessionURL ;
+(NSString *)getMessagesBtwMeAndAnotherURL ;
+(NSString *)getMessageCountURL ;
+(NSString *)getReadMessageURL ;
+(NSString *)getAllPhotosURL ;
+(NSString *)getLikePhotoURL ;
+(NSString *)getDeletePhotoURL ;
+(NSString *)getSendChatMessageURL ;
+(NSString *)getAddPhotoURL ;

+(NSString *)getAddPhotoCommentURL ;
+(NSString *)getAddPostCommentURL ;
+(NSString *)getAddSessionCommentURL ;
+(NSString *)getLogoutURL ;

+(NSString *)getAllTracksAndVenuesURL;
+(NSString *)searchInAgendaURL;

@end
