//
//  NewsFeedsViewController.m
//  Events
//
//  Created by M.I.Kamashany on 2/9/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import "NewsFeedsViewController.h"
#import "SideBarViewController.h"
#import "KIImagePager.h"
#import "FeedTableViewCell.h"
#import "SliderModel.h"
#import "SharedData.h"
#import "ServiceHandler.h"
#import "Helper.h"
#import "SVPullToRefresh.h"
#import "UIScrollView+SVPullToRefresh.h"
#import "UIView+Toast.h"
#import "MBProgressHUD.h"
#import "PostModel.h"
#import "UIScrollView+SVInfiniteScrolling.h"
#import "UIImageView+WebCache.h"
#import "WritePostViewController.h"
#import <STPopup/STPopup.h>
#import "CommentsViewController.h"
#import "AnswerViewController.h"
#import "Question.h"
#import "Answer.h"

@interface NewsFeedsViewController ()<UITableViewDelegate,UITableViewDataSource, KIImagePagerDelegate,KIImagePagerDataSource,ServiceHandlerDelegate,WritePostViewControllerDelegate,FeedTableViewCellDelegate>
{
    SharedData *shared ;
    ServiceHandler *service ;
    int count ;
    int jobsCount ;
    NSMutableArray *postsArr;
    BOOL pullFromTop ;
    NSArray *dateAndTime ;
    BOOL isCheckin ;
    BOOL isWritePostPopUpHidden ;
}
@property (weak, nonatomic) IBOutlet KIImagePager *imagePager;
@property (weak, nonatomic) IBOutlet UITableView *feedsTableView;
@property(nonatomic, strong) NewsFeedsViewController *weakSelf ;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityLoader;
@property (weak, nonatomic) IBOutlet UIButton *problemBtn;
- (IBAction)refresh:(id)sender;

@end

@implementation NewsFeedsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.feedsTableView.estimatedRowHeight = 180 ;
    self.feedsTableView.rowHeight = UITableViewAutomaticDimension ;
    self.feedsTableView.allowsSelection = NO ;
    
    self.weakSelf = self;
    // setup infinite scrolling
    [self.feedsTableView addPullToRefreshWithActionHandler:^{
        [self.weakSelf insertRowsAtTop];
    }];
    
    [self.feedsTableView addInfiniteScrollingWithActionHandler:^{
        [self.weakSelf insertRowAtBottom];
    }];
    
    service = [[ServiceHandler alloc]init];
    service.delegate = self ;
    shared = [SharedData getSharedObject];
    //caching user
    if (shared.user) {
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:shared.user];
        [[NSUserDefaults standardUserDefaults] setObject:data forKey:@"user"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    count = 1 ;
    jobsCount = 0 ;
    postsArr = [[NSMutableArray alloc]init];
    [self.activityLoader stopAnimating];
    self.problemBtn.hidden = YES ;
    isWritePostPopUpHidden = YES ;
    
}

-(UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent ;
}

-(void)viewWillAppear:(BOOL)animated{
    
    _imagePager.pageControl.currentPageIndicatorTintColor = [UIColor lightGrayColor];
    _imagePager.pageControl.pageIndicatorTintColor = [UIColor blackColor];
    _imagePager.slideshowTimeInterval = 3.0f;
    _imagePager.slideshowShouldCallScrollToDelegate = YES;
    [_imagePager setImageCounterDisabled:YES];
    [_imagePager setUserInteractionEnabled:NO];
//    [self.view sendSubviewToBack:self.imagePager];
    
    if (![Helper checkInternet]) {
        [self.activityLoader stopAnimating];
        self.problemBtn.hidden = YES ;
        
        //get cached posts and slider photos if it exists
        NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:@"postsArr"];
        if (data != nil) {
            shared.postsArr = [NSKeyedUnarchiver unarchiveObjectWithData:data];
            postsArr = shared.postsArr ;
        }
        
        if (isWritePostPopUpHidden) {
            [self.view makeToast:@"No Internet Connection!"
                        duration:1.5
                        position:CSToastPositionBottom];
        }
    }else{
        static dispatch_once_t once;
        dispatch_once(&once, ^ {
            [_imagePager reloadData];
        });
        if (postsArr.count == 0) {
            
                self.feedsTableView.hidden = YES ;
                [self.activityLoader startAnimating];
                self.problemBtn.hidden = YES ;
                [service loadAlPosts:[NSNumber numberWithInt:[shared.user.id intValue]] eventID:[NSNumber numberWithInt:1] andPageNum:[NSNumber numberWithInt:count] andPostDate:@""];
        }else{
            postsArr = shared.postsArr ;
            [_feedsTableView reloadData];
        }
    }
}


- (void)insertRowsAtTop {
    if (![Helper checkInternet]) {
        // toast with a specific duration and position
        [self.weakSelf.feedsTableView.pullToRefreshView stopAnimating];
        [self.view makeToast:@"No Internet Connection!"
                    duration:1.0
                    position:CSToastPositionBottom];
    }else{
        pullFromTop = YES ;
        count = 1 ;
        [self.feedsTableView.pullToRefreshView startAnimating];
        [service loadAlPosts:[NSNumber numberWithInt:[shared.user.id intValue]] eventID:[NSNumber numberWithInt:1] andPageNum:[NSNumber numberWithInt:count] andPostDate:@""];
    }
}

- (void)insertRowAtBottom {
    if (jobsCount > postsArr.count) {
        [self.weakSelf.feedsTableView.infiniteScrollingView stopAnimating];
    } else {
        if (![Helper checkInternet]) {
            // toast with a specific duration and position
            [self.weakSelf.feedsTableView.infiniteScrollingView stopAnimating];
            [self.view makeToast:@"No Internet Connection!"
                        duration:1.0
                        position:CSToastPositionBottom];
        }else{
            pullFromTop = NO ;
            [self.feedsTableView.infiniteScrollingView startAnimating];
            [service loadAlPosts:[NSNumber numberWithInt:[shared.user.id intValue]] eventID:[NSNumber numberWithInt:1] andPageNum:[NSNumber numberWithInt:count] andPostDate:@""];
        }
    }
}

#pragma mark - ServiceHandlerDelegate 

-(void)getPosts:(NSMutableArray *)posts {
    if (pullFromTop == YES) {
        [postsArr removeAllObjects];
        [postsArr addObjectsFromArray:posts];
        [self.activityLoader stopAnimating];
        self.feedsTableView.hidden = NO;
        [self.feedsTableView reloadData];
        shared.postsArr = postsArr ;
        count++ ;
        jobsCount = 50 ;
        [self.weakSelf.feedsTableView.pullToRefreshView stopAnimating];
    } else {
        [self.activityLoader stopAnimating];
        [postsArr addObjectsFromArray:posts];
        self.feedsTableView.hidden = NO;
        [self.feedsTableView reloadData];
        shared.postsArr = postsArr ;
        count ++ ;
        jobsCount += 50 ;
        [self.weakSelf.feedsTableView.infiniteScrollingView stopAnimating];
    }
}

-(void)requestFailWithError:(NSString *)err {
    if ([err isEqualToString:@"You Are Already Checked In !"]) {
        [self.view hideToasts];
        [self.view makeToast:err
                    duration:1.0
                    position:CSToastPositionBottom];
    }else{
        self.feedsTableView.hidden = YES ;
        [self.activityLoader stopAnimating];
        [self.problemBtn setTitle:err forState:UIControlStateNormal];
    }
    
}

#pragma mark - KIImagePager DataSource

- (NSArray *) arrayWithImages:(KIImagePager*)pager {
    return @[[UIImage imageNamed:@"NewsFeed-bg-EC"]];
}

- (UIViewContentMode) contentModeForImage:(NSUInteger)image inPager:(KIImagePager *)pager {
    return UIViewContentModeScaleAspectFill;
}

-(IBAction)showMenu:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark -TableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return postsArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellID = @"feedCell";
    FeedTableViewCell *cell = (FeedTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell == nil) {
        cell =[[FeedTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    PostModel *selectedPost = [postsArr objectAtIndex:indexPath.row];
    cell.feedOwnerLabel.text = selectedPost.username ;
    cell.feedDescLabel.text = selectedPost.post;
    cell.likesCountLabel.text = selectedPost.likes ;
    cell.CommentsCountLabel.text = [NSString stringWithFormat:@"%lu",(unsigned long)selectedPost.comments.count];
    
    dateAndTime = [selectedPost.post_date componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    cell.timeLabel.text = [Helper detectPMAM:[dateAndTime objectAtIndex:1]];
    
    dateAndTime = [[dateAndTime objectAtIndex:0] componentsSeparatedByString: @"-"];
    int monthNumber = [[dateAndTime objectAtIndex:1] intValue];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en"]];
    NSString *monthName = [[dateFormat monthSymbols] objectAtIndex:(monthNumber-1)];
    cell.dateLabel.text = [NSString stringWithFormat:@"%i %@",[[dateAndTime objectAtIndex:2] intValue],[monthName substringToIndex:3]];
    [cell.FeedOwnerImageView setShowActivityIndicatorView:YES];
    [cell.FeedOwnerImageView setIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [cell.FeedOwnerImageView sd_setImageWithURL:[NSURL URLWithString:selectedPost.user_image] placeholderImage:[UIImage imageNamed:@"profile-image"]];
    cell.selectedIndexPath = indexPath ;
    cell.delegate = self ;
    cell.upperView.alpha = 0 ;
    
    if ([selectedPost.make_like isEqualToString:@"0"]) {
        cell.islikeImageview.image = [UIImage imageNamed:@"notLike"];
    } else {
        cell.islikeImageview.image = [UIImage imageNamed:@"like"];
    }
    cell.likesCountLabel.text = selectedPost.likes ;
    if (selectedPost.poll_data) {
        cell.showPollBtn.hidden = NO;
        cell.feedPollDescLabel.hidden = NO ;
        cell.feedDescLabel.hidden = YES;
    } else {
        cell.showPollBtn.hidden = YES;
        cell.feedPollDescLabel.hidden = YES ;
        cell.feedDescLabel.hidden = NO;
    }
    return cell ;
}

- (IBAction)refresh:(id)sender {
    if (![Helper checkInternet]) {
        [self.activityLoader stopAnimating];
        self.problemBtn.hidden = NO ;
        [self.problemBtn setTitle:@"No internet Connection, try again" forState:UIControlStateNormal];
    }else{
        if (postsArr.count == 0) {
            [self.activityLoader startAnimating];
            self.problemBtn.hidden = YES ;
            self.feedsTableView.hidden = YES ;
            [service loadAlPosts:[NSNumber numberWithInt:[shared.user.id intValue]] eventID:[NSNumber numberWithInt:1] andPageNum:[NSNumber numberWithInt:count] andPostDate:@""];
        }
    }
}


- (IBAction)checkin:(id)sender {
    if (![Helper checkInternet]) {
        [self.activityLoader stopAnimating];
        [self.view makeToast:@"No Internet Connection!"
                    duration:1.0
                    position:CSToastPositionBottom];
    }else{
        isCheckin = YES ;
        [self.view makeToast:@"Checkin Loading..."
                    duration:10
                    position:CSToastPositionBottom];
        [service sendPostWithContent:@"" eventID:[NSNumber numberWithInt:1] userID:[NSNumber numberWithInt:[shared.user.id intValue]] isCheckIn:YES];
    }
}

- (IBAction)writePost:(id)sender {
    
    isWritePostPopUpHidden = NO ;
    WritePostViewController *destination = [self.storyboard instantiateViewControllerWithIdentifier:@"WritePostViewController"];
    destination.delegate = self ;
    destination.isWritePost = YES;
    destination.contentSizeInPopup = CGSizeMake(self.view.bounds.size.width-30, 350);
    STPopupController *popupController = [[STPopupController alloc] initWithRootViewController:destination];
    popupController.containerView.layer.cornerRadius = 6.0 ;
    popupController.navigationBarHidden = YES ;
    [popupController presentInViewController:self];
}

-(void)dismissWithPostDetails:(NSString *)details {
    if (![Helper checkInternet]) {
        [self.view makeToast:@"No Internet Connection!"
                    duration:1
                    position:CSToastPositionBottom];
    } else {
        isCheckin = NO ;
        [self.view makeToast:@"Post Loading..."
                    duration:10
                    position:CSToastPositionBottom];
        [service sendPostWithContent:details eventID:[NSNumber numberWithInt:1] userID:[NSNumber numberWithInt:[shared.user.id intValue]] isCheckIn:NO];
    }
}

-(void)postSuccess:(PostModel *)post {
    [self.view hideToasts];
    [postsArr insertObject:post atIndex:0];
    [_feedsTableView reloadData];
}

#pragma mark - FeedTableViewCellDelegate

-(void)likePost:(NSIndexPath *)selectedIndex  {
    if (![Helper checkInternet]) {
        [self.view makeToast:@"No Internet Connection!"
                            duration:1
                            position:CSToastPositionBottom];

    } else {
        PostModel *selectedPost = [postsArr objectAtIndex:selectedIndex.row];
        if ([selectedPost.make_like isEqualToString:@"0"]) {
            selectedPost.make_like = @"1" ;
            int likes = [selectedPost.likes intValue] + 1 ;
            selectedPost.likes = [NSString stringWithFormat:@"%i",likes];
            [postsArr removeObjectAtIndex:selectedIndex.row];
            [postsArr insertObject:selectedPost atIndex:selectedIndex.row];
            shared.postsArr = postsArr ;
            [self.feedsTableView beginUpdates];
            [self.feedsTableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:selectedIndex, nil] withRowAnimation:UITableViewRowAnimationNone];
            [self.feedsTableView endUpdates];
            [service likePost:[NSNumber numberWithInt:1] userID:[NSNumber numberWithInt:[shared.user.id intValue]] postID:[NSNumber numberWithInt:[selectedPost.id intValue]]];
        } else {
            selectedPost.make_like = @"0" ;
            int likes = [selectedPost.likes intValue] - 1 ;
            selectedPost.likes = [NSString stringWithFormat:@"%i",likes];
            [postsArr removeObjectAtIndex:selectedIndex.row];
            [postsArr insertObject:selectedPost atIndex:selectedIndex.row];
            shared.postsArr = postsArr ;
            [self.feedsTableView beginUpdates];
            [self.feedsTableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:selectedIndex, nil] withRowAnimation:UITableViewRowAnimationNone];
            [self.feedsTableView endUpdates];
            [service likePost:[NSNumber numberWithInt:1] userID:[NSNumber numberWithInt:[shared.user.id intValue]] postID:[NSNumber numberWithInt:[selectedPost.id intValue]]];
        }
    }
}

-(void)likePostSuccessfully {
    
}

-(void)showCommentsAtIndex:(NSIndexPath *)selectedIndex {
    PostModel *post = [postsArr objectAtIndex:selectedIndex.row];
    CommentsViewController *destination = [self.storyboard instantiateViewControllerWithIdentifier:@"CommentsViewController"];
    destination.selectedPost = post ;
    destination.selectedIndex = selectedIndex ;
    destination.selectePhoto = nil ;
    destination.selectedSession = nil ;
    [self.navigationController pushViewController:destination animated:YES];
}


-(void)showPollBy:(NSIndexPath *)selectedIndex {
    AnswerViewController *destination = [self.storyboard instantiateViewControllerWithIdentifier:@"AnswerViewController"];
    destination.pollIndexAtPolls = nil ;
    destination.postIndexAtNewsFeeds = selectedIndex ;
    [self.navigationController pushViewController:destination animated:YES];
}

@end
