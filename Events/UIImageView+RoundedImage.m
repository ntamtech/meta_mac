//
//  UIImageView+RoundedImage.m
//  Events
//
//  Created by M.I.Kamashany on 2/12/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import "UIImageView+RoundedImage.h"

@implementation UIImageView (RoundedImage)

-(void)setRoundedWithCornerRadius:(float )corner {
    self.layer.masksToBounds = YES ;
    self.layer.cornerRadius = corner ;
}

@end
