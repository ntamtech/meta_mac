//
//  LastLikeUser.m
//  Events
//
//  Created by M.I.Kamashany on 4/4/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import "PostModel.h"

@implementation LastLikeUser

- (void)encodeWithCoder:(NSCoder *)encoder {
    //Encode properties, other class variables, etc
    [encoder encodeObject:self.user_id forKey:@"user_id"];
    [encoder encodeObject:self.username forKey:@"username"];
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        //decode properties, other class vars
        self.user_id = [decoder decodeObjectForKey:@"user_id"];
        self.username = [decoder decodeObjectForKey:@"username"];
    }
    return self;
}


@end
