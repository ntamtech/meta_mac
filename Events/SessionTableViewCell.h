//
//  SessionTableViewCell.h
//  Events
//
//  Created by M.I.Kamashany on 2/13/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SessionTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *sessionDescLabel;
@property (weak, nonatomic) IBOutlet UILabel *calenderLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;
@property (weak, nonatomic) IBOutlet UIView *someView;

@end
