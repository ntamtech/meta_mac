//
//  SideBarCollectionViewCell.h
//  Events
//
//  Created by M.I.Kamashany on 2/9/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SideBarCollectionViewCell : UICollectionViewCell

@property (nonatomic,weak) IBOutlet UIImageView *sideBarIcon ;
@property (nonatomic,weak) IBOutlet UILabel *sideBarTitle ;
@property (weak, nonatomic) IBOutlet UILabel *messageCountLabel;

@end
