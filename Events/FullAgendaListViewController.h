//
//  FullAgendaListViewController.h
//  Events
//
//  Created by M.I.Kamashany on 3/6/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FullAgendaListViewController : UIViewController

@property (nonatomic,strong) NSMutableArray *sessionOfDay ;
@property (nonatomic,assign) int selectedAgendaIndex ;

@end
