//
//  PhotosViewController.m
//  Events
//
//  Created by M.I.Kamashany on 3/24/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import "PhotosViewController.h"
#import "PhotoCollectionViewCell.h"
#import "SideBarViewController.h"
#import "Helper.h"
#import "ServiceHandler.h"
#import "SharedData.h"
#import "MBProgressHUD.h"
#import "UIImageView+WebCache.h"
#import "UIView+Toast.h"
#import "SelectedPhotoViewController.h"
#import "UIViewController+Alert.h"

@interface PhotosViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,ServiceHandlerDelegate,PhotoCollectionViewCellDelegate,UINavigationControllerDelegate, UIImagePickerControllerDelegate>
{
    ServiceHandler *service ;
    SharedData *shared ;
    NSMutableArray *photoArrs ;
    UIImagePickerController *imagePicker ;
    BOOL isPhotosLoaded ;
}

@property (nonatomic,weak) IBOutlet UICollectionView *photosCollectionView ;
@property (weak, nonatomic) IBOutlet UIButton *refreshBtn;

@end

@implementation PhotosViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    photoArrs = [[NSMutableArray alloc]init];
    service = [[ServiceHandler alloc]init];
    service.delegate = self ;
    shared = [SharedData getSharedObject];
    _photosCollectionView.hidden = YES ;
    self.refreshBtn.hidden = YES ;
    isPhotosLoaded = NO ;
}

-(UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent ;
}

-(void)viewWillAppear:(BOOL)animated{
    if (photoArrs.count == 0) {
        if (!isPhotosLoaded) {
            isPhotosLoaded = YES ;
            if (![Helper checkInternet]) {
                [self.refreshBtn setTitle:@"No Internet Connection, Try again" forState:UIControlStateNormal];
                self.refreshBtn.hidden = NO ;
                _photosCollectionView.hidden = YES ;
            }else{
                [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                [service loadAllPhotos:[NSNumber numberWithInt:1] userID:[NSNumber numberWithInt:[shared.user.id intValue]]];
            }
        }else{
            [self.refreshBtn setTitle:@"Be the first one to upload photo" forState:UIControlStateNormal];
            self.refreshBtn.hidden = NO ;
            self.refreshBtn.enabled = NO ;
            _photosCollectionView.hidden = YES ;
        }
    } else {
        [_photosCollectionView reloadData];
    }
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return photoArrs.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *reuseIdentifier = @"photoCell";
    PhotoCollectionViewCell *cell = (PhotoCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    PhotoModel *selectedPhoto = [photoArrs objectAtIndex:indexPath.row];
    cell.indexPath = indexPath ;
    cell.layer.borderWidth = .5 ;
    cell.layer.borderColor = [UIColor lightGrayColor].CGColor ;
    [cell.photoImageView setShowActivityIndicatorView:YES];
    [cell.photoImageView setIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [cell.photoImageView sd_setImageWithURL:[NSURL URLWithString:selectedPhoto.image] placeholderImage:nil];
    cell.delegate = self ;
    if ([selectedPhoto.make_like isEqualToString:@"0"]) {
        [cell.likeStatusPhotoImageView setImage:[UIImage imageNamed:@"dislikePhoto"]];
    } else {
        [cell.likeStatusPhotoImageView setImage:[UIImage imageNamed:@"likePhoto"]];
    }
    
//    [cell.photoImageView sd_setImageWithURL:[NSURL URLWithString:selectedPhoto.image] placeholderImage:[UIImage imageNamed:@"profile-image"]];

    return cell;
}

#pragma mark <UICollectionViewDelegate>

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat screenWidth = self.photosCollectionView.bounds.size.width;
    float cellWidth = screenWidth /3.0 -10; //Replace the divisor with the column count requirement. Make sure to have it in float.
    CGSize size = CGSizeMake(cellWidth, cellWidth);
    return size;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(5,5, 5, 5);
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    SelectedPhotoViewController *destination = [self.storyboard instantiateViewControllerWithIdentifier:@"SelectedPhotoViewController"];
    destination.selectedPhotoNumber = indexPath ;
    [self.navigationController pushViewController:destination animated:YES];
}


-(IBAction)showMenu:(id)sender {
    SideBarViewController *destination = (SideBarViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"SideBarViewController"];
    [self.navigationController pushViewController:destination animated:YES];
}

-(IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)refresh:(id)sender {
    if ([Helper checkInternet]) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        self.refreshBtn.hidden = YES ;
        [service loadAllPhotos:[NSNumber numberWithInt:1] userID:[NSNumber numberWithInt:[shared.user.id intValue]]];
    }
}

#pragma mark - ServiceHandlerDelegate

-(void)getAllPhotos:(NSMutableArray *)photos {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    if (photos.count == 0) {
        [self.refreshBtn setTitle:@"Be the first one to upload photo" forState:UIControlStateNormal];
        [self.refreshBtn setEnabled:NO];
        if (self.refreshBtn.hidden) {
            [self.refreshBtn setHidden:NO];
        }
        [_photosCollectionView setHidden:YES];
    } else {
        [photoArrs addObjectsFromArray:photos];
        shared.photosArr = photoArrs ;
        [_photosCollectionView reloadData];
        [_photosCollectionView setHidden:NO];
    }
    
}

-(void)requestFailWithError:(NSString *)err{
    [self.view hideToasts];
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [self showAlert:err];
}

-(void)likePhotoSuccessfully {
    
}


#pragma mark - PhotoCollectionViewCellDelegate

-(void)likePhoto:(NSIndexPath *)selectedIndex {
    PhotoModel *photo = [photoArrs objectAtIndex:selectedIndex.row];
    if (![Helper checkInternet]) {
        [self.view makeToast:@"No Internet Connection!"
                    duration:1
                    position:CSToastPositionBottom];
    }else{
        if ([photo.make_like isEqualToString:@"0"]) {
            [service likePhoto:[NSNumber numberWithInt:1] userID:[NSNumber numberWithInt:[shared.user.id intValue]] photoID:[NSNumber numberWithInt:[photo.id intValue]]];
            photo.make_like = @"1";
            [photoArrs removeObjectAtIndex:selectedIndex.row];
            [photoArrs insertObject:photo atIndex:selectedIndex.row];
            [_photosCollectionView reloadItemsAtIndexPaths:@[selectedIndex]];
        }else{
            [service likePhoto:[NSNumber numberWithInt:1] userID:[NSNumber numberWithInt:[shared.user.id intValue]] photoID:[NSNumber numberWithInt:[photo.id intValue]]];
            photo.make_like = @"0";
            [photoArrs removeObjectAtIndex:selectedIndex.row];
            [photoArrs insertObject:photo atIndex:selectedIndex.row];
            [_photosCollectionView reloadItemsAtIndexPaths:@[selectedIndex]];
        }
    }
    
}

- (IBAction)addNewPhoto:(id)sender {
    NSString *title = @"Select Photo from";
    UIAlertController *alertController=   [UIAlertController
                                           alertControllerWithTitle:title
                                           message:@""
                                           preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *gallery = [UIAlertAction
                              actionWithTitle:@"Gallery"
                              style:UIAlertActionStyleDefault
                              handler:^(UIAlertAction * action)
                              {
                                  [self selectFromExisting];
                              }];
    
    UIAlertAction *camera = [UIAlertAction
                             actionWithTitle:@"Camera"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [self takeNewPhoto];
                                 
                             }];
    
    UIAlertAction *cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleDestructive
                             handler:^(UIAlertAction * action)
                             {
                                 [alertController dismissViewControllerAnimated:YES completion:nil];
                             }];
    [alertController addAction:camera];
    [alertController addAction:gallery];
    [alertController addAction:cancel];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void) takeNewPhoto{
    //this is used to check if the device have a camera or not .To avoid crashes .
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        imagePicker.allowsEditing = YES;
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:imagePicker animated:YES completion:NULL];
    }else{
        // NSLog(@"simulator doesnot have a camera");
    }
}
- (void) selectFromExisting{
    //this is used to check if the device have photo library or not .To avoid crashes .
    if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeSavedPhotosAlbum])
    {
        imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:imagePicker animated:YES completion:NULL];
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *selectedImage = [info objectForKey:UIImagePickerControllerOriginalImage];
    __block NSString  *imageBase64 ;
    [picker dismissViewControllerAnimated:YES completion:^{
        
        if (![Helper checkInternet]) {
            [self.view makeToast:@"No Internet Connection" duration:100000.0 position:CSToastPositionBottom];
        } else {
            [self.view makeToast:@"Uploading a Photo ..." duration:100000.0 position:CSToastPositionBottom];
            NSData *imageData = UIImageJPEGRepresentation(selectedImage, .8);
            int size = (int)[imageData length]/1024 ;
            float scale = 0.9 ;
            while (size > 500) {
                imageData = UIImageJPEGRepresentation(selectedImage,scale);
                scale -= .1;
                size = (int)[imageData length]/1024;
            }
            imageBase64 = [imageData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
            [service addPhoto:[NSNumber numberWithInt:1] andUserID:[NSNumber numberWithInt:[shared.user.id intValue]] andImageEncoded:imageBase64];
        }
    }];
}

-(void)photaAddedSuccessfullyWith:(NSString *)imgURL andImageID:(NSString *)imgID {
    [self.view hideToasts];
    PhotoModel *pm = [[PhotoModel alloc]init];
    pm.make_like = @"0";
    pm.image = imgURL ;
    pm.id = imgID ;
    pm.user_id = shared.user.id ;
    [photoArrs addObject:pm];
    shared.photosArr = photoArrs ;
    [_refreshBtn setHidden:YES];
    [_photosCollectionView setHidden:NO];
    [_photosCollectionView reloadData];
}
@end
