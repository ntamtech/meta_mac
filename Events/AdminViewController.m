//
//  AdminViewController.m
//  Events
//
//  Created by M.I.Kamashany on 2/22/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import "AdminViewController.h"
#import "AdminAnnouncementViewController.h"
#import "Helper.h"
#import "UIView+Toast.h"

@interface AdminViewController ()

@property (weak, nonatomic) IBOutlet UIView *adminView;
@end

@implementation AdminViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.adminView.layer.masksToBounds = YES ;
    self.adminView.layer.borderColor = [UIColor colorWithRed:34.0/255 green:98.0/255 blue:175.0/255 alpha:1].CGColor;
    self.adminView.layer.borderWidth = 1 ;
}

-(UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent ;
}

-(IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)showAdminAnnouncement:(id)sender {
    if (![Helper checkInternet]) {
            [self.view makeToast:@"No Internet Connection!"
                        duration:1.5
                        position:CSToastPositionBottom];
    }else{
        AdminAnnouncementViewController *destination = [self.storyboard instantiateViewControllerWithIdentifier:@"AdminAnnouncementViewController"];
        [self.navigationController pushViewController:destination animated:YES];
    }
}
@end
