//
//  User.h
//  Events
//
//  Created by M.I.Kamashany on 2/18/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"

@interface User : JSONModel


@property (nonatomic) NSString<Optional> *id ;
@property (nonatomic,strong)    NSString<Optional> *name ;
@property (nonatomic,strong)    NSString<Optional> *code ;
@property (nonatomic,strong)    NSString<Optional> *company ;
@property (nonatomic,strong)    NSString<Optional> *position ;
@property (nonatomic,strong)    NSString<Optional> *image ;
@property (nonatomic,strong)    NSString<Optional> *bio ;
@property (nonatomic,strong)    NSString<Optional> *admin ;
@property (nonatomic,strong)    NSString<Optional> *register_id ;
@property (nonatomic,strong)    NSString<Optional> *fb_link ;
@property (nonatomic,strong)    NSString<Optional> *twitter_link ;
@property (nonatomic,strong)    NSString<Optional> *linkedin_link ;
@property (nonatomic,strong)    NSString<Optional> *event_id ;
@property (nonatomic,strong)    NSString<Optional> *location ;
@property (nonatomic,strong)    NSString<Optional> *is_speaker ;

@property (nonatomic,strong)    NSString<Optional> *hospitalName ;
@property (nonatomic,strong)    NSString<Optional> *email ;
@property (nonatomic,strong)    NSString<Optional> *mobile ;
@property (nonatomic,strong)    NSString<Optional> *countryName ;

@end
