//
//  NewsFeedsHeader.m
//  Events
//
//  Created by M.I.Kamashany on 4/12/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import "NewsFeedsHeader.h"

@implementation NewsFeedsHeader

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        NSArray *nibs = [[NSBundle mainBundle] loadNibNamed:@"NewsFeedsHeader" owner:self options:nil];
        self = [nibs objectAtIndex:0];
    }
    return self ;
}

- (IBAction)showTwitterAccount:(id)sender {
   
}

- (IBAction)showLinkedINAccount:(id)sender {
   
}

@end
