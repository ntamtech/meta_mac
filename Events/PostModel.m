//
//  PostModel.m
//  Events
//
//  Created by M.I.Kamashany on 2/20/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import "PostModel.h"

@implementation PostModel


- (void)encodeWithCoder:(NSCoder *)encoder {
    //Encode properties, other class variables, etc
    [encoder encodeObject:self.id forKey:@"id"];
    [encoder encodeObject:self.user_id forKey:@"user_id"];
    [encoder encodeObject:self.username forKey:@"username"];
    [encoder encodeObject:self.post forKey:@"post"];
    [encoder encodeObject:self.user_image forKey:@"user_image"];
    [encoder encodeObject:self.post_date forKey:@"post_date"];
    [encoder encodeObject:self.make_like forKey:@"make_like"];
    [encoder encodeObject:self.comments forKey:@"comments"];
    [encoder encodeObject:self.likes forKey:@"likes"];
    [encoder encodeObject:self.last_likes forKey:@"last_likes"];
    [encoder encodeObject:self.poll_data forKey:@"poll_data"];
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        //decode properties, other class vars
        self.id= [decoder decodeObjectForKey:@"id"];
        self.user_id =[decoder decodeObjectForKey:@"user_id"];
        self.username =[decoder decodeObjectForKey:@"username"];
        self.post =[decoder decodeObjectForKey:@"post"];
        self.user_image= [decoder decodeObjectForKey:@"user_image"];
        self.post_date =[decoder decodeObjectForKey:@"post_date"];
        self.make_like =[decoder decodeObjectForKey:@"make_like"];
        self.comments =[decoder decodeObjectForKey:@"comments"];
        self.likes= [decoder decodeObjectForKey:@"likes"];
        self.last_likes =[decoder decodeObjectForKey:@"last_likes"];
        self.poll_data =[decoder decodeObjectForKey:@"poll_data"];
    }
    return self;
}


@end
