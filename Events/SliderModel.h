//
//  SliderModel.h
//  Events
//
//  Created by M.I.Kamashany on 2/20/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"

@interface SliderModel : JSONModel

@property (nonatomic) NSString<Optional> *id ;
@property (nonatomic,strong) NSString<Optional> *image ;

@end
