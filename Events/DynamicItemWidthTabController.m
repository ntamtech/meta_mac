//
//  DynamicItemWidthTabController.m
//  YPTabBarController
//
//  Created by 喻平 on 16/5/20.
//  Copyright © 2016年 YPTabBarController. All rights reserved.
//

#import "DynamicItemWidthTabController.h"
#import "FullAgendaListViewController.h"
#import "MyAgendaListViewController.h"
#import "SharedData.h"
#import "AgendaModel.h"

@interface DynamicItemWidthTabController ()
{
    SharedData *shared ;
}
@end

@implementation DynamicItemWidthTabController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    shared = [SharedData getSharedObject];
    CGSize screenSize = [UIScreen mainScreen].bounds.size;
    [self setTabBarFrame:CGRectMake(0, 0, screenSize.width, 54)
        contentViewFrame:CGRectMake(0, 64, screenSize.width, screenSize.height - 64)];
    ///self.tabBar.backgroundColor = [UIColor colorWithRed:180.0/255 green:22.0/255 blue:27.0/255 alpha:1];
    self.tabBar.backgroundColor = [UIColor lightGrayColor];
    
    self.tabBar.itemTitleColor = [UIColor whiteColor];
    self.tabBar.itemTitleSelectedColor = [UIColor whiteColor];
    self.tabBar.itemTitleFont = [UIFont boldSystemFontOfSize:11];
    self.tabBar.itemTitleSelectedFont = [UIFont boldSystemFontOfSize:13];
    self.tabBar.leadAndTrailSpace = 20;
    
    self.tabBar.itemFontChangeFollowContentScroll = YES;
//    self.tabBar.itemSelectedBgScrollFollowContent = YES;
    self.tabBar.itemColorChangeFollowContentScroll = YES;
    self.tabBar.selectedItem.backgroundColor = [UIColor darkGrayColor];
    
//    [self.tabBar setItemSelectedBgInsets:UIEdgeInsetsMake(0, 5, 2, 5) tapSwitchAnimated:YES];
    [self.tabBar setIndicatorInsets:UIEdgeInsetsMake(0, 5, 2, 5) tapSwitchAnimated:YES];
    [self.tabBar setScrollEnabledAndItemFitTextWidthWithSpacing:60];
    
    [self setContentScrollEnabledAndTapSwitchAnimated:NO];
    self.loadViewOfChildContollerWhileAppear = YES;
    
    [self initViewControllers];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)initViewControllers {
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    if (self.isFullAgenda) {
        NSMutableArray *sessionTabs = [[NSMutableArray alloc]init];
        int count = 0 ;
        for (AgendaModel *agenda in shared.allAgenda) {
            FullAgendaListViewController *controller1 = (FullAgendaListViewController *)[sb instantiateViewControllerWithIdentifier:@"FullAgendaListViewController"];
            controller1.selectedAgendaIndex = count ;
            controller1.yp_tabItemTitle = [NSString stringWithFormat:@"%@\n%@",agenda.day_number,agenda.event_date];
            controller1.sessionOfDay = agenda.sessions ;
            [sessionTabs addObject:controller1];
            count ++ ;
        }
        self.viewControllers = sessionTabs  ;
    }else{
        
        NSMutableArray *controllers = [[NSMutableArray alloc]init];
        for (AgendaModel *agenda in shared.myAgenda) {
            
            NSMutableArray *addedSession = [[NSMutableArray alloc]init];
            MyAgendaListViewController *controller1 = (MyAgendaListViewController *)[sb instantiateViewControllerWithIdentifier:@"MyAgendaListViewController"];
            controller1.yp_tabItemTitle = [NSString stringWithFormat:@"%@\n%@",agenda.day_number,agenda.event_date];
            controller1.agendaDay_number = agenda.day_number ;
            for (Session *session in agenda.sessions) {
                if ([session.add_to_agenda isEqualToString:@"1"]) {
                    [addedSession addObject:session];
                }
            }
            controller1.sessionOfDay = addedSession ;
            [controllers addObject:controller1];
        }
        self.viewControllers = controllers  ;
    }
}



@end
