//
//  AboutViewController.m
//  Events
//
//  Created by M.I.Kamashany on 2/11/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import "AboutViewController.h"
#import "ServiceHandler.h"
#import "MBProgressHUD.h"
#import "Helper.h"
#import "About.h"
#import "UIImageView+WebCache.h"
#import "SharedData.h"

@interface AboutViewController ()<ServiceHandlerDelegate>
{
    ServiceHandler *service ;
    UIButton *refreshBtn ;
    SharedData *shared ;
}

@property (nonatomic,weak) IBOutlet UIImageView *aboutAppImageView ;
@property (nonatomic,weak) IBOutlet UIWebView *webViewAboutApp ;
@end

@implementation AboutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    service = [[ServiceHandler alloc]init];
    service.delegate = self ;
    [self hideViews:YES];
    shared = [SharedData getSharedObject];
}

-(UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent ;
}

-(void)viewWillAppear:(BOOL)animated {
    if (shared.about == nil) {
        if (![Helper checkInternet]) {
            [self hideViews:YES];
            [self createRefreshBtn:@"No Internet Connection, Try again" enabled:YES];
        }else{
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            [service loadAboutData];
        }
    }else{
        [self hideViews:NO];
        //check content
        if (shared.about.content) {
            [self.webViewAboutApp loadHTMLString:shared.about.content baseURL:nil];
        }
        //check image
        if (shared.about.image) {
            [_aboutAppImageView setShowActivityIndicatorView:YES];
            [_aboutAppImageView setIndicatorStyle:UIActivityIndicatorViewStyleGray];
            [_aboutAppImageView sd_setImageWithURL:[NSURL URLWithString:shared.about.image] placeholderImage:nil];
        }
    }
}


#pragma mark -ServiceHandlerDelegate 

-(void)getAboutData:(About *)data {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [self hideViews:NO];
    shared.about = data ;
    //check content
    if (data.content) {
        [self.webViewAboutApp loadHTMLString:data.content baseURL:nil];
    }
    //check image
    if (data.image) {
        [_aboutAppImageView setShowActivityIndicatorView:YES];
        [_aboutAppImageView setIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [_aboutAppImageView sd_setImageWithURL:[NSURL URLWithString:data.image] placeholderImage:nil];
    }
}

-(void)requestFailWithError:(NSString *)err {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [self hideViews:YES];
    [self createRefreshBtn:err enabled:NO];
}

- (IBAction)showMenu:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)createRefreshBtn:(NSString *)title enabled:(BOOL)enabled{
    refreshBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    refreshBtn.frame = CGRectMake(0, self.view.frame.size.height/2, self.view.frame.size.width, 25);
    [refreshBtn setTitle:title forState:UIControlStateNormal];
    refreshBtn.enabled = enabled ;
    refreshBtn.titleLabel.textColor = [UIColor lightGrayColor];
    [refreshBtn addTarget:self
                          action:@selector(refresh:)
                forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:refreshBtn];
    [self.view bringSubviewToFront:refreshBtn];
}

-(IBAction)refresh:(id)sender {
    if ([Helper checkInternet]) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [refreshBtn removeFromSuperview];
        [service loadAboutData];
    }
}

-(void)hideViews:(BOOL)isHidden {
    self.aboutAppImageView.hidden = isHidden ;
    self.webViewAboutApp.hidden = isHidden ;
}

-(IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)downloadBrochure:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:shared.about.pdf_file]];
}

@end
