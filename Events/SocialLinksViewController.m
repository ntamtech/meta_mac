//
//  SocialLinksViewController.m
//  Events
//
//  Created by M.I.Kamashany on 3/20/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import "SocialLinksViewController.h"
#import "SocialTableViewCell.h"
#import "Helper.h"
#import "SharedData.h"
#import "ServiceHandler.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "SVProgressHUD.h"
#import "UIViewController+Alert.h"
#import <linkedin-sdk/LISDK.h>
#import <TwitterKit/TwitterKit.h>
#import "PostModel.h"
#import "MBProgressHUD.h"

@interface SocialLinksViewController ()<ServiceHandlerDelegate>
{
    SharedData *shared ;
    UIAlertController *alertController ;
    ServiceHandler *service ;
    NSString *imgprofileURL ;
    NSString *username ;
    NSString *company ;
    NSString *position ;
    NSString *location ;
    FBSDKLoginManager *login ;
}
@end

@implementation SocialLinksViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    shared = [SharedData getSharedObject];
    service = [[ServiceHandler alloc]init];
    service.delegate = self ;
    
    
    imgprofileURL = shared.user.image ;
    username = shared.user.name ;
    position = shared.user.position ;
    company = shared.user.company ;
    
    if (shared.user.location) {
        location = shared.user.location;
    }else{
        location = @"" ;
    }
    
    if (![FBSDKAccessToken currentAccessToken]){
        login = [[FBSDKLoginManager alloc]init];
    }
}

-(UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent ;
}

-(void)viewDidDisappear:(BOOL)animated {
    [FBSDKAccessToken setCurrentAccessToken:nil];
}
#pragma mark -TableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1 ;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellID = @"socialCell";
    SocialTableViewCell *cell = (SocialTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell == nil) {
        cell =[[SocialTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    
    switch (indexPath.row) {
        case 0:
            cell.iconImageView.image = [UIImage imageNamed:@"facebook-1"];
            cell.linkLabel.text = @"Facebook";
            break;
        case 1:
            cell.iconImageView.image = [UIImage imageNamed:@"linkedin-1"];
            cell.linkLabel.text = @"LinedIn" ;
            break;
        case 2:
            cell.iconImageView.image = [UIImage imageNamed:@"twitter-2"];
            cell.linkLabel.text = @"Twitter";
            break;
        default:
            break;
    }
    tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    return cell ;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (![Helper checkInternet]) {
        [self showAlert:@"No Internet Connection"];
    } else {
        switch (indexPath.row) {
            case 0:
                [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                [self integrateWithFacebook];
                break;
            case 1:
                [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                [self integrateWithLinkedIn];
                break;
            case 2:
                [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                [self integrateWithTwitter];
                break;
            default:
                break;
        }
    }
}


#pragma  mark - ServiceHandlerDelegate

-(void)accountImageEditSuccessfully:(NSString *)newURL {
    shared.user.image = imgprofileURL ;
    [service editProfile:[NSNumber numberWithInt:1] userID:[NSNumber numberWithInt:[shared.user.id intValue]] name:username position:position company:company location:location];
}

-(void)profileEditedSuccessfully {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    shared.user.name = username ;
    shared.user.position = position ;
    shared.user.location = location ;
    shared.user.company = company ;
    NSMutableArray *editedPosts = [[NSMutableArray alloc]init];
    
    for (PostModel *p in shared.postsArr) {
        if ([p.user_id isEqualToString:shared.user.id]) {
            p.user_image = imgprofileURL ;
            p.username = username ;
        }
        [editedPosts addObject:p];
    }
    shared.postsArr = editedPosts ;
    [self.navigationController popViewControllerAnimated:YES];
}


-(void)requestFailWithError:(NSString *)err {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [self showAlert:err];
}

-(IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma  mark - Social Networking integrations

-(void)integrateWithFacebook {
    
        [login
         logInWithReadPermissions: @[@"public_profile"]
         fromViewController:self
         handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
             if (error) {
                 [MBProgressHUD hideHUDForView:self.view animated:YES];
             } else if (result.isCancelled) {
                 [MBProgressHUD hideHUDForView:self.view animated:YES];
             } else {
                 if ([FBSDKAccessToken currentAccessToken]){
                     [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"name, picture.type(large), email,gender"}]
                      startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                          if (error){
                              [MBProgressHUD hideHUDForView:self.view animated:YES];
                              alertController = [UIAlertController alertControllerWithTitle:@"Login Failed" message:@"Please try again" preferredStyle:UIAlertControllerStyleAlert];
                              UIAlertAction *cancel = [UIAlertAction
                                                       actionWithTitle:@"Ok"
                                                       style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action)
                                                       {
                                                           [alertController dismissViewControllerAnimated:YES completion:nil];
                                                           
                                                       }];
                              [alertController addAction:cancel];
                              [self presentViewController:alertController animated:YES completion:nil];
                          }else{
                              username = shared.user.name;
                              imgprofileURL = [[[result objectForKey:@"picture"] objectForKey:@"data"] objectForKey:@"url"];
                              NSURL *imageURL = [NSURL URLWithString:imgprofileURL];
                              NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
                              NSString *imageBase64 = [imageData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
                              [service editProfileImage:[NSNumber numberWithInt:1] userID:[NSNumber numberWithInt:[shared.user.id intValue]] imageContent:imageBase64];
                          }
                      }];
                 }else{
                     [MBProgressHUD hideHUDForView:self.view animated:YES];
                 }
             }}];
}


-(void)integrateWithLinkedIn{
    NSArray *permissions = [NSArray arrayWithObjects:LISDK_BASIC_PROFILE_PERMISSION, nil];
    if ([LISDKSessionManager hasValidSession]) {
        [[LISDKAPIHelper sharedInstance] getRequest:@"https://api.linkedin.com/v1/people/~:(first-name,last-name,positions,location,picture-url)?format=json"
                                            success:^(LISDKAPIResponse *response) {
                                                NSData* data = [response.data dataUsingEncoding:NSUTF8StringEncoding];
                                                NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                                                username = shared.user.name;
                                                location = [[dictResponse objectForKey:@"location"] objectForKey:@"name"];
                                                NSArray  *positionsArr = [[dictResponse objectForKey:@"positions"] objectForKey:@"values"] ;
                                                NSDictionary *currentPosition = [positionsArr objectAtIndex:0];
                                                position = [currentPosition objectForKey:@"title"];
                                                
                                                company = [[currentPosition objectForKey:@"company"] objectForKey:@"name"];
                                                
                                                imgprofileURL = [dictResponse valueForKey:@"pictureUrl"];
                                                if (imgprofileURL) {
                                                    NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:imgprofileURL]];
                                                    NSString *imageBase64 = [imageData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
                                                    [service editProfileImage:[NSNumber numberWithInt:1] userID:[NSNumber numberWithInt:[shared.user.id intValue]] imageContent:imageBase64];
                                                } else {
                                                    [service editProfile:[NSNumber numberWithInt:1] userID:[NSNumber numberWithInt:[shared.user.id intValue]] name:username position:position company:company location:location];
                                                }
                                            }
                                              error:^(LISDKAPIError *apiError) {
                                                  NSInteger errorCode = apiError.code;
                                                  if (errorCode == 401) {
                                                      [LISDKSessionManager clearSession];
                                                      [MBProgressHUD hideHUDForView:self.view animated:YES];
                                                  }
        }];
    }else{
        [LISDKSessionManager createSessionWithAuth:permissions state:nil showGoToAppStoreDialog:NO successBlock:^(NSString *returnState)
         {
             [[LISDKAPIHelper sharedInstance] getRequest:@"https://api.linkedin.com/v1/people/~:(first-name,last-name,positions,location,picture-url)?format=json"
                                                 success:^(LISDKAPIResponse *response)
              {
                  NSData* data = [response.data dataUsingEncoding:NSUTF8StringEncoding];
                  NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                  username = shared.user.name;
                  location = [[dictResponse objectForKey:@"location"] objectForKey:@"name"];
                  NSArray  *positionsArr = [[dictResponse objectForKey:@"positions"] objectForKey:@"values"] ;
                  NSDictionary *currentPosition = [positionsArr objectAtIndex:0];
                  position = [currentPosition objectForKey:@"title"];
                  
                  company = [[currentPosition objectForKey:@"company"] objectForKey:@"name"];
                  
                  imgprofileURL = [dictResponse valueForKey:@"pictureUrl"];
                  if (imgprofileURL) {
                      NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:imgprofileURL]];
                      NSString *imageBase64 = [imageData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
                      [service editProfileImage:[NSNumber numberWithInt:1] userID:[NSNumber numberWithInt:[shared.user.id intValue]] imageContent:imageBase64];
                  } else {
                      [service editProfile:[NSNumber numberWithInt:1] userID:[NSNumber numberWithInt:[shared.user.id intValue]] name:username position:position company:company location:location];
                  }
              } error:^(LISDKAPIError *apiError) {
                  [MBProgressHUD hideHUDForView:self.view animated:YES];
                  NSLog(@"Error : %@", apiError);
              }];
         } errorBlock:^(NSError *error)
         {
             NSLog(@"%s","error called!");
             [MBProgressHUD hideHUDForView:self.view animated:YES];
         }];
    }
}

-(void)integrateWithTwitter {
    // Objective-C
    [[Twitter sharedInstance] logInWithCompletion:^(TWTRSession *session, NSError *error) {
        if (session) {
            TWTRAPIClient *client = [TWTRAPIClient clientWithCurrentUser];
            NSURLRequest *request = [client URLRequestWithMethod:@"GET"
                                                             URL:@"https://api.twitter.com/1.1/account/verify_credentials.json"
                                                      parameters:@{@"include_email": @"true", @"skip_status": @"true"}
                                                           error:nil];
            
            [client sendTwitterRequest:request completion:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                });
                NSDictionary *userDic = [NSJSONSerialization JSONObjectWithData:data
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:NULL];
                imgprofileURL = [userDic objectForKey:@"profile_image_url"];
                username = shared.user.name;
                NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:imgprofileURL]];
                NSString *imageBase64 = [imageData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
                [service editProfileImage:[NSNumber numberWithInt:1] userID:[NSNumber numberWithInt:[shared.user.id intValue]] imageContent:imageBase64];
            }];
        } else {
            dispatch_async(dispatch_get_main_queue(), ^{
                [MBProgressHUD hideHUDForView:self.view animated:YES];
            });
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            NSLog(@"error: %@", [error localizedDescription]);
        }
    }];
}
@end
