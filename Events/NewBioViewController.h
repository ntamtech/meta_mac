//
//  NewBioViewController.h
//  Events
//
//  Created by M.I.Kamashany on 3/14/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HPGrowingTextView.h"

@interface NewBioViewController : UIViewController<HPGrowingTextViewDelegate>{
    UIView *containerView;
    HPGrowingTextView *bioTextView;
}
@end
