//
//  NewBioViewController.m
//  Events
//
//  Created by M.I.Kamashany on 3/14/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import "NewBioViewController.h"
#import "MBProgressHUD.h"
#import "ServiceHandler.h"
#import "Helper.h"
#import "UIViewController+Alert.h"
#import "SharedData.h"

@interface NewBioViewController ()<ServiceHandlerDelegate> {
    ServiceHandler *service ;
    SharedData *shared ;
}

@end

@implementation NewBioViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    service = [[ServiceHandler alloc]init];
    service.delegate = self ;
    shared = [SharedData getSharedObject];
    
    //create uitextview
    
    CGRect frame = [UIScreen mainScreen].bounds ;
    bioTextView = [[HPGrowingTextView alloc] initWithFrame:CGRectMake(8, 75 , frame.size.width-16 , 200)];
    bioTextView.isScrollable = NO;
    bioTextView.contentInset = UIEdgeInsetsMake(0, 5, 0, 5);
    // you can also set the maximum height in points with maxHeight
//    bioTextView.maxHeight = 200.0f;
//    bioTextView.minHeight = 100.0f;
    bioTextView.returnKeyType = UIReturnKeyDefault; //just as an example
    bioTextView.font = [UIFont systemFontOfSize:14.0f];
    bioTextView.delegate = self;
    bioTextView.internalTextView.scrollIndicatorInsets = UIEdgeInsetsMake(5, 0, 1, 0);
    bioTextView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    bioTextView.placeholder = @"Add your biography here ...";

    // textView.text = @"test\n\ntest";
    // textView.animateHeightChange = NO; //turns off animation
    bioTextView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    
    // view hierachy
    [self.view addSubview:bioTextView];
}

-(void)viewWillAppear:(BOOL)animated {
    
    CGRect frame = [UIScreen mainScreen].bounds ;
    if (![shared.user.bio isEqualToString:@""]) {
        bioTextView.text = shared.user.bio ;
        bioTextView.minNumberOfLines = 7 ;
    }else{
        bioTextView.minNumberOfLines = 7 ;
    }
    bioTextView.maxNumberOfLines = 15 ;
}

-(IBAction)saveBio:(id)sender {
    if (![Helper checkInternet]) {
        [self showAlert:@"No Internet Connection!"];
    }else{
        [self.view endEditing:YES];
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [service editBio:[NSNumber numberWithInt:1] userID:[NSNumber numberWithInt:[shared.user.id intValue]] bio:bioTextView.text];
    }
}

-(void)bioEditedSuccessfully {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    shared.user.bio = bioTextView.text ;
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)requestFailWithError:(NSString *)err {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [self showAlert:err];
}

-(IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


@end
