//
//  UIViewController+Alert.h
//  Ntam Care
//
//  Created by M.I.Kamashany on 1/16/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (Alert)

-(void)showAlert:(NSString *)msg ;

@end
