//
//  CommentTableViewCell.h
//  Events
//
//  Created by M.I.Kamashany on 3/27/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PaddingLabel.h"


//chatting messages
@interface CommentTableViewCell : UITableViewCell

@property (nonatomic,weak) IBOutlet UIImageView *leftImageView ;
@property (nonatomic,weak) IBOutlet UIImageView *rightImageView ;

@property (nonatomic,weak) IBOutlet PaddingLabel *messageLabel ;
@property (nonatomic,weak) IBOutlet UILabel *dateLabel ;
@property (nonatomic,weak) IBOutlet UIView *bgView ;

@property (nonatomic,weak) IBOutlet NSLayoutConstraint *bgViewLeadingConstraint ;
@property (nonatomic,weak) IBOutlet NSLayoutConstraint *bgViewTrailingConstraint ;

@end
