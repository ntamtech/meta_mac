//
//  Session.m
//  Events
//
//  Created by M.I.Kamashany on 2/19/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import "Speaker.h"

@implementation Session


- (void)encodeWithCoder:(NSCoder *)encoder {
    //Encode properties, other class variables, etc
    [encoder encodeObject:self.id forKey:@"id"];
    [encoder encodeObject:self.session_name forKey:@"session_name"];
    [encoder encodeObject:self.session_date forKey:@"session_date"];
    [encoder encodeObject:self.start_time forKey:@"start_time"];
    [encoder encodeObject:self.end_time forKey:@"end_time"];
    [encoder encodeObject:self.session_tags forKey:@"session_tags"];
    [encoder encodeObject:self.desc forKey:@"desc"];
    [encoder encodeObject:self.has_rate forKey:@"has_rate"];
    [encoder encodeObject:self.venue forKey:@"venue"];
    [encoder encodeObject:self.physical_address forKey:@"physical_address"];
    [encoder encodeObject:self.list_of_speaker forKey:@"list_of_speaker"];
    [encoder encodeObject:self.no_of_interested forKey:@"no_of_interested"];
    [encoder encodeObject:self.session_rate_of_user forKey:@"session_rate_of_user"];
    [encoder encodeObject:self.add_to_agenda forKey:@"add_to_agenda"];
    [encoder encodeObject:self.session_comments forKey:@"session_comments"];
    [encoder encodeObject:self.session_likes forKey:@"session_likes"];
    [encoder encodeObject:self.make_like forKey:@"make_like"];
    [encoder encodeObject:self.last_likes forKey:@"last_likes"];
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        //decode properties, other class vars
        self.id= [decoder decodeObjectForKey:@"id"];
        self.session_name =[decoder decodeObjectForKey:@"session_name"];
        self.session_date =[decoder decodeObjectForKey:@"session_date"];
        self.start_time =[decoder decodeObjectForKey:@"start_time"];
        self.end_time= [decoder decodeObjectForKey:@"end_time"];
        self.session_tags =[decoder decodeObjectForKey:@"session_tags"];
        self.desc =[decoder decodeObjectForKey:@"desc"];
        self.has_rate =[decoder decodeObjectForKey:@"has_rate"];
        self.venue= [decoder decodeObjectForKey:@"venue"];
        self.physical_address =[decoder decodeObjectForKey:@"physical_address"];
        self.list_of_speaker =[decoder decodeObjectForKey:@"list_of_speaker"];
        self.no_of_interested= [decoder decodeObjectForKey:@"no_of_interested"];
        self.session_rate_of_user =[decoder decodeObjectForKey:@"session_rate_of_user"];
        self.add_to_agenda =[decoder decodeObjectForKey:@"add_to_agenda"];
        self.session_comments =[decoder decodeObjectForKey:@"session_comments"];
        self.session_likes= [decoder decodeObjectForKey:@"session_likes"];
        self.make_like =[decoder decodeObjectForKey:@"make_like"];
        self.last_likes =[decoder decodeObjectForKey:@"last_likes"];
    }
    return self;
}
@end
