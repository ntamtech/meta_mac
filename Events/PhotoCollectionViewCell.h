//
//  PhotoCollectionViewCell.h
//  Events
//
//  Created by M.I.Kamashany on 3/24/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PhotoCollectionViewCellDelegate <NSObject>

-(void)likePhoto:(NSIndexPath *)selectedIndex ;

@end

@interface PhotoCollectionViewCell : UICollectionViewCell

@property (nonatomic,weak) id<PhotoCollectionViewCellDelegate> delegate ;
@property (nonatomic,weak) IBOutlet UIImageView *photoImageView ;
@property (nonatomic,weak) IBOutlet UIImageView *likeStatusPhotoImageView ;
@property (nonatomic,weak) IBOutlet UIButton *likeBtn ;
@property (nonatomic,strong) NSIndexPath *indexPath ;

-(IBAction)likePhoto:(id)sender ;
@end
