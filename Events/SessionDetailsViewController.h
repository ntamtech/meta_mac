//
//  SessionDetailsViewController.h
//  Events
//
//  Created by M.I.Kamashany on 2/27/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Speaker.h"

@interface SessionDetailsViewController : UIViewController

@property (nonatomic , strong) Session *selectedSession ;
@property (nonatomic,assign) BOOL isComingFromSpeakers ;
@property (nonatomic,assign) int selectedAgendaIndex ;
@property (nonatomic,assign) int selectedSessionIndex ;
@property (nonatomic,assign) NSString *agendaDay_number ;
@property (nonatomic,assign) NSString *isFromFullAgenda ; // 0 = fullAgenda 1 = myAgenda
@property (nonatomic,assign) int selectedSpeakerIndex ;
@property (nonatomic,strong) NSIndexPath *selectedSessionIndexPath ;
@end
