//
//  AnswerViewTableViewCell.h
//  Events
//
//  Created by M.I.Kamashany on 3/16/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AnswerViewTableViewCell : UITableViewCell

@property (nonatomic,weak) IBOutlet UIImageView *iconImageView ;
@property (nonatomic,weak) IBOutlet  UILabel *answerLabel ;

@end
