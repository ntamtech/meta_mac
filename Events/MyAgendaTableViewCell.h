//
//  MyAgendaTableViewCell.h
//  Events
//
//  Created by M.I.Kamashany on 3/6/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MyAgendaTableViewCellDelegate <NSObject>

-(void)likeSession:(NSIndexPath *)selectedIndex ;
-(void)showCommentsAtIndex:(NSIndexPath *)selectedIndex ;

@end

@interface MyAgendaTableViewCell : UITableViewCell


@property (nonatomic,weak) id<MyAgendaTableViewCellDelegate> delegate ;
@property (weak, nonatomic) IBOutlet UILabel *sessionDescLabel;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;
@property (weak, nonatomic) IBOutlet UILabel *numOfInterested;
@property (weak, nonatomic) IBOutlet UIButton *likesCountBtn;
@property (weak, nonatomic) IBOutlet UIButton *commentsCountBtn;
@property (nonatomic,strong) NSIndexPath *selectedIndexPath ;
@property (weak, nonatomic) IBOutlet UILabel *startTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *endTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *speakerFixedLabel;
@property (weak, nonatomic) IBOutlet UILabel *speakerNameLabel;

-(IBAction)likeSession:(id)sender ;
-(IBAction)addCommentOnSession:(id)sender ;

@end
