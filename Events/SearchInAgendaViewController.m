//
//  SearchInAgendaViewController.m
//  Events
//
//  Created by M.I.Kamashany on 9/28/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import "SearchInAgendaViewController.h"
#import "SharedData.h"
#import "Helper.h"
#import "UIView+Toast.h"
#import "ServiceHandler.h"

@interface SearchInAgendaViewController ()<UITableViewDelegate,UITableViewDataSource,ServiceHandlerDelegate>
{
    SharedData *shared ;
    NSString *keyWord ;
    int selectedRow ;
    ServiceHandler *service ;
}
@property (weak, nonatomic) IBOutlet UITableView *dataTableView;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedControl;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityLoading;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *searchLoading;
@property (weak, nonatomic) IBOutlet UIButton *searchBtn;
@property (weak,nonatomic) NSMutableArray *data ;

@end

@implementation SearchInAgendaViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    shared = [SharedData getSharedObject];
    _data = shared.venuesArr ;
    keyWord = @"" ;
    selectedRow = 1000 ;
    service = [[ServiceHandler alloc]init];
    service.delegate = self ;
    [_activityLoading stopAnimating];
    [_searchLoading stopAnimating];
    _searchBtn.layer.cornerRadius = 6.0;
    _searchBtn.layer.masksToBounds = YES;
}

-(void)viewWillAppear:(BOOL)animated {
    if (_data == nil) {
        self.dataTableView.hidden = YES;
        if ([Helper checkInternet]) {
            [_activityLoading startAnimating];
            [service loadTracksAndVenues:[NSNumber numberWithInt:1]];
        }else{
            [self.view makeToast:@"No Internet Connection!"
                        duration:1
                        position:CSToastPositionBottom];
        }
    }
}

- (IBAction)switch:(id)sender {
    selectedRow = 1000 ;
    if (_segmentedControl.selectedSegmentIndex == 0) {
        _data = shared.venuesArr ;
        [_dataTableView reloadData];
    } else if(_segmentedControl.selectedSegmentIndex == 1) {
        _data = shared.tracksArr ;
        [_dataTableView reloadData];
    }
}
- (IBAction)search:(id)sender {
    
    if ([Helper checkInternet]){
        if (selectedRow == 1000 || [keyWord isEqualToString:@""]) {
            [self.view makeToast:@"Select which track or venue to search!"
                        duration:1.5
                        position:CSToastPositionBottom];
        }else{
            [_searchLoading startAnimating];
            [_searchBtn setUserInteractionEnabled:NO];
            [service searchInAgendaBy:[NSNumber numberWithInt:1] andSearchKey:keyWord andUserID:[NSNumber numberWithInt:[shared.user.id intValue]]];
        }
    }else{
        [self.view makeToast:@"No Internet Connection!"
                    duration:1
                    position:CSToastPositionBottom];
    }
}

-(void)getFullAgenda:(NSMutableArray *)fullAgendaArr {
    [_searchBtn setUserInteractionEnabled:YES];
    [_searchLoading stopAnimating];
    if (fullAgendaArr == nil) {
        [self.view makeToast:@"No sessions!"
                    duration:1.5
                    position:CSToastPositionBottom];
    } else if (fullAgendaArr.count == 0) {
        [self.view makeToast:@"No sessions!"
                    duration:1.5
                    position:CSToastPositionBottom];
    }else{
        
    }
    
}

-(void)requestFailWithError:(NSString *)err {
    [_searchBtn setUserInteractionEnabled:YES];
    [self.view makeToast:err
                duration:1
                position:CSToastPositionBottom];
}

- (IBAction)cancel:(id)sender {
    [self dismissViewControllerAnimated:NO completion:nil];
}

#pragma mark -TableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1 ;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (_data == nil) {
        return 0 ;
    } else {
        return _data.count ;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellID = @"cell";
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell == nil) {
        cell =[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.textLabel.font = [UIFont systemFontOfSize:12.0];
    if (selectedRow == indexPath.row){
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }else{
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    cell.textLabel.text = _data[indexPath.row];
    tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    return cell ;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (selectedRow == indexPath.row){
        selectedRow = 1000 ;
        keyWord = @"";
        [self.dataTableView reloadData];
    }else{
        selectedRow = (int)indexPath.row ;
        keyWord = cell.textLabel.text;
        [self.dataTableView reloadData];
    }
}

-(void)loadVenuesAndTracksSuccessfully {
    _data = shared.venuesArr ;
    _dataTableView.hidden = NO;
    [_dataTableView reloadData];
}

@end
