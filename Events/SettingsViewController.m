//
//  SettingsViewController.m
//  Events
//
//  Created by M.I.Kamashany on 2/11/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import "SettingsViewController.h"
#import "SettingsTableViewCell.h"
#import "SharedData.h"
#import "LoginViewController.h"
#import "ProfileViewController.h"
#import "ServiceHandler.h"
#import "Helper.h"

@interface SettingsViewController ()<UITableViewDelegate,UITableViewDataSource,ServiceHandlerDelegate>
{
    ServiceHandler *service ;
    SharedData *shared ;
}

@end

@implementation SettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    service = [[ServiceHandler alloc]init];
    service.delegate = self ;
    shared = [SharedData getSharedObject];
}

-(UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent ;
}

#pragma mark -TableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellID = @"settingsCell";
    SettingsTableViewCell *cell = (SettingsTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell == nil) {
        cell =[[SettingsTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    if (indexPath.row == 0) {
        cell.settingsIcon.image = [UIImage imageNamed:@"profile-image"];
        cell.settingsTitleLabel.text = @"My Profile";
    }else{
        cell.settingsIcon.image = [UIImage imageNamed:@"logout-image"];
        cell.settingsTitleLabel.text = @"Sign Out";
    }
    tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    if (indexPath.row == 1) {
        tableView.separatorInset = UIEdgeInsetsZero ;
    }
    return cell ;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.row) {
        case 0:{
                ProfileViewController *destination = (ProfileViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
                [self.navigationController pushViewController:destination animated:YES];
            }
            break;
        case 1:{
                NSString *title = @"";
                NSString *subtitle = @"Are you sure to logout?";
                UIAlertController *alertController=   [UIAlertController
                                                       alertControllerWithTitle:title
                                                       message:subtitle
                                                       preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *ok = [UIAlertAction
                                     actionWithTitle:@"Ok"
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                                         [SharedData deleteAllCachedData];
                                         if ([Helper checkInternet]) {
                                            [service logoutUser:[NSNumber numberWithInt:[shared.user.id intValue]]];
                                     }
//                                         }else{
//                                             [[UIApplication sharedApplication] unregisterForRemoteNotifications];
//                                         }
                                         LoginViewController *destination = (LoginViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                                         [self.navigationController pushViewController:destination animated:YES];
                                     }];
                UIAlertAction *cancel = [UIAlertAction
                                         actionWithTitle:@"Cancel"
                                         style:UIAlertActionStyleDestructive
                                         handler:^(UIAlertAction * action)
                                         {
                                             [alertController dismissViewControllerAnimated:YES completion:nil];
                                             
                                         }];
                [alertController addAction:cancel];
                [alertController addAction:ok];
                [self presentViewController:alertController animated:YES completion:nil];
            }
            break;
        default:
            break;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 75.5 ;
}

-(IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


-(void)logoutSuccessfully {
    
}
@end
