//
//  PhotoCollectionViewCell.m
//  Events
//
//  Created by M.I.Kamashany on 3/24/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import "PhotoCollectionViewCell.h"

@implementation PhotoCollectionViewCell


-(IBAction)likePhoto:(id)sender {
    [self.delegate likePhoto:self.indexPath];
}
@end
