//
//  EditBasicUserDataViewController.m
//  Events
//
//  Created by M.I.Kamashany on 3/18/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import "EditBasicUserDataViewController.h"
#import "ServiceHandler.h"
#import "SharedData.h"
#import "Helper.h"
#import "MBProgressHUD.h"
#import "UIViewController+Alert.h"

@interface EditBasicUserDataViewController ()<ServiceHandlerDelegate>
{
    ServiceHandler *service ;
    SharedData *shared ;
}
@property (nonatomic,weak) IBOutlet UITextField *nameTF ;
@property (nonatomic,weak) IBOutlet UITextField *positionTitleTF ;
@property (nonatomic,weak) IBOutlet UITextField *companyTF ;
@property (nonatomic,weak) IBOutlet UITextField *locationTF ;

@end

@implementation EditBasicUserDataViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    service = [[ServiceHandler alloc]init];
    service.delegate = self ;
    shared = [SharedData getSharedObject];
}

-(void)viewWillAppear:(BOOL)animated {
    if (![shared.user.name isEqualToString:@""]) {
        _nameTF.text = shared.user.name ;
    }
    
    if (![shared.user.position isEqualToString:@""]) {
        _positionTitleTF.text = shared.user.position ;
    }
    
    if (![shared.user.company isEqualToString:@""]) {
        _companyTF.text = shared.user.company ;
    }
    
    
    _nameTF.layer.borderWidth = 1 ;
    _nameTF.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _positionTitleTF.layer.borderWidth = 1 ;
    _positionTitleTF.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _companyTF.layer.borderWidth = 1 ;
    _companyTF.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _locationTF.layer.borderWidth = 1 ;
    _locationTF.layer.borderColor = [UIColor lightGrayColor].CGColor;
}


-(IBAction)saveBasicInfo:(id)sender {
    if (![Helper checkInternet]) {
        [self showAlert:@"No Internet Connection!"];
    }else{
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [service editProfile:[NSNumber numberWithInt:1] userID:[NSNumber numberWithInt:[shared.user.id intValue]] name:_nameTF.text position:_positionTitleTF.text company:_companyTF.text location:_locationTF.text];
    }
}

-(IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - ServiceHandlerDelegate

-(void)profileEditedSuccessfully {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    shared.user.name = _nameTF.text ;
    shared.user.position = _positionTitleTF.text ;
    shared.user.company = _companyTF.text ;
    shared.user.location = _locationTF.text ;
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)requestFailWithError:(NSString *)err {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [self showAlert:err];
}

@end
