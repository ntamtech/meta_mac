//
//  EditSocailLinksViewController.m
//  Events
//
//  Created by M.I.Kamashany on 3/18/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import "EditSocailLinksViewController.h"
#import "ServiceHandler.h"
#import "SharedData.h"
#import "Helper.h"
#import "MBProgressHUD.h"
#import "UIViewController+Alert.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "UIViewController+Alert.h"
#import <linkedin-sdk/LISDK.h>
#import <TwitterKit/TwitterKit.h>

@interface EditSocailLinksViewController ()<ServiceHandlerDelegate>
{
    ServiceHandler *service ;
    SharedData *shared ;
    FBSDKLoginManager *login ;
    UIAlertController *alertController ;
    NSString *facebookLink ;
    NSString *linkedInLink ;
    NSString *twitterLink ;
}

@property (nonatomic,weak) IBOutlet UITextField *facebookTF ;
@property (nonatomic,weak) IBOutlet UITextField *linkedInTF ;
@property (nonatomic,weak) IBOutlet UITextField *twitterTF ;

@end

@implementation EditSocailLinksViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    service = [[ServiceHandler alloc]init];
    service.delegate = self ;
    shared = [SharedData getSharedObject];
    
    
    if (shared.user.fb_link == nil){
        facebookLink = @"" ;
    }else{
        facebookLink = shared.user.fb_link;
    }
    
    if (shared.user.twitter_link == nil){
        twitterLink = @"" ;
    }else{
        twitterLink = shared.user.twitter_link;
    }
    
    if (shared.user.linkedin_link == nil){
        linkedInLink = @"";
    }else{
        linkedInLink = shared.user.linkedin_link;
    }
    
    if (![FBSDKAccessToken currentAccessToken]){
        login = [[FBSDKLoginManager alloc]init];
    }
}

-(void)viewWillAppear:(BOOL)animated {
    if (![facebookLink isEqualToString:@""]) {
        _facebookTF.text = facebookLink ;
    }
    
    if (![linkedInLink isEqualToString:@""]) {
        _linkedInTF.text = linkedInLink ;
    }
    
    if (![twitterLink isEqualToString:@""]) {
        _twitterTF.text = twitterLink ;
    }
    _facebookTF.layer.borderWidth = 1 ;
    _facebookTF.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _linkedInTF.layer.borderWidth = 1 ;
    _linkedInTF.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _twitterTF.layer.borderWidth = 1 ;
    _twitterTF.layer.borderColor = [UIColor lightGrayColor].CGColor;
}

-(void)viewDidDisappear:(BOOL)animated {
    [FBSDKAccessToken setCurrentAccessToken:nil];
}

-(IBAction)saveSocialLinks:(id)sender {
    if (![Helper checkInternet]) {
        [self showAlert:@"No Internet Connection!"];
    }else{
        
        if (!_facebookTF.hasText && !_linkedInTF.hasText && !_twitterTF.hasText) {
            [self showAlert:@"No Links"];
            return;
        }else{
            [self.view endEditing:YES];
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            [service editSocialLinks:[NSNumber numberWithInt:1] userID:[NSNumber numberWithInt:[shared.user.id intValue]] fb_link:facebookLink twitter_link:twitterLink linkedin_link:linkedInLink];
        }
    }
}

-(IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - ServiceHandlerDelegate

-(void)socialLinksEditedSuccessfully {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    shared.user.fb_link = _facebookTF.text ;
    shared.user.linkedin_link = _linkedInTF.text ;
    shared.user.twitter_link = _twitterTF.text ;
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)requestFailWithError:(NSString *)err {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [self showAlert:err];
}


- (BOOL) validateUrl: (NSString *) candidate {
    NSString *urlRegEx =
    @"(http|https)://((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+";
    NSPredicate *urlTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", urlRegEx];
    BOOL isValid = [urlTest evaluateWithObject:candidate];
    return isValid ;
}

- (IBAction)getFacebookLink:(id)sender {
    [self integrateWithFacebook];
}

- (IBAction)getLinkedInLink:(id)sender {
    [self integrateWithLinkedIn];
}
- (IBAction)getTwitterLink:(id)sender {
    [self integrateWithTwitter];
}



#pragma  mark - Social Networking integrations

-(void)integrateWithFacebook {
    
    [login
     logInWithReadPermissions: @[@"public_profile"]
     fromViewController:self
     handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
         if (error) {
             [MBProgressHUD hideHUDForView:self.view animated:YES];
         } else if (result.isCancelled) {
             [MBProgressHUD hideHUDForView:self.view animated:YES];
         } else {
             if ([FBSDKAccessToken currentAccessToken]){
                 [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                 [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"name, picture.type(large), email,gender"}]
                  startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                      if (error){
                          [MBProgressHUD hideHUDForView:self.view animated:YES];
                          alertController = [UIAlertController alertControllerWithTitle:@"Login Failed" message:@"Please try again" preferredStyle:UIAlertControllerStyleAlert];
                          UIAlertAction *cancel = [UIAlertAction
                                                   actionWithTitle:@"Ok"
                                                   style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action)
                                                   {
                                                       [alertController dismissViewControllerAnimated:YES completion:nil];
                                                       
                                                   }];
                          [alertController addAction:cancel];
                          [self presentViewController:alertController animated:YES completion:nil];
                      }else{
                          [MBProgressHUD hideHUDForView:self.view animated:YES];
                          _facebookTF.text = [NSString stringWithFormat:@"https://www.facebook.com/%@",[result objectForKey:@"id"] ];
                          facebookLink = _facebookTF.text;
                      }
                  }];
             }else{
                 [MBProgressHUD hideHUDForView:self.view animated:YES];
             }
         }}];
}


-(void)integrateWithLinkedIn{
    NSArray *permissions = [NSArray arrayWithObjects:LISDK_BASIC_PROFILE_PERMISSION, nil];
    if ([LISDKSessionManager hasValidSession]) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [[LISDKAPIHelper sharedInstance] getRequest:@"https://api.linkedin.com/v1/people/~:(public-profile-url)?format=json"
                                            success:^(LISDKAPIResponse *response) {
                                                dispatch_async(dispatch_get_main_queue(), ^{
                                                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                                                    NSData *data = [response.data dataUsingEncoding:NSUTF8StringEncoding];
                                                    NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                                                    _linkedInTF.text = [NSString stringWithFormat:@"%@",[dictResponse objectForKey:@"publicProfileUrl"]];
                                                    linkedInLink = _linkedInTF.text;
                                                });
                                            }
                                              error:^(LISDKAPIError *apiError) {
                                                  NSInteger errorCode = apiError.code;
                                                  if (errorCode == 401) {
                                                      [LISDKSessionManager clearSession];
                                                  }
                                              }];
    }else{
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [LISDKSessionManager createSessionWithAuth:permissions state:nil showGoToAppStoreDialog:NO successBlock:^(NSString *returnState)
         {
             [[LISDKAPIHelper sharedInstance] getRequest:@"https://api.linkedin.com/v1/people/~:(public-profile-url)?format=json"
                                                 success:^(LISDKAPIResponse *response) {
                                                     dispatch_async(dispatch_get_main_queue(), ^{
                                                         [MBProgressHUD hideHUDForView:self.view animated:YES];
                                                         NSData *data = [response.data dataUsingEncoding:NSUTF8StringEncoding];
                                                         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                                                         _linkedInTF.text = [NSString stringWithFormat:@"%@",[dictResponse objectForKey:@"publicProfileUrl"]];
                                                         linkedInLink = _linkedInTF.text;
                                                     });
                                                 } error:^(LISDKAPIError *apiError) {
                                                     [MBProgressHUD hideHUDForView:self.view animated:YES];
                                                     NSLog(@"Error : %@", apiError);
                                                 }];
         } errorBlock:^(NSError *error)
         {
             [MBProgressHUD hideHUDForView:self.view animated:YES];
             NSLog(@"%s","error called!");
         }];
    }
    
}

-(void)integrateWithTwitter {
    // Objective-C
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[Twitter sharedInstance] logInWithCompletion:^(TWTRSession *session, NSError *error) {
        if (session) {
            TWTRAPIClient *client = [TWTRAPIClient clientWithCurrentUser];
            NSURLRequest *request = [client URLRequestWithMethod:@"GET"
                                                             URL:@"https://api.twitter.com/1.1/account/verify_credentials.json"
                                                      parameters:@{@"include_entities":@"true",@"include_email": @"true", @"skip_status": @"true"}
                                                           error:nil];
            
            [client sendTwitterRequest:request completion:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                    NSDictionary *userDic = [NSJSONSerialization JSONObjectWithData:data
                                                                            options:NSJSONReadingAllowFragments
                                                                              error:NULL];
                    NSString *screen_name = [userDic objectForKey:@"screen_name"];
                    _twitterTF.text = [NSString stringWithFormat:@"www.twitter.com/%@",screen_name];
                    twitterLink = _twitterTF.text;
                });
            }];
        } else {
            dispatch_async(dispatch_get_main_queue(), ^{
                [MBProgressHUD hideHUDForView:self.view animated:YES];
            });
        }
    }];
}

@end
