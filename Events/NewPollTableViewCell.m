//
//  NewPollTableViewCell.m
//  Events
//
//  Created by M.I.Kamashany on 3/9/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import "NewPollTableViewCell.h"

@implementation NewPollTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        NSArray *nibs = [[NSBundle mainBundle] loadNibNamed:@"NewPollTableViewCell" owner:self options:nil];
        self = [nibs objectAtIndex:0];
    }
    return self ;
}


- (IBAction)deletePoll:(id)sender {
    [self.delegate deletePollByIndexPath:self.cellIndexPath];
}
@end
