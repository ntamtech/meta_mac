//
//  AdminAnnouncementViewController.m
//  Events
//
//  Created by M.I.Kamashany on 2/22/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import "AdminAnnouncementViewController.h"
#import "UITextView+Placeholder.h"
#import "SideBarViewController.h"
#import "AnnouncementTypeTableViewCell.h"
#import "ServiceHandler.h"
#import "Helper.h"
#import "AnnouncementType.h"
#import "MBProgressHUD.h"
#import "UIViewController+Alert.h"

@interface AdminAnnouncementViewController ()<UITableViewDelegate,UITableViewDataSource,ServiceHandlerDelegate>
{
    ServiceHandler *service ;
    NSMutableArray *announcementTypes ;
    NSMutableArray *selectedTypes ;
    
    //views
    
    UILabel *EventsAdminAccessOnly_lbl ;
    UILabel *chooseReceipients_lbl ;
    UITableView *typesTableView ;
    UILabel *composeMessage_lbl ;
    UILabel *title_lbl ;
}
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView ;
@property (weak, nonatomic) IBOutlet UIView *containerView ;
@property (weak, nonatomic) IBOutlet UITableView *typesTableView ;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewheightConstraint ;


- (IBAction)send:(id)sender;
- (IBAction)save:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *saveBtn;
@property (weak, nonatomic) IBOutlet UIButton *sendBtn;

@property (weak, nonatomic) IBOutlet UIView *messageTitleView;
@property (weak, nonatomic) IBOutlet UITextField *messageTitleTF;
@property (weak, nonatomic) IBOutlet UITextView *messageContentTV;

@end

@implementation AdminAnnouncementViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    announcementTypes = [[NSMutableArray alloc]init];
    selectedTypes = [[NSMutableArray alloc]init];
    service = [[ServiceHandler alloc]init];
    service.delegate = self ;
    self.typesTableView.separatorStyle = UITableViewCellSeparatorStyleNone ;
}

-(void)viewWillAppear:(BOOL)animated {
//    self.typesTableView.hidden = YES ;
    _containerView.hidden = YES ;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [service loadAnnouncementTypes:[NSNumber numberWithInt:1]];
    dispatch_async(dispatch_get_main_queue(), ^{
        _scrollView.contentSize = CGSizeMake(0, _containerView.bounds.size.height);
    });
}

-(UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent ;
}

-(void)viewDidAppear:(BOOL)animated {
   
}
-(void)getAllAnnouncementTypes:(NSMutableArray *)types {
    _containerView.hidden = NO ;
    announcementTypes = types;
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [self.typesTableView reloadData];
    //change the tableview height based the number of rows
    float height = announcementTypes.count * 62 ;
    _tableViewheightConstraint.constant = height ;
    _containerView.frame = CGRectMake(0, 0, _containerView.bounds.size.width, 379 + height);
    dispatch_async(dispatch_get_main_queue(), ^{
        _scrollView.contentSize = CGSizeMake(_containerView.bounds.size.width, _containerView.bounds.size.height);
    });
    [self.view layoutIfNeeded];
}

-(void)setupChildViews {
    self.messageContentTV.layer.borderColor = [UIColor colorWithRed:59.0/255 green:119.0/255 blue:183.0/255 alpha:1].CGColor;
    self.messageContentTV.layer.borderWidth = 1 ;
    
    self.messageTitleView.layer.borderColor = [UIColor colorWithRed:59.0/255 green:119.0/255 blue:183.0/255 alpha:1].CGColor;
    self.messageTitleView.layer.borderWidth = 1 ;
    
    NSDictionary *attrs = @ {
    NSFontAttributeName: [UIFont systemFontOfSize:13],
    };
    self.messageContentTV.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Message content to be pushed to all selected users." attributes:attrs];
    self.messageContentTV.placeholderColor = [UIColor lightGrayColor];
    
    self.saveBtn.layer.borderWidth = 1 ;
    self.saveBtn.layer.borderColor = [UIColor grayColor].CGColor;
}



-(IBAction)showMenu:(id)sender {
    SideBarViewController *destination = (SideBarViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"SideBarViewController"];
    [self.navigationController pushViewController:destination animated:YES];
}

-(IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}



#pragma mark - UITableView Delegate & DataSource


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return announcementTypes.count  ;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellID = @"announcementTypeCell";
    AnnouncementTypeTableViewCell *cell = (AnnouncementTypeTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell == nil) {
        cell =[[AnnouncementTypeTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    AnnouncementType *type = [announcementTypes objectAtIndex:indexPath.row];
    cell.typeLabel.text = type.name ;
    BOOL isSelected = NO ;
    for (NSDictionary *typeDic in selectedTypes) {
        if ([[typeDic objectForKey:@"target_id"] isEqualToString:type.id]) {
            cell.checkBoxImageView.image = [UIImage imageNamed:@"Checkbox-10"];\
            cell.isChecked = YES;
            isSelected = YES ;
            break ;
        }
    }
    if (!isSelected) {
        cell.isChecked = NO;
        cell.checkBoxImageView.image = [UIImage imageNamed:@"Admin-rectangle-10-copy-2-EC"];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone ;
    return cell ;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 62 ;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    AnnouncementType *selectedType = [announcementTypes objectAtIndex:indexPath.row];
    AnnouncementTypeTableViewCell *selectedCell = (AnnouncementTypeTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    if (selectedCell.isChecked){
        selectedCell.isChecked = NO ;
        selectedCell.checkBoxImageView.image = [UIImage imageNamed:@"Admin-rectangle-10-copy-2-EC"];
        int count = 0 ;
        for (NSDictionary *typeDic in selectedTypes) {
            if ([[typeDic objectForKey:@"target_id"] isEqualToString:selectedType.id]) {
                [selectedTypes removeObjectAtIndex:count];
                break ;
            }
            count ++ ;
        }
    }else{
        selectedCell.isChecked = YES ;
        selectedCell.checkBoxImageView.image = [UIImage imageNamed:@"Checkbox-10"];
        [selectedTypes addObject:@{@"target_id":selectedType.id}];
    }
}

#pragma mark - Action

- (IBAction)send:(id)sender {
    if (![Helper checkInternet]) {
        [self showAlert:@"No Internet Connection!"];
    } else {
        if (selectedTypes.count == 0) {
            [self showAlert:@"Select Announcement Type first"];
        } else {
            if ([_messageTitleTF.text isEqualToString:@""]) {
                [self showAlert:@"Enter Announcement title first"];
            } else {
                if ([_messageContentTV.text isEqualToString:@""]) {
                    [self showAlert:@"Enter Announcement body first"];
                } else {
                    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                    NSError *error = nil ;
                    NSDictionary *dic = @{@"targets":selectedTypes};
                    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dic
                                                                       options:NSJSONWritingPrettyPrinted error:&error];
                    NSString  *json = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
                    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                    [service sendNotification:[NSNumber numberWithInt:1] title:_messageTitleTF.text body:_messageContentTV.text isDraft:[NSNumber numberWithInt:0] notificationTarget:json];
                }
            }
        }
    }
}


- (IBAction)save:(id)sender {
    if (![Helper checkInternet]) {
        [self showAlert:@"No Internet Connection!"];
    } else {
        
        if (selectedTypes.count == 0) {
            [self showAlert:@"Select Announcement Type first"];
        } else {
            if ([_messageTitleTF.text isEqualToString:@""]) {
                [self showAlert:@"Enter Announcement title first"];
            } else {
                if ([_messageContentTV.text isEqualToString:@""]) {
                    [self showAlert:@"Enter Announcement body first"];
                } else {
                    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                    NSError *error = nil ;
                    NSDictionary *dic = @{@"targets":selectedTypes};
                    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dic
                                                                       options:NSJSONWritingPrettyPrinted error:&error];
                    NSString  *json = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
                    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                    [service sendNotification:[NSNumber numberWithInt:1] title:_messageTitleTF.text body:_messageContentTV.text isDraft:[NSNumber numberWithInt:1] notificationTarget:json];
                }
            }
        }
    }
}

#pragma mark - ServiceHandlerDelegate

-(void)notificationSentSuccessfully  {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)requestFailWithError:(NSString *)err {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [self showAlert:err];
}


@end
