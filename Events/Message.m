//
//  Message.m
//  Events
//
//  Created by M.I.Kamashany on 2/19/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import "Message.h"

@implementation Message

- (void)encodeWithCoder:(NSCoder *)encoder {
    //Encode properties, other class variables, etc
    [encoder encodeObject:self.id forKey:@"id"];
    [encoder encodeObject:self.name forKey:@"name"];
    [encoder encodeObject:self.image forKey:@"image"];
    [encoder encodeObject:self.message forKey:@"message"];
    [encoder encodeObject:self.message_time forKey:@"message_time"];
    [encoder encodeObject:self.email forKey:@"email"];
    [encoder encodeObject:self.read forKey:@"read"];
    [encoder encodeObject:self.unread_meassage forKey:@"unread_meassage"];
    [encoder encodeObject:self.message_details forKey:@"message_details"];
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        //decode properties, other class vars
        self.id= [decoder decodeObjectForKey:@"id"];
        self.name =[decoder decodeObjectForKey:@"name"];
        self.image = [decoder decodeObjectForKey:@"image"];
        self.message =[decoder decodeObjectForKey:@"message"];
        self.message_time = [decoder decodeObjectForKey:@"message_time"];
        self.email =[decoder decodeObjectForKey:@"email"];
        self.read = [decoder decodeObjectForKey:@"read"];
        self.unread_meassage = [decoder decodeObjectForKey:@"unread_meassage"];
        self.message_details = [decoder decodeObjectForKey:@"message_details"];
    }
    return self;
}

@end
