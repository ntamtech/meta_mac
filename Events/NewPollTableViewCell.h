//
//  NewPollTableViewCell.h
//  Events
//
//  Created by M.I.Kamashany on 3/9/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol  NewPollTableViewCellDelegate <NSObject>

@optional
-(void)deletePollByIndexPath:(NSIndexPath *)index ;

@end

@interface NewPollTableViewCell : UITableViewCell


@property(nonatomic,weak) id<NewPollTableViewCellDelegate> delegate ;

@property (nonatomic,weak) IBOutlet UILabel *questionLabel ;
@property (nonatomic,strong) NSIndexPath *cellIndexPath ;
- (IBAction)deletePoll:(id)sender;

@end
