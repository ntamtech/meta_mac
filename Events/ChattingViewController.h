//
//  ChattingViewController.h
//  Events
//
//  Created by M.I.Kamashany on 3/27/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChattingViewController : UIViewController

@property (nonatomic,strong) NSString *anotherUserID ;
@property (nonatomic,strong) NSString *anotherUserImageURL ;
@property (nonatomic,strong) NSMutableArray *messagesArr ;
@property (nonatomic,assign) BOOL *isThereNewMessages ;
@property (nonatomic,assign) int selectedMsgNum ;

@end
