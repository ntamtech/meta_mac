//
//  AnnouncementListViewController.m
//  Events
//
//  Created by M.I.Kamashany on 3/11/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import "AnnouncementListViewController.h"
#import "MBProgressHUD.h"
#import "ServiceHandler.h"
#import "Helper.h"
#import "SharedData.h"
#import "AnnouncementTableViewCell.h"
#import "AnnouncementType.h"
#import "UIViewController+Alert.h"

@interface AnnouncementListViewController ()<ServiceHandlerDelegate>
{
    ServiceHandler *service ;
    SharedData *shared ;
    UIButton *refreshBtn ;
    NSMutableArray *announcementsListArr ;
    NSString *dateAndTime ;
}

@property (nonatomic,weak) IBOutlet UITableView *announcementListTableView ;

@end

@implementation AnnouncementListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    announcementsListArr = [[NSMutableArray alloc]init];
    service = [[ServiceHandler alloc]init];
    service.delegate = self ;
    shared = [SharedData getSharedObject];
    self.announcementListTableView.hidden = YES ;
    
    self.announcementListTableView.estimatedRowHeight = 83 ;
    self.announcementListTableView.rowHeight = UITableViewAutomaticDimension ;
    self.announcementListTableView.separatorStyle = UITableViewCellSeparatorStyleNone ;
}

-(UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent ;
}

-(void)viewWillAppear:(BOOL)animated {
    if (![Helper checkInternet]) {
        [self createRefreshBtn:@"No Internet Connection!" enabled:YES];
    } else {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [service loadAnnouncementList:[NSNumber numberWithInt:1] userID:[NSNumber numberWithInt:[shared.user.id intValue]]];
    }
}


#pragma mark -ServiceHandlerDelegate 

-(void)getAllAnnouncementTypes:(NSMutableArray *)types {
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    if (types.count == 0) {
        [self createRefreshBtn:@"No Announcements" enabled:NO];
    } else {
        announcementsListArr = types ;
        [self.announcementListTableView reloadData];
        self.announcementListTableView.hidden = NO ;
    }
    
}

-(void)requestFailWithError:(NSString *)err {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [self showAlert:err];
}
#pragma mark -TableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
   return  1 ;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return announcementsListArr.count ;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellID = @"announcementCell";
    AnnouncementTableViewCell *cell = (AnnouncementTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell == nil) {
        cell =[[AnnouncementTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    AnnouncementType *announcement = [announcementsListArr objectAtIndex:indexPath.row];
    cell.bodyLabel.text = announcement.body ;
    cell.titleLabel.text = announcement.title ;
    
    NSString *date = [[announcement.notification_time componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] objectAtIndex:0];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    
    [dateFormat setDateStyle:NSDateFormatterMediumStyle];
    [dateFormat setDateFormat:@"HH:mm:ss"];
    NSDate *date1 = [dateFormat dateFromString:[[announcement.notification_time componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] objectAtIndex:1]];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"hh:mm a"];
    NSString *time = [formatter stringFromDate:date1];
    
    NSString *monthNumberStr = [[date componentsSeparatedByString: @"-"]objectAtIndex:1];
    int monthNumber = [monthNumberStr intValue];
    NSString *day = [[date componentsSeparatedByString: @"-"]objectAtIndex:2];
    NSString *year = [[date componentsSeparatedByString: @"-"]objectAtIndex:0];
    
    
    NSString *monthName = [[dateFormat monthSymbols] objectAtIndex:(monthNumber-1)];
    cell.dateTimeLabel.text = [NSString stringWithFormat:@"%@ %@ %@, %@",[monthName substringToIndex:3],day,year,time];

    return cell ;
}


-(IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)createRefreshBtn:(NSString *)title enabled:(BOOL)enabled{
    refreshBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [refreshBtn setTitle:title forState:UIControlStateNormal];
    refreshBtn.enabled = enabled ;
    refreshBtn.titleLabel.textColor = [UIColor lightGrayColor];
    refreshBtn.frame = CGRectMake(0, self.view.frame.size.height/2, self.view.frame.size.width, 25);
    [refreshBtn addTarget:self
                   action:@selector(refresh:)
         forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:refreshBtn];
    [self.view bringSubviewToFront:refreshBtn];
}

-(IBAction)refresh:(id)sender {
    if ([Helper checkInternet]) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [refreshBtn removeFromSuperview];
        [service loadAnnouncementList:[NSNumber numberWithInt:1] userID:[NSNumber numberWithInt:[shared.user.id intValue]]];
    }
}

@end
