//
//  ChattingViewController.m
//  Events
//
//  Created by M.I.Kamashany on 3/27/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import "ChattingViewController.h"
#import "DAKeyboardControl.h"
#import "CommentTableViewCell.h"
#import "SharedData.h"
#import "ServiceHandler.h"
#import "Helper.h"
#import "MBProgressHUD.h"
#import "UIImageView+WebCache.h"
#import "UIView+Toast.h"
#import "SideBarViewController.h"
#import "IQKeyboardManager.h"
#import "IQKeyboardManager.h"
#import "UIViewController+Alert.h"

@interface ChattingViewController ()<UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource,ServiceHandlerDelegate>
{
    UITextField *textField ;
    NSArray *messages ;
    NSMutableArray *allMessagesArr ;
    UIButton *refreshBtn ;
    NSTimer *refreshMessagesTimer ;
    ServiceHandler *service ;
    SharedData *shared ;
    BOOL isLoadingFirstTime ;
}

@property (nonatomic,weak) IBOutlet UITableView *messagesTableView ;

@end

@implementation ChattingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    allMessagesArr = [[NSMutableArray alloc]init];
    _messagesTableView.estimatedRowHeight = 79 ;
    _messagesTableView.rowHeight = UITableViewAutomaticDimension ;
    
    service = [[ServiceHandler alloc]init];
    service.delegate = self ;
    shared = [SharedData getSharedObject];
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:false];
    isLoadingFirstTime = NO;
}

-(UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent ;
}

-(void)viewWillAppear:(BOOL)animated{
    if (self.messagesArr != nil) {
        allMessagesArr = [NSMutableArray arrayWithArray:self.messagesArr];
        [self refreshMessages];
//        [_messagesTableView reloadData];
    }else{
        if ([Helper checkInternet]) {
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            _messagesTableView.hidden = YES ;
            [service getMessagesBtwMe:[NSNumber numberWithInt:[shared.user.id intValue]]
                           andAnother:[NSNumber numberWithInt:[self.anotherUserID intValue]]
                           andEventID:[NSNumber numberWithInt:1]];
        } else {
//            [self setupKeyboard];
            [self.view makeToast:@"No Internet Connection!"
                        duration:1.5
                        position:CSToastPositionBottom];
        }
    }
}

-(void)viewDidAppear:(BOOL)animated {
    if (self.messagesArr != nil) {
        if (_isThereNewMessages) {
            if ([Helper checkInternet]) {
                [service notifyMessageRead:[NSNumber numberWithInt:[shared.user.id intValue]] andAnotherUser:[NSNumber numberWithInt:[self.anotherUserID intValue]]];
            }
            Message *selectedMSG = [shared.allMessages objectAtIndex:_selectedMsgNum];
            selectedMSG.unread_meassage = @"0";
            [shared.allMessages removeObjectAtIndex:_selectedMsgNum];
            [shared.allMessages insertObject:selectedMSG atIndex:_selectedMsgNum];
            int totalUnReadMessages = shared.unreadMessagesCount.intValue - 1 ;
            shared.unreadMessagesCount = [NSNumber numberWithInt:totalUnReadMessages];
        }
    }
   [self setupKeyboard];
}

-(void)viewDidDisappear:(BOOL)animated {
    [refreshMessagesTimer invalidate];
}

#pragma mark -TableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1 ;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return allMessagesArr.count ;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellID = @"chatCell";
    CommentTableViewCell *cell = (CommentTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell == nil) {
        cell =[[CommentTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    
    cell.rightImageView.layer.cornerRadius = 14 ;
    cell.rightImageView.layer.masksToBounds = YES ;
    cell.leftImageView.layer.cornerRadius = 14 ;
    cell.leftImageView.layer.masksToBounds = YES ;
    
    Message *mess = [allMessagesArr objectAtIndex:indexPath.row];
    if ([mess.sender_id isEqualToString:shared.user.id]) {
        cell.messageLabel.isMe = true ;
        cell.leftImageView.hidden = YES ;
        cell.rightImageView.hidden = NO ;
        cell.messageLabel.textAlignment = NSTextAlignmentRight ;
        cell.dateLabel.textAlignment = NSTextAlignmentRight ;
        [cell.rightImageView setShowActivityIndicatorView:YES];
        [cell.rightImageView setIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [cell.rightImageView sd_setImageWithURL:[NSURL URLWithString:shared.user.image] placeholderImage:[UIImage imageNamed:@"profile-image"]];
        cell.bgView.backgroundColor = [UIColor lightGrayColor];
        cell.bgViewTrailingConstraint.constant = -36 ;
        cell.bgViewLeadingConstraint.constant = 0 ;
        [self.view layoutIfNeeded];
    } else {
        cell.messageLabel.isMe = false ;
        cell.rightImageView.hidden = YES ;
        cell.leftImageView.hidden = NO ;
        cell.messageLabel.textAlignment = NSTextAlignmentLeft ;
        cell.dateLabel.textAlignment = NSTextAlignmentLeft ;
        [cell.leftImageView setShowActivityIndicatorView:YES];
        [cell.leftImageView setIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [cell.leftImageView sd_setImageWithURL:[NSURL URLWithString:self.anotherUserImageURL] placeholderImage:[UIImage imageNamed:@"profile-image"]];
        cell.bgView.backgroundColor = [UIColor groupTableViewBackgroundColor];
        cell.bgViewLeadingConstraint.constant = 36 ;
        cell.bgViewTrailingConstraint.constant = 0 ;
        [self.view layoutIfNeeded];
    }
    cell.messageLabel.text = mess.message;
    cell.dateLabel.text = mess.message_time ;
    cell.selectionStyle = UITableViewCellSelectionStyleNone ;
    return cell ;
}

-(void)setupKeyboard {
    UIToolbar *toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0.0f,
                                                                     self.view.bounds.size.height - 40.0f,
                                                                     self.view.bounds.size.width,
                                                                     40.0f)];
    toolBar.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleWidth;
    [self.view addSubview:toolBar];
    
    //constraints of uitableview
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:toolBar attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:_messagesTableView attribute:NSLayoutAttributeBottom multiplier:1 constant:0]];
    [self.view layoutIfNeeded];
    
    
    textField = [[UITextField alloc] initWithFrame:CGRectMake(10.0f,
                                                              6.0f,
                                                              toolBar.bounds.size.width - 20.0f - 68.0f,
                                                              30.0f)];
    textField.borderStyle = UITextBorderStyleRoundedRect;
    textField.returnKeyType = UIReturnKeyDone ;
    textField.delegate = self ;
    textField.placeholder = @"Write your message";
    textField.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    [toolBar addSubview:textField];
    
    UIButton *sendButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    sendButton.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
    [sendButton setTitle:@"Send" forState:UIControlStateNormal];
    [sendButton addTarget:self action:@selector(addMessage:) forControlEvents:UIControlEventTouchDown];
    sendButton.frame = CGRectMake(toolBar.bounds.size.width - 68.0f,
                                  6.0f,
                                  58.0f,
                                  29.0f);
    [toolBar addSubview:sendButton];
    
    
    self.view.keyboardTriggerOffset = toolBar.bounds.size.height;
//    __block ChattingViewController *blockSelf = self;
    if (allMessagesArr.count > 1) {
        [self showAnswersFromBottom];
    }
    [self.view addKeyboardPanningWithFrameBasedActionHandler:^(CGRect keyboardFrameInView, BOOL opening, BOOL closing) {
        /*
         Try not to call "self" inside this block (retain cycle).
         But if you do, make sure to remove DAKeyboardControl
         when you are done with the view controller by calling:
         [self.view removeKeyboardControl];
         */
        
        CGRect toolBarFrame = toolBar.frame;
        toolBarFrame.origin.y = keyboardFrameInView.origin.y - toolBarFrame.size.height;
        toolBar.frame = toolBarFrame;
    } constraintBasedActionHandler:nil];
}

-(IBAction)addMessage:(id)sender {
    if (![textField.text isEqualToString:@""]) {
        
        if (![Helper checkInternet]) {
            [self.view makeToast:@"No Internet Connection!"
                        duration:1
                        position:CSToastPositionCenter];
        } else {
            _messagesTableView.hidden = NO ;
            Message *newMessage = [[Message alloc]init];
            newMessage.message = textField.text ;
            newMessage.sender_id = shared.user.id ;
            newMessage.image = shared.user.image ;
            NSDate *date = [NSDate date];
            NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
            [formatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
            newMessage.message_time = [formatter stringFromDate:date];
            [allMessagesArr addObject:newMessage];
            
            //update in shared data
            Message *chatMessages = [shared.allMessages objectAtIndex:self.selectedMsgNum];
            chatMessages.message = textField.text ;
            chatMessages.message_time = newMessage.message_time ;
            [chatMessages.message_details removeAllObjects];
            [chatMessages.message_details addObjectsFromArray:allMessagesArr] ;
            [shared.allMessages removeObjectAtIndex:self.selectedMsgNum];
            [shared.allMessages insertObject:chatMessages atIndex:self.selectedMsgNum];
            
            [_messagesTableView reloadData];
            NSIndexPath* ip = [NSIndexPath indexPathForRow:allMessagesArr.count-1 inSection:0];
            [_messagesTableView scrollToRowAtIndexPath:ip atScrollPosition:UITableViewScrollPositionTop animated:NO];
            
            [service sendChatMessage:[NSNumber numberWithInt:1] andSenderID:[NSNumber numberWithInt:[shared.user.id intValue]] andReceiverID:[NSNumber numberWithInt:[self.anotherUserID intValue]] andMessage:textField.text andImage:@""];
            textField.text = @"" ;
        }
        
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    if(textField.returnKeyType == UIReturnKeyDone) {
        [textField resignFirstResponder];
    }
    return YES;
}

-(void)showAnswersFromBottom {
    NSIndexPath* ip = [NSIndexPath indexPathForRow:allMessagesArr.count-1 inSection:0];
    [_messagesTableView scrollToRowAtIndexPath:ip atScrollPosition:UITableViewScrollPositionTop animated:YES];
}


-(void)createRefreshBtn:(NSString *)title enabled:(BOOL)enabled{
    refreshBtn = [[UIButton alloc]init];
    [refreshBtn setTitle:title forState:UIControlStateNormal];
    refreshBtn.enabled = enabled ;
    refreshBtn.titleLabel.numberOfLines = 0 ;
    refreshBtn.center = self.view.center ;
    [refreshBtn addTarget:self
                   action:@selector(refresh:)
         forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:refreshBtn];
}

-(IBAction)refresh:(id)sender {
    if ([Helper checkInternet]) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [refreshBtn removeFromSuperview];
        [service loadAllMessages:[NSNumber numberWithInt:[shared.user.id intValue]] eventID:[NSNumber numberWithInt:1]];
    }
}


-(IBAction)showMenu:(id)sender {
    SideBarViewController *destination = (SideBarViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"SideBarViewController"];
    [self.navigationController pushViewController:destination animated:YES];
}

-(IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - ServiceHandlerDelegate

-(void)showChatMessages:(NSMutableArray *)chatMessages {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [_messagesTableView setHidden:NO];
    if (chatMessages.count != 0) {
        [allMessagesArr removeAllObjects];
        [allMessagesArr addObjectsFromArray:chatMessages];
        allMessagesArr = [NSMutableArray arrayWithArray:[[allMessagesArr reverseObjectEnumerator] allObjects]];
        [_messagesTableView reloadData];
        if (!isLoadingFirstTime) {
            isLoadingFirstTime = YES ;
            [self showAnswersFromBottom];
        }
        
    }
    [self setupKeyboard];
    [self refreshMessages];
}
// timer to refresh chatting after 3 seconds
-(void)refreshMessages {
//    if (refreshMessagesTimer == nil) {
//        refreshMessagesTimer = [NSTimer scheduledTimerWithTimeInterval:3.0f target:self selector:@selector(timerFired:) userInfo:nil repeats:YES];
//    }
}

-(void)msgSendSuccessfully {
    
}

-(void)requestFailWithError:(NSString *)err {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [self showAlert:err];
}


- (void)timerFired:(NSTimer*)theTimer {
    if ([Helper checkInternet]) {
        [service getMessagesBtwMe:[NSNumber numberWithInt:[shared.user.id intValue]]
                       andAnother:[NSNumber numberWithInt:[self.anotherUserID intValue]]
                       andEventID:[NSNumber numberWithInt:1]];
    }
}

@end
