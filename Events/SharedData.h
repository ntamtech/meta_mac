//
//  SharedData.h
//  iJobs
//
//  Created by M.I.Kamashany on 12/11/16.
//  Copyright © 2016 NTAMNTAMTech. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"
#import "About.h"
#import "Question.h"
#import "WebData.h"

@interface SharedData : NSObject

//cyprus
@property (nonatomic, strong) Question *activatedQuestion ;

//techne Summit
@property (nonatomic, strong) NSMutableArray *venuesArr ;
@property (nonatomic, strong) NSMutableArray *tracksArr ;


@property (nonatomic, strong) User *user ;
@property (nonatomic, strong) About *about ;
@property (nonatomic, strong) WebData *webData ;
@property (nonatomic, strong) NSMutableArray *sliderArr ;

@property (nonatomic, strong) NSMutableArray *pollsArr ;

@property (nonatomic,strong) NSString *tkn ;
@property (nonatomic,strong) NSString *notificationsCount ;

@property (nonatomic,strong) NSMutableArray *allSpeakersArr ;
//used when select session from speakersViewController
@property (nonatomic,assign) NSIndexPath *selectedSpeaker ;
@property (nonatomic,assign) NSIndexPath *selectedSession ;

//used when select session from agenda
@property (nonatomic,assign) NSIndexPath *selectedSessionIndexPath ;

@property (nonatomic,strong) NSMutableArray *allAgenda ;
@property (nonatomic,strong) NSMutableArray *myAgenda ;

@property (nonatomic,strong) NSMutableArray *postsArr ;

@property (nonatomic,strong) NSMutableArray *photosArr ;

@property (nonatomic,strong) NSMutableDictionary *allAttendees ;

@property (nonatomic,strong) NSMutableArray *allMessages ;
@property (nonatomic,strong) NSNumber *unreadMessagesCount ;


+(id)getSharedObject ;
+(void)deleteUser ;
+(void)deleteAllCachedData ;

@end
