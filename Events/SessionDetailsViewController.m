//
//  SessionDetailsViewController.m
//  Events
//
//  Created by M.I.Kamashany on 2/27/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import "SessionDetailsViewController.h"
#import "SideBarViewController.h"
#import "CoordinateManager.h"
#import "SessionDetailsHeaderView.h"
#import "SpeakerTableViewCell.h"
#import "UIImageView+WebCache.h"
#import "WritePostViewController.h"
#import <STPopup/STPopup.h>
#import "Helper.h"
#import "UIView+Toast.h"
#import "ServiceHandler.h"
#import "MBProgressHUD.h"
#import "SharedData.h"
#import "Helper.h"
#import "AgendaModel.h"

@interface SessionDetailsViewController ()<UITableViewDelegate,UITableViewDataSource,WritePostViewControllerDelegate,ServiceHandlerDelegate,SessionDetailsHeaderViewDelegate,EDStarRatingProtocol>
{
    ServiceHandler *service ;
    SharedData *shared ;
}

@property CoordinateManager *coordinateManager;
@property (strong, nonatomic) UITableView *speakersTableView;
@property (strong, nonatomic) SessionDetailsHeaderView *sessionDetailsView;

@end

@implementation SessionDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    service = [[ServiceHandler alloc]init];
    service.delegate = self ;
    shared = [SharedData getSharedObject];
}

-(void)viewWillAppear:(BOOL)animated {
    
    self.speakersTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, self.view.frame.size.width, self.view.frame.size.height-64) style:UITableViewStylePlain];
    self.speakersTableView.dataSource = self;
    self.speakersTableView.delegate = self;
    self.speakersTableView.backgroundColor = [UIColor colorWithRed:255.0/255 green:255.0/255 blue:255.0/255 alpha:1];
    
    self.sessionDetailsView = [[SessionDetailsHeaderView alloc]init];
    if (self.selectedSession.desc == nil || [self.selectedSession.desc isEqualToString:@""]) {
        self.sessionDetailsView.sessionDetailsTV.text = @"No Description" ;
    } else {
        self.sessionDetailsView.sessionDetailsTV.text = self.selectedSession.desc ;
    }
    CGRect descriptionFrame = [self changeTextViewFrame:self.sessionDetailsView.sessionDetailsTV];
    self.sessionDetailsView.frame = CGRectMake(0, 0, self.view.frame.size.width, 350 + descriptionFrame.size.height );
    self.sessionDetailsView.selectedSession = self.selectedSession ;
    self.sessionDetailsView.delegate = self ;
//    if ([self.selectedSession.has_rate isEqualToString:@"0"]) {
//        self.sessionDetailsView.sessionRating.editable=NO;
//    }
    
    if ([self.selectedSession.add_to_agenda isEqualToString:@"1"]) {
        [self.sessionDetailsView.addToAgendaBtn setTitle:@"Added to agenda" forState:UIControlStateNormal];
        self.sessionDetailsView.addToAgendaBtn.enabled = NO ;
        self.sessionDetailsView.addToAgendaImageView.image = [UIImage imageNamed:@"addedToAgenda.png"];
    }
    self.coordinateManager = [[CoordinateManager alloc]initManager:self scroll:self.speakersTableView header:self.sessionDetailsView];
    self.speakersTableView.backgroundColor = [UIColor colorWithRed:255.0/255 green:255.0/255 blue:255.0/255 alpha:1];
    // set views
    [_coordinateManager setContainer:self.speakersTableView views:nil];
    [self.view addSubview:self.speakersTableView];
    
    self.speakersTableView.allowsSelection = NO ;

    if (self.selectedSession) {
        self.sessionDetailsView.sessionNameLabel.text = self.selectedSession.session_name ;
        NSAttributedString *text =
        [[NSAttributedString alloc]initWithString:self.selectedSession.session_tags
                                       attributes:@{NSBackgroundColorAttributeName : [UIColor colorWithRed:32.0/255 green:98.0/255 blue:176.0/255 alpha:1]}];
        self.sessionDetailsView.sessionTagLabel.attributedText = text ;
        self.sessionDetailsView.calenderLabel.text = self.selectedSession.session_date ;
        NSString *startTime = [Helper detectPMAM:self.selectedSession.start_time];
        NSString *endTime = [Helper detectPMAM:self.selectedSession.end_time];
        self.sessionDetailsView.timeLabel.text = [NSString stringWithFormat:@"%@ - %@",startTime,endTime];
        //
        if (self.selectedSession.physical_address == nil || [self.selectedSession.physical_address isEqualToString:@""]) {
            self.sessionDetailsView.locationLabel.text = @"No Location" ;
        } else {
            self.sessionDetailsView.locationLabel.text = self.selectedSession.physical_address ;
        }
        
        
    }
}


#pragma mark -TableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.selectedSession.list_of_speaker.count ;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellID = @"speakerCell";
    SpeakerTableViewCell *cell = (SpeakerTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell == nil) {
        cell =[[SpeakerTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    
    Speaker *selectedSpeaker = [self.selectedSession.list_of_speaker objectAtIndex:indexPath.row];
    [cell.speakerIcon setShowActivityIndicatorView:YES];
    [cell.speakerIcon setIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [cell.speakerIcon sd_setImageWithURL:[NSURL URLWithString:selectedSpeaker.image] placeholderImage:nil];
    cell.speakerNameLabel.text = selectedSpeaker.name ;
    cell.speakerNameLabel.textColor = [UIColor blackColor];
    cell.speakerJobDesc.text = selectedSpeaker.position ;
    cell.speakerJobDesc.textColor = [UIColor blackColor];
    tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    return cell ;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 101 ;
}

-(IBAction)showMenu:(id)sender {
    SideBarViewController *destination = (SideBarViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"SideBarViewController"];
    [self.navigationController pushViewController:destination animated:YES];
}

-(IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


//- (void)changeTextViewFrame:(UITextView *)textView {
//    
//    CGFloat fixedWidth = textView.frame.size.width;
//    CGSize newSize = [textView sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
//    CGRect newFrame = textView.frame;
//    newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), newSize.height);
//    textView.frame = newFrame;
//}

-(IBAction)writePost:(id)sender {
    WritePostViewController *destination = [self.storyboard instantiateViewControllerWithIdentifier:@"WritePostViewController"];
    destination.delegate = self ;
    destination.contentSizeInPopup = CGSizeMake(300, 300);
    STPopupController *popupController = [[STPopupController alloc] initWithRootViewController:destination];
    popupController.navigationBarHidden = YES ;
    [popupController presentInViewController:self];
}

-(void)dismissWithPostDetails:(NSString *)details {
    if (![Helper checkInternet]) {
        [self.view makeToast:@"No Internet Connection!"
                    duration:1
                    position:CSToastPositionBottom];
    } else {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [service sendPostWithContent:details eventID:[NSNumber numberWithInt:1] userID:[NSNumber numberWithInt:[shared.user.id intValue]] isCheckIn:NO];
    }
}

#pragma mark - ServiceHandlerDelegate

-(void)postSuccess:(PostModel *)post  {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [self.view makeToast:@"Posted Successfully"
                duration:1
                position:CSToastPositionBottom];
}

-(void)requestFailWithError:(NSString *)err {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [self.view makeToast:err
                duration:2
                position:CSToastPositionBottom];
}

#pragma mark - SessionDetailsHeaderViewDelegate

-(void)addSessionToAgendaWithSessionID:(NSNumber *)sessionID {
    if (![Helper checkInternet]) {
        [self.view makeToast:@"No Internet Connection!"
                    duration:1
                    position:CSToastPositionBottom];
    } else {
        if (_isComingFromSpeakers) {
            _selectedSession.add_to_agenda = @"1";
            Speaker *speakerSessions = [shared.allSpeakersArr objectAtIndex:self.selectedSpeakerIndex];
            [speakerSessions.sessions removeObjectAtIndex:self.selectedSessionIndexPath.row];
            [speakerSessions.sessions insertObject:_selectedSession atIndex:self.selectedSessionIndexPath.row];
            
            [shared.allSpeakersArr removeObjectAtIndex:self.selectedSpeakerIndex];
            [shared.allSpeakersArr insertObject:speakerSessions atIndex:self.selectedSpeakerIndex];
        } else {
            if ([_isFromFullAgenda isEqualToString:@"0"]) {
                _selectedSession.add_to_agenda = @"1";
                AgendaModel *selectedAgenda = [shared.allAgenda objectAtIndex:_selectedAgendaIndex];
                [selectedAgenda.sessions removeObjectAtIndex:_selectedSessionIndex];
                [selectedAgenda.sessions insertObject:_selectedSession atIndex:_selectedSessionIndex];
                
                [shared.allAgenda removeObjectAtIndex:_selectedAgendaIndex];
                [shared.allAgenda insertObject:selectedAgenda atIndex:_selectedAgendaIndex];
            } else {
                
            }
        }
        
        [service addSessionToAgenda:[NSNumber numberWithInt:1] sessionID:sessionID userID:[NSNumber numberWithInt:[shared.user.id intValue]]];
    }
}


-(void)sessionAddedSuccessfully {
    [self.view makeToast:@"Session added Successfully!"
                duration:1
                position:CSToastPositionBottom];
    self.sessionDetailsView.addToAgendaBtn.enabled = NO ;
    [self.sessionDetailsView.addToAgendaBtn setTitle:@"Added to agenda" forState:UIControlStateNormal];
    self.sessionDetailsView.selectedSession.add_to_agenda = @"1";
    self.sessionDetailsView.addToAgendaImageView.image = [UIImage imageNamed:@"addedToAgenda.png"];
}




#pragma mark - EDStarRatingProtocol

-(void)starsSelectionChanged:(EDStarRating*)control rating:(float)rating {
    if (![Helper checkInternet]) {
        [self.view makeToast:@"No Internet Connection!"
                    duration:1
                    position:CSToastPositionBottom];
    } else {
        if (_isComingFromSpeakers) {
            _selectedSession.session_rate_of_user = [NSString stringWithFormat:@"%f",rating];
            Speaker *speakerSessions = [shared.allSpeakersArr objectAtIndex:self.selectedSpeakerIndex];
            [speakerSessions.sessions removeObjectAtIndex:self.selectedSessionIndexPath.row];
            [speakerSessions.sessions insertObject:_selectedSession atIndex:self.selectedSessionIndexPath.row];
            
            [shared.allSpeakersArr removeObjectAtIndex:self.selectedSpeakerIndex];
            [shared.allSpeakersArr insertObject:speakerSessions atIndex:self.selectedSpeakerIndex];
        } else {
            if ([_isFromFullAgenda isEqualToString:@"0"]) {
                _selectedSession.session_rate_of_user = [NSString stringWithFormat:@"%f",rating];
                AgendaModel *selectedAgenda = [shared.allAgenda objectAtIndex:_selectedAgendaIndex];
                [selectedAgenda.sessions removeObjectAtIndex:_selectedSessionIndex];
                [selectedAgenda.sessions insertObject:_selectedSession atIndex:_selectedSessionIndex];
                
                [shared.allAgenda removeObjectAtIndex:_selectedAgendaIndex];
                [shared.allAgenda insertObject:selectedAgenda atIndex:_selectedAgendaIndex];
            } else {
                int count = 0 ;
                BOOL isFound = NO ;
                for (AgendaModel *agenda in shared.allAgenda) {
                    if ([agenda.day_number isEqualToString:_agendaDay_number]){
                        for (int i = 0 ; i < agenda.sessions.count ; i++) {
                            Session *sess = [agenda.sessions objectAtIndex:i];
                            if ([sess.id isEqualToString:_selectedSession.id]) {
                                sess.session_rate_of_user = [NSString stringWithFormat:@"%f",rating];
                                
                                [agenda.sessions removeObjectAtIndex:i];
                                [agenda.sessions insertObject:sess atIndex:i];
                                
                                [shared.allAgenda removeObjectAtIndex:count];
                                [shared.allAgenda insertObject:agenda atIndex:count];
                                
                                // filter again
                                NSMutableArray *myAgendaArr = [[NSMutableArray alloc]init];
                                for (int i = 0; i <shared.allAgenda.count; i++) {
                                    AgendaModel *agenda = [shared.allAgenda objectAtIndex:i];
                                    for (Session *selectedSession in agenda.sessions) {
                                        if ([selectedSession.add_to_agenda isEqualToString:@"1"]) {
                                            [myAgendaArr addObject:agenda];
                                            break ;
                                        }
                                    }
                                }
                                shared.myAgenda = myAgendaArr ;
                                isFound = YES ;
                                break;
                            }
                        }
                    }
                    if (isFound) {
                        break ;
                    }
                    count ++ ;
                }
            }
        }
        [service addSessionRate:[NSNumber numberWithFloat:rating] sessionID:[NSNumber numberWithInt:[self.selectedSession.id intValue]] userID:[NSNumber numberWithInt:[shared.user.id intValue]]];
    }
}

-(void)sessionRatingSuccessfully:(NSNumber *)rate {
    [self.view makeToast:@"Session rate added Successfully!"
                duration:1
                position:CSToastPositionBottom];
}

- (CGRect )changeTextViewFrame:(UILabel *)label
{
    CGFloat fixedWidth = label.frame.size.width;
    CGSize newSize = [label sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
    CGRect newFrame = label.frame;
    newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), newSize.height);
    return newFrame ;
}
@end
