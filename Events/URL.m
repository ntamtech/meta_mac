//
//  URL.m
//  iJobs
//
//  Created by M.I.Kamashany on 12/11/16.
//  Copyright © 2016 NTAMNTAMTech. All rights reserved.
//

#import "URL.h"

@implementation URL

+(NSString *)getAboutAppURL {
    return [self getURLByKeyword:@"about"];
}


+(NSString *)getLoginURL {
    return [self getURLByKeyword:@"login"];
}

+(NSString *)getSignupURL{
    return [self getURLByKeyword:@"signup"];
}

+(NSString *)getAllMessagesURL {
    return [self getURLByKeyword:@"allMessages"];
}

+(NSString *)getAllSpeakersURL {
    return [self getURLByKeyword:@"allSpeakers"];
}

+(NSString *)getEventSliderURL {
    return [self getURLByKeyword:@"eventSlider"];
}

+(NSString *)getAllPostsURL {
    return [self getURLByKeyword:@"allPosts"];
}

+(NSString *)getAttendeesURL  {
    return [self getURLByKeyword:@"allAttendees"];
}

+(NSString *)getAaddPostURL  {
    return [self getURLByKeyword:@"addPost"];
}


+(NSString *)getAddSessionToAgenda {
    return [self getURLByKeyword:@"addSessionToAgenda"];
}

+(NSString *)getAddSessionRateURL  {
    return [self getURLByKeyword:@"addSessionRate"];
}

+(NSString *)getPollsURL {
    return [self getURLByKeyword:@"getPolls"];
}

+(NSString *)getDeletePollURL {
    return [self getURLByKeyword:@"deletePoll"];
}

+(NSString *)getFullAgendaURL {
    return [self getURLByKeyword:@"fullAgenda"];
}

+(NSString *)getMyAgendaURL {
    return [self getURLByKeyword:@"myAgenda"];
}

+(NSString *)getAnnouncementTypesURL {
    return [self getURLByKeyword:@"announcementTypes"];
}

+(NSString *)getAddPollURL {
    return [self getURLByKeyword:@"addPoll"];
}

+(NSString *)getSendNotificationURL {
    return [self getURLByKeyword:@"sendNotification"];
}

+(NSString *)getAnnouncementListURL {
    return [self getURLByKeyword:@"announcementList"];
}

+(NSString *)getEditBioURL {
    return [self getURLByKeyword:@"editBio"];
}

+(NSString *)getAnswerPollURL {
    return [self getURLByKeyword:@"answerPoll"];
}

+(NSString *)getEditProfileURL {
    return [self getURLByKeyword:@"editProfile"];
}

+(NSString *)getEditAccountImageURL {
    return [self getURLByKeyword:@"editAccountImage"];
}

+(NSString *)getEditSocialLinksURL {
    return [self getURLByKeyword:@"editSocailLinks"];
}

+(NSString *)getLikePostURL {
    return [self getURLByKeyword:@"likePost"];    
}

+(NSString *)getLikeSessionURL {
    return [self getURLByKeyword:@"likeSession"];
}

+(NSString *)getMessagesBtwMeAndAnotherURL {
    return [self getURLByKeyword:@"messagesBtwMeAndAnother"];
}

+(NSString *)getMessageCountURL {
    return [self getURLByKeyword:@"messagesCount"];
}

+(NSString *)getReadMessageURL {
    return [self getURLByKeyword:@"readMessage"];
}

+(NSString *)getLikePhotoURL {
    return [self getURLByKeyword:@"likePhoto"];
}

+(NSString *)getAllPhotosURL {
    return [self getURLByKeyword:@"allPhotos"];
}


+(NSString *)getDeletePhotoURL {
    return [self getURLByKeyword:@"deletePhoto"];
}

+(NSString *)getSendChatMessageURL {
    return [self getURLByKeyword:@"sendChatMessage"];    
}

+(NSString *)getAddPhotoURL {
    return [self getURLByKeyword:@"addPhoto"];    
}

+(NSString *)getAddPhotoCommentURL {
    return [self getURLByKeyword:@"addPhotoComment"];
}

+(NSString *)getAddPostCommentURL  {
    return [self getURLByKeyword:@"addPostComment"];
}

+(NSString *)getAddSessionCommentURL {
    return [self getURLByKeyword:@"addSessionComment"];
}

+(NSString *)getLogoutURL {
    return [self getURLByKeyword:@"logout"];
}

+(NSString *)getAllTracksAndVenuesURL {
    return [self getURLByKeyword:@"allTracksAndVenues"];
}

+(NSString *)searchInAgendaURL {
    return [self getURLByKeyword:@"searchInAgenda"];
}

+(NSString *)getURLByKeyword:(NSString *)keyword {
    NSString *UrlsFilePath = [[NSBundle mainBundle] pathForResource:@"URLS" ofType:@"plist"];
    NSDictionary *dic = [NSDictionary dictionaryWithContentsOfFile:UrlsFilePath];
    NSString *url = [dic valueForKey:keyword];
    return url ;
}


@end
