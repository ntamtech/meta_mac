//
//  About.h
//  Events
//
//  Created by M.I.Kamashany on 2/18/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JsonModel.h"

@interface About : JSONModel

@property (nonatomic) NSString<Optional> *event_id ;
@property (nonatomic,strong)    NSString<Optional> *name ;
@property (nonatomic,strong)    NSString<Optional> *content ;
@property (nonatomic,strong)    NSString<Optional> *image ;
@property (nonatomic,strong)    NSString<Optional> *logo ;
@property (nonatomic,strong)    NSString<Optional> *header ;
@property (nonatomic,strong)    NSString<Optional> *event_tag ;
@property (nonatomic,strong)    NSString<Optional> *event_location ;
@property (nonatomic,strong)    NSString<Optional> *pdf_file ;

@end
