//
//  AppDelegate.m
//  Events
//
//  Created by M.I.Kamashany on 2/9/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import "AppDelegate.h"
#import "User.h"
#import "SharedData.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <linkedin-sdk/LISDK.h>
#import <Fabric/Fabric.h>
#import <TwitterKit/TwitterKit.h>

#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
@import UserNotifications;
#endif

@import Firebase;
@import FirebaseInstanceID;
@import FirebaseMessaging;
@import GoogleMaps;

// Implement UNUserNotificationCenterDelegate to receive display notification via APNS for devices
// running iOS 10 and above. Implement FIRMessagingDelegate to receive data message via FCM for
// devices running iOS 10 and above.
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
@interface AppDelegate () <UNUserNotificationCenterDelegate, FIRMessagingDelegate>
@end
#endif

// Copied from Apple's header in case it is missing in some cases (e.g. pre-Xcode 8 builds).
#ifndef NSFoundationVersionNumber_iOS_9_x_Max
#define NSFoundationVersionNumber_iOS_9_x_Max 1299
#endif


@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // [START configure_firebase]
    [FIRApp configure];
    //    [GMSPlacesClient provideAPIKey:@"AIzaSyDmeOXcnKRVPQ8eyDHUcLLQ0d1YVie7I5A"];
    // Register for remote notifications
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_7_1) {
        // iOS 7.1 or earlier. Disable the deprecation warnings.
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
        UIRemoteNotificationType allNotificationTypes =
        (UIRemoteNotificationTypeSound |
         UIRemoteNotificationTypeAlert |
         UIRemoteNotificationTypeBadge);
        [application registerForRemoteNotificationTypes:allNotificationTypes];
#pragma clang diagnostic pop
    } else {
        // iOS 8 or later
        // [START register_for_notifications]
        if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_9_x_Max) {
            UIUserNotificationType allNotificationTypes =
            (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
            UIUserNotificationSettings *settings =
            [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
            [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
        } else {
            // iOS 10 or later
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
            UNAuthorizationOptions authOptions =
            UNAuthorizationOptionAlert
            | UNAuthorizationOptionSound
            | UNAuthorizationOptionBadge;
            [[UNUserNotificationCenter currentNotificationCenter]
             requestAuthorizationWithOptions:authOptions
             completionHandler:^(BOOL granted, NSError * _Nullable error) {
             }
             ];
            
            // For iOS 10 display notification (sent via APNS)
            [[UNUserNotificationCenter currentNotificationCenter] setDelegate:self];
            // For iOS 10 data message (sent via FCM)
            [[FIRMessaging messaging] setRemoteMessageDelegate:self];
#endif
        }
        
        [[UIApplication sharedApplication] registerForRemoteNotifications];
        // [END register_for_notifications]
    }
    
    // [END configure_firebase]
    // Add observer for InstanceID token refresh callback.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tokenRefreshNotification:)
                                                 name:kFIRInstanceIDTokenRefreshNotification object:nil];
//    NSDictionary *userInfo = [launchOptions valueForKey:@"UIApplicationLaunchOptionsRemoteNotificationKey"];
//    NSDictionary *apsInfo = [userInfo objectForKey:@"aps"];
//    
//    if(apsInfo) {
//        [[NSUserDefaults standardUserDefaults] setObject:@"yes" forKey:@"notification"];
//        [[NSUserDefaults standardUserDefaults] synchronize];
//    }
    
    //facebook integration
//    [[FBSDKApplicationDelegate sharedInstance] application:application
//                             didFinishLaunchingWithOptions:launchOptions];
//    //twitter integration
//    [Fabric with:@[[Twitter class]]];
//    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
//    [GMSServices provideAPIKey:@"AIzaSyCFiGZdNFQKY9dJ5BhDeoZS4lFDx9FYGOc"];
    return YES;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    BOOL handled = NO;
    
//    if ([[[url absoluteString] substringToIndex:2] isEqualToString:@"fb"]) {
//        handled = [[FBSDKApplicationDelegate sharedInstance] application:application
//                                                                 openURL:url
//                                                       sourceApplication:sourceApplication
//                                                              annotation:annotation
//                   ];
//    } else if([[[url absoluteString] substringToIndex:2] isEqualToString:@"li"]){
//        handled =  [LISDKCallbackHandler application:application openURL:url sourceApplication:sourceApplication annotation:annotation];
//    }
//    }else{
//        handled = [[Twitter sharedInstance] application:application openURL:url options:options] ;
//        
//    }
    return handled;
}
    
    // linkedin check
//    if ([LISDKCallbackHandler shouldHandleUrl:url]) {
//        handled =  [LISDKCallbackHandler application:application openURL:url sourceApplication:sourceApplication annotation:annotation];
//    }
    // Add any custom logic here.
//    return handled;
//}


#pragma mark- notifications


// [START receive_message]
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    // If you are receiving a notification message while your app is in the background,
    // this callback will not be fired till the user taps on the notification launching the application.
    // TODO: Handle data of notification
    UIApplicationState state = [[UIApplication sharedApplication] applicationState];
    if (state == UIApplicationStateBackground || state == UIApplicationStateInactive)
    {
        //Do checking here.
    }else{
        
    }
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    // If you are receiving a notification message while your app is in the background,
    // this callback will not be fired till the user taps on the notification launching the application.
    // TODO: Handle data of notification
    // Print message ID.
//    NSString *notificationsMsg = userInfo[@"aps"][@"alert"] ;
//    NSMutableArray *newNotifications = [[NSMutableArray alloc]init];
//    NSMutableArray *notifications = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"notifications"]];
//    if (notifications != nil) {
//        [newNotifications addObject:notificationsMsg];
//        [newNotifications addObjectsFromArray:notifications];
//    } else {
//        [newNotifications addObject:notificationsMsg];
//    }
//    [[NSUserDefaults standardUserDefaults] setObject:newNotifications forKey:@"notifications"];
//    [[NSUserDefaults standardUserDefaults]synchronize];
//    SharedData *shared = [SharedData getSharedObject];
//    int count = [shared.notificationsCount intValue];
//    count += 1 ;
//    shared.notificationsCount = [NSString stringWithFormat:@"%i",count];
    
    //    UIViewController *root = self.window.rootViewController ;
    //    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    //    FavouriteJobsViewController *destination = [sb instantiateViewControllerWithIdentifier:@"FavouriteJobsViewController"];
    //    [root presentViewController:destination animated:YES completion:nil];
}
// [END receive_message]

// [START ios_10_message_handling]
// Receive displayed notifications for iOS 10 devices.
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
// when notification come in the forground mode
// Handle incoming notification messages while app is in the foreground.
- (void)userNotificationCenter:(UNUserNotificationCenter *)center
       willPresentNotification:(UNNotification *)notification
         withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler {
    // Print message ID.
//    NSDictionary *userInfo = notification.request.content.userInfo;
//    NSMutableArray *newNotifications = [[NSMutableArray alloc]init];
//    NSMutableArray *notifications = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"notifications"]];
//    NSString *notificationsMsg = userInfo[@"aps"][@"alert"] ;
//    if (notifications != nil) {
//        [newNotifications addObject:notificationsMsg];
//        [newNotifications addObjectsFromArray:notifications];
//    } else {
//        [newNotifications addObject:notificationsMsg];
//    }
//    [[NSUserDefaults standardUserDefaults] setObject:newNotifications forKey:@"notifications"];
//    [[NSUserDefaults standardUserDefaults]synchronize];
//    SharedData *shared = [SharedData getSharedObject];
//    int count = [shared.notificationsCount intValue];
//    count += 1 ;
//    shared.notificationsCount = [NSString stringWithFormat:@"%i",count];
}

// Handle notification messages after display notification is tapped by the user.
- (void)userNotificationCenter:(UNUserNotificationCenter *)center
didReceiveNotificationResponse:(UNNotificationResponse *)response
         withCompletionHandler:(void (^)())completionHandler {
//    NSDictionary *userInfo = response.notification.request.content.userInfo;
//    UIApplicationState state = [[UIApplication sharedApplication] applicationState];
//    if (state == UIApplicationStateBackground || state == UIApplicationStateInactive)
//    {
//        NSString *notificationsMsg = userInfo[@"aps"][@"alert"] ;
//        NSMutableArray *newNotifications = [[NSMutableArray alloc]init];
//        NSMutableArray *notifications = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"notifications"]];
//        if (notifications != nil) {
//            [newNotifications addObject:notificationsMsg];
//            [newNotifications addObjectsFromArray:notifications];
//        } else {
//            [newNotifications addObject:notificationsMsg];
//        }
//        [[NSUserDefaults standardUserDefaults] setObject:newNotifications forKey:@"notifications"];
//        [[NSUserDefaults standardUserDefaults]synchronize];
//        SharedData *shared = [SharedData getSharedObject];
//        int count = [shared.notificationsCount intValue];
//        count += 1 ;
//        shared.notificationsCount = [NSString stringWithFormat:@"%i",count];
//    }
}
#endif
// [END ios_10_message_handling]

// [START ios_10_data_message_handling]
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
// Receive data message on iOS 10 devices while app is in the foreground.
- (void)applicationReceivedRemoteMessage:(FIRMessagingRemoteMessage *)remoteMessage {
    // Print full message
    NSLog(@"%@", [remoteMessage appData]);
}
#endif
// [END ios_10_data_message_handling]

// [START refresh_token]
- (void)tokenRefreshNotification:(NSNotification *)notification {
    // Note that this callback will be fired everytime a new token is generated, including the first
    // time. So if you need to retrieve the token as soon as it is available this is where that
    // should be done.
    NSString *refreshedToken = [[FIRInstanceID instanceID] token];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setObject:refreshedToken forKey:@"token"];
    [userDefault synchronize];
    // Connect to FCM since connection may have failed when attempted before having a token.
    [self connectToFcm];
    
    // TODO: If necessary send token to application server.
}
// [END refresh_token]

// [START connect_to_fcm]
- (void)connectToFcm {
    [[FIRMessaging messaging] connectWithCompletion:^(NSError * _Nullable error) {
        if (error != nil) {
            NSLog(@"Unable to connect to FCM. %@", error);
        } else {
            NSLog(@"Connected to FCM.");
        }
    }];
}
// [END connect_to_fcm]

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"Unable to register for remote notifications: %@", error);
}

// This function is added here only for debugging purposes, and can be removed if swizzling is enabled.
// If swizzling is disabled then this function must be implemented so that the APNs token can be paired to
// the InstanceID token.
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    // for development
    //    [[FIRInstanceID instanceID] setAPNSToken:deviceToken type:FIRInstanceIDAPNSTokenTypeSandbox];
    
    // for production
    // [[FIRInstanceID instanceID] setAPNSToken:deviceToken type:FIRInstanceIDAPNSTokenTypeProd];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    [self connectToFcm];
}

// [START disconnect_from_fcm]
- (void)applicationDidEnterBackground:(UIApplication *)application {
    [[FIRMessaging messaging] disconnect];
    NSLog(@"Disconnected from FCM");
    
    SharedData *shared = [SharedData getSharedObject];
    //caching user
    if (shared.user) {
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:shared.user];
        [[NSUserDefaults standardUserDefaults] setObject:data forKey:@"user"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    //caching posts
    if (shared.postsArr) {
        if (shared.postsArr.count > 1) {
            NSData *data = [NSKeyedArchiver archivedDataWithRootObject:shared.postsArr];
            [[NSUserDefaults standardUserDefaults] setObject:data forKey:@"postsArr"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    }
    
    
    //caching all speakers
    if (shared.allSpeakersArr) {
        if (shared.allSpeakersArr.count > 1) {
            NSData *data = [NSKeyedArchiver archivedDataWithRootObject:shared.allSpeakersArr];
            [[NSUserDefaults standardUserDefaults] setObject:data forKey:@"allSpeakersArr"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    }
    
    //caching all agenda
    if (shared.allAgenda) {
        if (shared.allAgenda.count > 1) {
            NSData *data = [NSKeyedArchiver archivedDataWithRootObject:shared.allAgenda];
            [[NSUserDefaults standardUserDefaults] setObject:data forKey:@"allAgenda"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    }
    
    //caching all attendees
    if (shared.allAttendees) {
        if (shared.allAttendees.count > 1) {
            NSData *data = [NSKeyedArchiver archivedDataWithRootObject:shared.allAttendees];
            [[NSUserDefaults standardUserDefaults] setObject:data forKey:@"allAttendees"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    }
    
    //caching all attendees
    if (shared.allMessages) {
        if (shared.allMessages.count > 1) {
            NSData *data = [NSKeyedArchiver archivedDataWithRootObject:shared.allMessages];
            [[NSUserDefaults standardUserDefaults] setObject:data forKey:@"allMessages"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    }
}
// [END disconnect_from_fcm]
- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}

-(void)applicationWillTerminate:(UIApplication *)application {
    SharedData *shared = [SharedData getSharedObject];
    
    //caching user
    if (shared.user) {
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:shared.user];
        [[NSUserDefaults standardUserDefaults] setObject:data forKey:@"user"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    //caching posts
    if (shared.postsArr) {
        if (shared.postsArr.count > 1) {
            NSData *data = [NSKeyedArchiver archivedDataWithRootObject:shared.postsArr];
            [[NSUserDefaults standardUserDefaults] setObject:data forKey:@"postsArr"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    }
    
    //caching slider
    if (shared.sliderArr) {
        if (shared.sliderArr.count > 1) {
            NSData *data = [NSKeyedArchiver archivedDataWithRootObject:shared.sliderArr];
            [[NSUserDefaults standardUserDefaults] setObject:data forKey:@"sliderArr"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    }
    
    //caching all speakers
    if (shared.allSpeakersArr) {
        if (shared.allSpeakersArr.count > 1) {
            NSData *data = [NSKeyedArchiver archivedDataWithRootObject:shared.allSpeakersArr];
            [[NSUserDefaults standardUserDefaults] setObject:data forKey:@"allSpeakersArr"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    }
    
    //caching all agenda
    if (shared.allAgenda) {
        if (shared.allAgenda.count > 1) {
            NSData *data = [NSKeyedArchiver archivedDataWithRootObject:shared.allAgenda];
            [[NSUserDefaults standardUserDefaults] setObject:data forKey:@"allAgenda"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    }
    
    //caching all attendees
    if (shared.allAttendees) {
        if (shared.allAttendees.count > 1) {
            NSData *data = [NSKeyedArchiver archivedDataWithRootObject:shared.allAttendees];
            [[NSUserDefaults standardUserDefaults] setObject:data forKey:@"allAttendees"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    }
    
    //caching all attendees
    if (shared.allMessages) {
        if (shared.allMessages.count > 1) {
            NSData *data = [NSKeyedArchiver archivedDataWithRootObject:shared.allMessages];
            [[NSUserDefaults standardUserDefaults] setObject:data forKey:@"allMessages"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    }
}
@end
