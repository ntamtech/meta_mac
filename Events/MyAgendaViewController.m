//
//  MyAgendaViewController.m
//  Events
//
//  Created by M.I.Kamashany on 3/5/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import "MyAgendaViewController.h"
#import "DynamicItemWidthTabController.h"
#import "SharedData.h"

@interface MyAgendaViewController ()
{
    SharedData *shared ;
}
@property (weak, nonatomic) IBOutlet UILabel *noSessionLabel;

@end

@implementation MyAgendaViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    shared = [SharedData getSharedObject];
    
    if (shared.myAgenda.count == 0) {
        _noSessionLabel.hidden = NO ;
    } else {
        _noSessionLabel.hidden = YES ;
        DynamicItemWidthTabController *controller1 = [[DynamicItemWidthTabController alloc] init];
        controller1.isFullAgenda = NO;
        [self addChildViewController:controller1];
        controller1.view.frame = self.view.frame;
        [self.view addSubview:controller1.view];
        [controller1 didMoveToParentViewController:self];
    }  
}

-(UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent ;
}

@end
