//
//  PaddingLabel.h
//  Messages
//
//  Created by M.I.Kamashany on 3/27/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PaddingLabel : UILabel

@property (nonatomic,assign) BOOL *isMe ;

@end
