//
//  SocialTableViewCell.m
//  Events
//
//  Created by M.I.Kamashany on 3/13/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import "SocialTableViewCell.h"

@implementation SocialTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        NSArray *nibs = [[NSBundle mainBundle] loadNibNamed:@"SocialTableViewCell" owner:self options:nil];
        self = [nibs objectAtIndex:0];
    }
    return self;
}
@end
