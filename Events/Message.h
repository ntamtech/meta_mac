//
//  Message.h
//  Events
//
//  Created by M.I.Kamashany on 2/19/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"

@protocol Message ;

@interface Message : JSONModel

//need these variables in messages view controller
@property (nonatomic) NSString<Optional> *id ;
@property (nonatomic,strong)    NSString<Optional> *name ;
@property (nonatomic,strong)    NSString<Optional> *image ;
@property (nonatomic,strong)    NSString<Optional> *message ;
@property (nonatomic,strong)    NSString<Optional> *message_time ;
@property (nonatomic,strong)    NSString<Optional> *email ;
@property (nonatomic,strong)    NSString<Optional> *read ;
@property (nonatomic,strong)    NSString<Optional> *unread_meassage ;
@property (nonatomic,strong)    NSMutableArray <Message,Optional> *message_details ;

//need this two variables in chatting view controller
@property (nonatomic,strong)    NSString<Optional> *sender_id ;
@property (nonatomic,strong)    NSString<Optional> *if_owner ;

@end

