//
//  PostModel.h
//  Events
//
//  Created by M.I.Kamashany on 2/20/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"
#import "PollModel.h"

@protocol CommentModel ;
@protocol LastLikeUser ;
@protocol PollModel ;

@interface PostModel : JSONModel

@property (nonatomic) NSString<Optional> *id ;
@property (nonatomic,strong)    NSString<Optional> *user_id ;
@property (nonatomic,strong)    NSString<Optional> *username ;
@property (nonatomic,strong)    NSString<Optional> *post ;
@property (nonatomic,strong)    NSString<Optional> *user_image ;
@property (nonatomic,strong)    NSString<Optional> *post_date ;
@property (nonatomic,strong)    NSString<Optional> *make_like ;
@property (nonatomic,strong)    NSMutableArray<CommentModel> *comments ;
@property (nonatomic,strong)    NSString<Optional> *likes ;
@property (nonatomic,strong)    NSMutableArray<LastLikeUser,Optional> *last_likes ;
@property (nonatomic,strong)    PollModel <Optional> *poll_data ;

@end

@interface PhotoModel : JSONModel

@property (nonatomic) NSString<Optional> *id ;
@property (nonatomic,strong)    NSString<Optional> *user_id ;
@property (nonatomic,strong)    NSString<Optional> *username ;
@property (nonatomic,strong)    NSString<Optional> *image ;
@property (nonatomic,strong)    NSString<Optional> *image_time ;
@property (nonatomic,strong)    NSMutableArray<CommentModel,Optional> *comments ;
@property (nonatomic,strong)    NSString<Optional> *likes ;
@property (nonatomic,strong)    NSString<Optional> *make_like ;
@property (nonatomic,strong)    NSMutableArray<LastLikeUser,Optional> *last_likes ;

@end

@interface CommentModel : JSONModel

@property (nonatomic) NSString<Optional> *id ;
@property (nonatomic,strong)    NSString<Optional> *user_id ;
@property (nonatomic,strong)    NSString<Optional> *username ;
@property (nonatomic,strong)    NSString<Optional> *comment ;
@property (nonatomic,strong)    NSString<Optional> *comment_date ;
@property (nonatomic,strong)    NSString<Optional> *user_image ;

@end


@interface LastLikeUser : JSONModel

@property (nonatomic) NSString<Optional> *user_id ;
@property (nonatomic,strong) NSString<Optional> *username ;

@end
