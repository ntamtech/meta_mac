//
//  SpeakerDetailsHeaderView.m
//  Ntam Care
//
//  Created by M.I.Kamashany on 2/12/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import "SpeakerDetailsHeaderView.h"
#import "UIImageView+RoundedImage.h"

@implementation SpeakerDetailsHeaderView

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
       NSArray *nibs = [[NSBundle mainBundle] loadNibNamed:@"SpeakerDetailsHeaderView" owner:self options:nil];
        self = [nibs objectAtIndex:0];
        [self.speakerImageView setRoundedWithCornerRadius:35];
    }
    return self ;
}

- (IBAction)showTwitterAccount:(id)sender {
    [self.delegate completeShow:2];
}

- (IBAction)showLinkedINAccount:(id)sender {
    [self.delegate completeShow:3];
}

- (IBAction)showFacebookAccount:(id)sender {
    [self.delegate completeShow:1];
}
@end
