//
//  CommentsViewController.h
//  Events
//
//  Created by M.I.Kamashany on 3/29/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PostModel.h"
#import "Speaker.h"
#import "MyAgendaListViewController.h"

@protocol CommentsViewControllerDelegate <NSObject>

-(void)updateSession:(Session *)session atIndexPath:(NSIndexPath *)index ;

@end

@interface CommentsViewController : UIViewController


@property (nonatomic,weak) id<CommentsViewControllerDelegate> delegate ;

@property (nonatomic,weak) MyAgendaListViewController *sessionDelegate ;
@property (nonatomic,strong) NSIndexPath *selectedIndex ;

@property (nonatomic,strong) PostModel *selectedPost ;
@property (nonatomic,strong) PhotoModel *selectePhoto ;

@property (nonatomic,strong) Session *selectedSession ;
@property (nonatomic,assign) NSString *agendaDay_number ;

@end
