//
//  MyAgendaListViewController.m
//  Events
//
//  Created by M.I.Kamashany on 3/6/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import "MyAgendaListViewController.h"
#import "MyAgendaTableViewCell.h"
#import "Speaker.h"
#import "SessionDetailsViewController.h"
#import "SharedData.h"
#import "Helper.h"
#import "ServiceHandler.h"
#import "AgendaModel.h"
#import "CommentsViewController.h"

@interface MyAgendaListViewController ()<ServiceHandlerDelegate,MyAgendaTableViewCellDelegate,CommentsViewControllerDelegate>
{
    ServiceHandler *service ;
    SharedData *shared ;
}
@property (nonatomic,weak) IBOutlet UILabel *noCommentsLabel ;
@property (nonatomic,weak) IBOutlet UITableView *agendaSessionsTableView ;

@end

@implementation MyAgendaListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _agendaSessionsTableView.estimatedRowHeight = 115 ;
    _agendaSessionsTableView.rowHeight = UITableViewAutomaticDimension ;
    _agendaSessionsTableView.separatorStyle = UITableViewCellSeparatorStyleNone ;
    _agendaSessionsTableView.allowsSelection = false;
    service = [[ServiceHandler alloc]init];
    service.delegate = self ;
    shared = [SharedData getSharedObject];
}

-(UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent ;
}

-(void)viewWillAppear:(BOOL)animated {
    [_agendaSessionsTableView reloadData];
}
#pragma mark -TableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.sessionOfDay.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellID = @"myAgendaCell";
    MyAgendaTableViewCell *cell = (MyAgendaTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell == nil) {
        cell =[[MyAgendaTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    Session *selectedSession = [self.sessionOfDay objectAtIndex:indexPath.row];
    if (selectedSession.list_of_speaker.count == 0 ) {
        cell.speakerNameLabel.hidden = true ;
        cell.speakerFixedLabel.hidden = true;
    }else{
        cell.speakerNameLabel.hidden = false ;
        cell.speakerFixedLabel.hidden = false;
        Speaker *speaker = selectedSession.list_of_speaker[0];
        cell.speakerNameLabel.text = speaker.name;
    }
    cell.sessionDescLabel.text = selectedSession.session_name ;
    cell.numOfInterested.text = selectedSession.no_of_interested ;
    if (selectedSession.physical_address != nil && ![selectedSession.physical_address isEqualToString:@""]) {
        cell.locationLabel.text = [NSString stringWithFormat:@"Location: %@",selectedSession.physical_address] ;
        cell.locationLabel.hidden = NO;
    } else {
        cell.locationLabel.hidden = YES;
    }
    
    NSString *startTime = [Helper detectPMAM:selectedSession.start_time];
    NSString *endTime = [Helper detectPMAM:selectedSession.end_time];
    cell.startTimeLabel.text = startTime;
    cell.endTimeLabel.text = endTime ;
    cell.selectedIndexPath = indexPath ;
    
    cell.delegate = self ;
    if ([selectedSession.make_like isEqualToString:@"0"]) {
        [cell.likesCountBtn setImage:[UIImage imageNamed:@"notLike"] forState:UIControlStateNormal];
    } else {
        [cell.likesCountBtn setImage:[UIImage imageNamed:@"like"] forState:UIControlStateNormal];
    }
    [cell.likesCountBtn setTitle:selectedSession.session_likes forState:UIControlStateNormal];
    [cell.commentsCountBtn setTitle:[NSString stringWithFormat:@"%lu",(unsigned long)selectedSession.session_comments.count] forState:UIControlStateNormal];
    return cell ;
}

//-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    [tableView deselectRowAtIndexPath:indexPath animated:YES];
//    SessionDetailsViewController *destination = [self.storyboard instantiateViewControllerWithIdentifier:@"SessionDetailsViewController"];
//    destination.selectedSession = [self.sessionOfDay objectAtIndex:indexPath.row];
//    destination.isFromFullAgenda = @"1";
//    destination.agendaDay_number = _agendaDay_number ;
//    destination.selectedSessionIndex = (int)indexPath.row ;
//    destination.isComingFromSpeakers = NO ;
//    [self.navigationController pushViewController:destination animated:YES];
//}

#pragma mark - MyAgendaTableViewCellDelegate

-(void)likeSession:(NSIndexPath *)selectedIndex {
    Session *selectedSession = [self.sessionOfDay objectAtIndex:selectedIndex.row];
    if ([selectedSession.make_like isEqualToString:@"0"]) {
        
        for (int j =0 ; j < shared.allAgenda.count ; j++){
            AgendaModel *agenda = [shared.allAgenda objectAtIndex:j];
            if ([agenda.day_number isEqualToString:self.agendaDay_number]){
                for (int i = 0 ; i <agenda.sessions.count ; i ++){
                    Session *sess = [agenda.sessions objectAtIndex:i];
                    if ([selectedSession.id isEqualToString:sess.id]) {
                        sess.make_like = @"1" ;
                        selectedSession.make_like = @"1" ;
                        int likes = [selectedSession.session_likes intValue] + 1 ;
                        selectedSession.session_likes = [NSString stringWithFormat:@"%i",likes];
                        [agenda.sessions removeObjectAtIndex:i];
                        [agenda.sessions insertObject:sess atIndex:i];
                        break;
                    }
                }
                [shared.allAgenda removeObjectAtIndex:j];
                [shared.allAgenda insertObject:agenda atIndex:j];
                [self.sessionOfDay removeObjectAtIndex:selectedIndex.row];
                [self.sessionOfDay insertObject:selectedSession atIndex:selectedIndex.row];
                
                [self.agendaSessionsTableView beginUpdates];
                [self.agendaSessionsTableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:selectedIndex, nil] withRowAnimation:UITableViewRowAnimationNone];
                [self.agendaSessionsTableView endUpdates];
                //change session data in the server .
                [service likeSession:[NSNumber numberWithInt:[shared.user.id intValue]] sessionID:[NSNumber numberWithInt:[selectedSession.id intValue]]];
            }
        }
    }else{
        for (int j =0 ; j < shared.allAgenda.count ; j++){
            AgendaModel *agenda = [shared.allAgenda objectAtIndex:j];
            if ([agenda.day_number isEqualToString:self.agendaDay_number]){
                for (int i = 0 ; i <agenda.sessions.count ; i ++){
                    Session *sess = [agenda.sessions objectAtIndex:i];
                    if ([selectedSession.id isEqualToString:sess.id]) {
                        sess.make_like = @"0" ;
                        selectedSession.make_like = @"0" ;
                        int likes = [selectedSession.session_likes intValue] - 1 ;
                        selectedSession.session_likes = [NSString stringWithFormat:@"%i",likes];
                        [agenda.sessions removeObjectAtIndex:i];
                        [agenda.sessions insertObject:sess atIndex:i];
                        break;
                    }
                }
                [shared.allAgenda removeObjectAtIndex:j];
                [shared.allAgenda insertObject:agenda atIndex:j];
                [self.sessionOfDay removeObjectAtIndex:selectedIndex.row];
                [self.sessionOfDay insertObject:selectedSession atIndex:selectedIndex.row];
                
                [self.agendaSessionsTableView beginUpdates];
                [self.agendaSessionsTableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:selectedIndex, nil] withRowAnimation:UITableViewRowAnimationNone];
                [self.agendaSessionsTableView endUpdates];
                //change session data in the server .
                [service likeSession:[NSNumber numberWithInt:[shared.user.id intValue]] sessionID:[NSNumber numberWithInt:[selectedSession.id intValue]]];
            }
        }
    }
}

-(void)likeSessionSuccessfully {
    
}

-(void)showCommentsAtIndex:(NSIndexPath *)selectedIndex {
    Session *sess = [self.sessionOfDay objectAtIndex:selectedIndex.row];
    CommentsViewController *destination = [self.storyboard instantiateViewControllerWithIdentifier:@"CommentsViewController"];
    destination.selectedSession = sess ;
    destination.agendaDay_number = self.agendaDay_number ;
    destination.sessionDelegate = self ;
    destination.selectePhoto = nil ;
    destination.selectedPost = nil ;
    [self.navigationController pushViewController:destination animated:YES];
}


-(void)updateSession:(Session *)session atIndexPath:(NSIndexPath *)index {
    [self.sessionOfDay removeObjectAtIndex:index.row];
    [self.sessionOfDay insertObject:session atIndex:index.row];
    [_agendaSessionsTableView reloadData];
}


@end
