//
//  SpeakerTableViewCell.h
//  Events
//
//  Created by M.I.Kamashany on 2/12/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SpeakerTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *speakerIcon;
@property (weak, nonatomic) IBOutlet UILabel *speakerNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *speakerJobDesc;


@end
