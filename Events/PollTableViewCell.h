//
//  PollTableViewCell.h
//  Events
//
//  Created by M.I.Kamashany on 3/2/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PollModel.h"

@protocol PollTableViewCellDelegate <NSObject>

-(void)showPollDetails:(NSIndexPath *)selectedIndex ;

@end

@interface PollTableViewCell : UITableViewCell

@property (nonatomic,weak) id<PollTableViewCellDelegate> delegate ;
@property (nonatomic,weak) IBOutlet UILabel *pollQuestionLabel ;
@property (nonatomic,strong) PollModel *selectedPoll ;
@property (nonatomic,strong) NSIndexPath *selectedIndex ;
-(IBAction)ShowPoll:(id)sender ;

@end
