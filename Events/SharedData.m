//
//  SharedData.m
//  iJobs
//
//  Created by M.I.Kamashany on 12/11/16.
//  Copyright © 2016 NTAMNTAMTech. All rights reserved.
//

#import "SharedData.h"

@implementation SharedData

static SharedData *sharedObject = nil ;

+(id)getSharedObject {
    static dispatch_once_t onceToken ;
    dispatch_once(&onceToken, ^{
        sharedObject = [[self alloc]init];
        sharedObject.notificationsCount = @"0";
    });
    return sharedObject ;
}

+(void)deleteUser {
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"user"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    sharedObject.user = nil ;
}

+(void)deletePosts {
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"postsArr"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    sharedObject.postsArr = nil ;
}

+(void)deleteSliderImages {
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"sliderArr"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    sharedObject.sliderArr = nil ;
}

+(void)deleteSpeakers {
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"allSpeakersArr"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    sharedObject.allSpeakersArr = nil ;
}

+(void)deleteAttendees {
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"allAttendees"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    sharedObject.allAttendees = nil ;
}

+(void)deleteAgenda {
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"allAgenda"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    sharedObject.allAgenda = nil ;
}

+(void)deleteAllCachedData {
    [self deleteUser];
    [self deletePosts];
    [self deleteSpeakers];
    [self deleteAttendees];
    [self deleteAgenda];
    [self deleteSliderImages];
}
//+(void)deleteMessages {
//    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"user"];
//    [[NSUserDefaults standardUserDefaults] synchronize];
//    sharedObject. = nil ;
//}

@end
