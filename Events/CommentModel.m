//
//  CommentModel.m
//  Events
//
//  Created by M.I.Kamashany on 2/20/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import "PostModel.h"

@implementation CommentModel


- (void)encodeWithCoder:(NSCoder *)encoder {
    //Encode properties, other class variables, etc
    [encoder encodeObject:self.id forKey:@"id"];
    [encoder encodeObject:self.user_id forKey:@"user_id"];
    [encoder encodeObject:self.username forKey:@"username"];
    [encoder encodeObject:self.comment forKey:@"comment"];
    [encoder encodeObject:self.comment_date forKey:@"comment_date"];
    [encoder encodeObject:self.user_image forKey:@"user_image"];
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        //decode properties, other class vars
        self.id= [decoder decodeObjectForKey:@"id"];
        self.user_id =[decoder decodeObjectForKey:@"user_id"];
        self.username =[decoder decodeObjectForKey:@"username"];
        self.comment =[decoder decodeObjectForKey:@"comment"];
        self.comment_date= [decoder decodeObjectForKey:@"comment_date"];
        self.user_image =[decoder decodeObjectForKey:@"user_image"];
    }
    return self;
}



@end
