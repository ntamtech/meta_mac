//
//  Constants.h
//  iJobs
//
//  Created by M.I.Kamashany on 12/7/16.
//  Copyright © 2016 NTAMNTAMTech. All rights reserved.
//

#ifndef Constants_h
#define Constants_h

#define FORGETPASSWORD_URL @"http://gps.weblinesolutions.com/pparents/forgotPassword"
//update base url (alaa)
//#define BASE_URL @"http://ntam.tech/techne_summit_service/"
//#define BASE_URL @"http://ntam.tech/eye_care_service/"
#define BASE_URL @"http://ntam.tech/istanbul_event_service/"

#define EVENT_ID @"1"
#define EmailRequired @"ادخل الايميل من فضلك"
#define EmailRegularExpressionRequired @"ايميل خاطئ، ادخل ايميل صحيح"
#define NameRequired @"ادخل الاسم من فضلك"
#define PasswordRequired @"ادخل كلمة المرور من فضلك"
#define PasswordmatchedRequired @"كلمتي المرور غير متشابهتان"
#define NoInternetConnection @"لا يوجد انترنت "
#define requestFailed @"There is a problem, try again"
#define NotFound @"لا يوجد"
#define NeedJob @"باحث عن وظيفة"
#define Owner @"صاحب عمل"
#define NoData @"No Data"
#define SaudiArabiaLink @"saudi-arabia"
#define TWITTER1 @"https://twitter.com/search?q=%23"
#define TWITTER2 @"&src=typd&lang=en"


//arabic langauge
#define RQUESTFAILED @"يوجد مشكلة حاول مرة آخري"
#define LOGINFAILEDMESSAGE @"خطأ في الايميل او الباسورد"



#define LOGINFAILED @"Email Or Password is incorrect , please retry!"
#define LOGIN_SUCCESS @"login success"
#define update_profile_message @"data updated successfully"
#define ADD_NURSE_MESSAGE @"Nurse Added Successfully"


#define QUESTION_KEY @"Qkey"
#endif /* Constants_h */

// for class WebViewViewController
// In a header file

typedef NS_ENUM(NSInteger) {
    GeneralInfo,
    LeaderShipPrinciples,
    FerringPhilosophy,
    Dinner
} ContentType;
