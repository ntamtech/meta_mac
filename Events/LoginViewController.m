//
//  LoginViewController.m
//  Events
//
//  Created by M.I.Kamashany on 2/9/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import "LoginViewController.h"
#import "NewsFeedsViewController.h"
#import "RegisterationViewController.h"
#import "ServiceHandler.h"
#import "MBProgressHUD.h"
#import "UIViewController+Alert.h"
#import "Helper.h"
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "SharedData.h"
#import <FirebaseDatabase/FirebaseDatabase.h>
#import "SideBarViewController.h"

@interface LoginViewController ()<ServiceHandlerDelegate,UITextFieldDelegate,FBSDKLoginButtonDelegate>
{
    ServiceHandler *service ;
    SharedData *shared ;
}
@property (weak, nonatomic) IBOutlet UITextField *emailTF;
@property (weak, nonatomic) IBOutlet UITextField *codeTF;
@property (weak, nonatomic) IBOutlet UIButton *signinBtn;
- (IBAction)signin:(id)sender;
@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.signinBtn.layer.masksToBounds = YES ;
    self.signinBtn.layer.cornerRadius = 25 ;
    
    service = [[ServiceHandler alloc]init];
    service.delegate = self ;
    shared = [SharedData getSharedObject];
//    [self observeQuestions];
}

-(void)viewWillAppear:(BOOL)animated{}

-(UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent ;
}

- (IBAction)signin:(id)sender {
    if ([Helper checkInternet]) {
        if ([_emailTF.text isEqualToString:@""]) {
            [self showAlert:@"Enter Username First"];
        }else{
           
            if ([_codeTF.text isEqualToString:@""]) {
                [self showAlert:@"Enter Password First"];
            }else{
                NSString *emailtrimmed = [_emailTF.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                [self.view endEditing:YES];
                [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                NSString *tokenID = [[NSUserDefaults standardUserDefaults] objectForKey:@"token"];
                [service loginByEmail:[emailtrimmed lowercaseString] code:_codeTF.text registerID:tokenID eventID:[NSNumber numberWithInt:1]];
            }
        }
    } else {
        [self showAlert:@"No Internet Connection!"];
    }
    
}

-(void)loginSuccess {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    SharedData *shared = [SharedData getSharedObject];
    if ([shared.user.email isEqualToString:@""]) {
        RegisterationViewController *destination = [self.storyboard instantiateViewControllerWithIdentifier:@"RegisterationViewController"];
        [self.navigationController pushViewController:destination animated:YES];
        
    }else{
        SideBarViewController *destination = [self.storyboard instantiateViewControllerWithIdentifier:@"SideBarViewController"];
        [self.navigationController pushViewController:destination animated:YES];
    }
    
}

-(void)requestFailWithError:(NSString *)err {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [self showAlert:err];
}

#pragma mark - UITextFieldDelegate 

//- (void)textFieldDidEndEditing:(UITextField *)textField {
//    if (textField == _emailTF) {
//        [_codeTF becomeFirstResponder];
//    } else {
//        [self.view endEditing:YES];
//    }
//}


//-(void)observeQuestions {
//    FIRDatabaseReference *ref = [[FIRDatabase database] reference];
//    [[[ref child:@"questions"] queryOrderedByKey] observeEventType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
//        NSDictionary *postDict = snapshot.value;
//        for (NSString* key in postDict.allKeys) {
//            NSDictionary *value = [postDict objectForKey:key];
//            printf("%@", value);
//        }
//    }];
//}

@end
