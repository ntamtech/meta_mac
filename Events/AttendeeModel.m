//
//  AttendeeModel.m
//  Events
//
//  Created by M.I.Kamashany on 2/26/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import "AttendeeModel.h"

@implementation AttendeeModel

- (void)encodeWithCoder:(NSCoder *)encoder {
    //Encode properties, other class variables, etc
    [encoder encodeObject:self.id forKey:@"id"];
    [encoder encodeObject:self.name forKey:@"name"];
    [encoder encodeObject:self.facebook_link forKey:@"facebook_link"];
    [encoder encodeObject:self.twitter_link forKey:@"twitter_link"];
    [encoder encodeObject:self.linkedin_link forKey:@"linkedin_link"];
    [encoder encodeObject:self.bio forKey:@"bio"];
    [encoder encodeObject:self.image forKey:@"image"];
    [encoder encodeObject:self.position forKey:@"position"];
    [encoder encodeObject:self.company forKey:@"company"];
    [encoder encodeObject:self.event_id forKey:@"event_id"];
    [encoder encodeObject:self.event_id forKey:@"user_types"];
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        //decode properties, other class vars
        self.id = [decoder decodeObjectForKey:@"id"];
        self.name = [decoder decodeObjectForKey:@"name"];
        self.facebook_link = [decoder decodeObjectForKey:@"facebook_link"];
        self.twitter_link = [decoder decodeObjectForKey:@"twitter_link"];
        self.linkedin_link = [decoder decodeObjectForKey:@"linkedin_link"];
        self.bio = [decoder decodeObjectForKey:@"bio"];
        self.image = [decoder decodeObjectForKey:@"image"];
        self.position = [decoder decodeObjectForKey:@"position"];
        self.company = [decoder decodeObjectForKey:@"company"];
        self.event_id = [decoder decodeObjectForKey:@"event_id"];
        self.user_types = [decoder decodeObjectForKey:@"user_types"];
    }
    return self;
}


@end
