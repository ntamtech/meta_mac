//
//  AnswerViewController.m
//  Events
//
//  Created by M.I.Kamashany on 3/13/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import "AnswerViewController.h"
#import "SharedData.h"
#import "ServiceHandler.h"
#import "Helper.h"
#import "MBProgressHUD.h"
#import "PollModel.h"
#import "AnswerViewTableViewCell.h"
#import "UIViewController+Alert.h"
#import "SideBarViewController.h"

@interface AnswerViewController ()<ServiceHandlerDelegate,UITableViewDelegate,UITableViewDataSource>
{
    ServiceHandler *service ;
    SharedData *shared ;
    PollModel *selectedPoll ;
    BOOL  showProgress ;
    Choice *selectedChoice ;
}

@property (nonatomic,weak) IBOutlet UILabel *pollQuestionLabel ;
@property (nonatomic,weak) IBOutlet UIView *answeredView ;
@property (nonatomic,weak) IBOutlet UILabel *resultsLabel ;
@property (nonatomic,weak) IBOutlet UITableView *answersTableView ;
@property (nonatomic,weak) IBOutlet UIButton *submitBtn ;

@end

@implementation AnswerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    service = [[ServiceHandler alloc]init];
    service.delegate = self ;
    shared = [SharedData getSharedObject];
    showProgress = NO ;
    _answersTableView.estimatedRowHeight = 54 ;
    _answersTableView.rowHeight = UITableViewAutomaticDimension ;
}

-(void)viewWillAppear:(BOOL)animated{
    if (_pollIndexAtPolls) {
        selectedPoll = [shared.pollsArr objectAtIndex:_pollIndexAtPolls.row];
    } else {
        PostModel *post = [shared.postsArr objectAtIndex:_postIndexAtNewsFeeds.row];
        selectedPoll = post.poll_data ;
    }
    _pollQuestionLabel.text = selectedPoll.question ;
    if (selectedPoll.user_answer != nil && ![selectedPoll.user_answer isEqualToString:@""]) {
        for (Choice *choice in selectedPoll.choices) {
            if ([choice.id isEqualToString:selectedPoll.user_answer]) {
                selectedChoice = choice ;
                break ;
            }
        }
    }
}


#pragma mark -TableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1 ;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return selectedPoll.choices.count ;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellID = @"answerViewCell";
    AnswerViewTableViewCell *cell = (AnswerViewTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell == nil) {
        cell =[[AnswerViewTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    return cell ;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    Choice *choice = [selectedPoll.choices objectAtIndex:indexPath.row];
    AnswerViewTableViewCell *selectedCell = (AnswerViewTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
     if (selectedChoice != nil) {
        if ([selectedChoice.id isEqualToString:choice.id]){
            selectedChoice = nil ;
            selectedCell.iconImageView.image = [UIImage imageNamed:@"rectangle-10"];
        }else{
            selectedChoice = choice ;
            selectedCell.iconImageView.image = [UIImage imageNamed:@"Checkbox-10"];
        }
    }else{
        selectedChoice = choice ;
        selectedCell.iconImageView.image = [UIImage imageNamed:@"Checkbox-10"];
    }
    [_answersTableView reloadData];
}

#pragma mark - ServiceHandlerDelegate

-(void)getPollChoices:(NSMutableArray *)choices {
    _submitBtn.hidden = YES ;
    showProgress = YES ;
    _answersTableView.separatorStyle = UITableViewCellSeparatorStyleNone ;
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [selectedPoll.choices removeAllObjects];
    [selectedPoll.choices addObjectsFromArray:choices];
    
    selectedPoll.user_answer = [NSString stringWithFormat:@"%@",selectedChoice.id];
    if (_pollIndexAtPolls) {
        [shared.pollsArr removeObjectAtIndex:_pollIndexAtPolls.row];
        [shared.pollsArr insertObject:selectedPoll atIndex:_pollIndexAtPolls.row];
        for (int i = 0 ; i < shared.postsArr.count  ; i++) {
            PostModel *post = [shared.postsArr objectAtIndex:i];
            if ([post.poll_data.id isEqualToString:selectedPoll.id]){
                post.poll_data = selectedPoll;
                [shared.postsArr removeObjectAtIndex:_postIndexAtNewsFeeds.row];
                [shared.postsArr insertObject:post atIndex:_postIndexAtNewsFeeds.row];
                break;
            }
        }
    } else {
        PostModel *post = [shared.postsArr objectAtIndex:_postIndexAtNewsFeeds.row];
        post.poll_data = selectedPoll;
        [shared.postsArr removeObjectAtIndex:_postIndexAtNewsFeeds.row];
        [shared.postsArr insertObject:post atIndex:_postIndexAtNewsFeeds.row];
        
    }
    [_answersTableView reloadData];
}


-(void)requestFailWithError:(NSString *)err {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [self showAlert:err];
}

-(IBAction)submitpollChoice:(id)sender {
    if (selectedChoice != nil) {
        if (![Helper checkInternet]) {
            [self showAlert:@"No Internet Connection!"];
        } else {
            
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            [service answerPoll:[NSNumber numberWithInt:[shared.user.id intValue]] PollID:[NSNumber numberWithInt:[selectedPoll.id intValue]] choiceID:[NSNumber numberWithInt:[selectedChoice.id intValue]]];
        }
    } else {
        [self showAlert:@"Select your answer"];
    }
    
}

-(IBAction)showMenu:(id)sender {
    SideBarViewController *destination = (SideBarViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"SideBarViewController"];
    [self.navigationController pushViewController:destination animated:YES];
}

-(IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
