//
//  AnnouncementTypeTableViewCell.m
//  Events
//
//  Created by M.I.Kamashany on 3/7/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import "AnnouncementTypeTableViewCell.h"

@interface AnnouncementTypeTableViewCell()

@property (nonatomic,weak) IBOutlet UIView *someView ;

@end


@implementation AnnouncementTypeTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        NSArray *nibs = [[NSBundle mainBundle] loadNibNamed:@"AnnouncementTypeTableViewCell" owner:self options:nil];
        self = [nibs objectAtIndex:0];
    }
    self.someView.layer.borderColor = [UIColor grayColor].CGColor;
    self.someView.layer.borderWidth = 1 ;
    return self;
}


@end
