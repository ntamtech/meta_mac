//
//  AttendeeTableViewCell.h
//  Events
//
//  Created by M.I.Kamashany on 2/26/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AttendeeModel.h"

@protocol AttendeeTableViewCellDelegate <NSObject>

@optional
-(void)sendMessageToAttendee:(AttendeeModel *)attendee ;

@end

@interface AttendeeTableViewCell : UITableViewCell

@property(nonatomic,weak) id<AttendeeTableViewCellDelegate> delegate ;

@property (weak, nonatomic) IBOutlet UILabel *attendeeNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *attendeePositionLabel;
@property (weak, nonatomic) IBOutlet UIImageView *attendeeImageView;
@property (weak, nonatomic) IBOutlet UIButton *sendMessageBtn;
@property (weak, nonatomic) IBOutlet UIButton *sendMessageBigBtn;

@property (nonatomic,strong) AttendeeModel *selectedAttendee ;

- (IBAction)sendMessge:(id)sender;

@end
