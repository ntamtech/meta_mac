//
//  LaunchingViewController.m
//  Events
//
//  Created by M.I.Kamashany on 2/9/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import "LaunchingViewController.h"
#import "LoginViewController.h"
#import "User.h"
#import "SharedData.h"
#import "NewsFeedsViewController.h"
#import "ServiceHandler.h"
#import "Helper.h"
#import "UIViewController+Alert.h"
#import "RegisterationViewController.h"
#import "SideBarViewController.h"

@interface LaunchingViewController ()<ServiceHandlerDelegate> {
    SharedData *shared ;
    ServiceHandler *service ;
}

@property (weak, nonatomic) IBOutlet UIButton *refreshBtn;
- (IBAction)refresh:(id)sender;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityLoading;


@end

@implementation LaunchingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    shared = [SharedData getSharedObject];
    service = [[ServiceHandler alloc]init];
    service.delegate = self ;
}

-(void)viewWillAppear:(BOOL)animated {
    self.refreshBtn.hidden = YES ;
     NSString *tokenID = [[NSUserDefaults standardUserDefaults] objectForKey:@"token"];
    int delayTime = 0 ;
    if (tokenID == nil)  {
        delayTime = 5 ;
    }
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayTime * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:@"user"];
        if (data != nil) {
            User *user = [NSKeyedUnarchiver unarchiveObjectWithData:data];
            shared.user = user ;
            if (shared.user.email == nil) {
                RegisterationViewController *destination = [self.storyboard instantiateViewControllerWithIdentifier:@"RegisterationViewController"];
                [self.navigationController pushViewController:destination animated:YES];
            }else{
                if ([shared.user.email isEqualToString:@""]) {
                    RegisterationViewController *destination = [self.storyboard instantiateViewControllerWithIdentifier:@"RegisterationViewController"];
                    [self.navigationController pushViewController:destination animated:YES];
                }else{
                    SideBarViewController *destination = [self.storyboard instantiateViewControllerWithIdentifier:@"SideBarViewController"];
                    [self.navigationController pushViewController:destination animated:YES];
                }
            }
        } else {
            LoginViewController *destination = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
            [self.navigationController pushViewController:destination animated:YES];
        }
    });
}



-(UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent ;
}


@end
