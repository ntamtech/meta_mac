//
//  FeedTableViewCell.h
//  Events
//
//  Created by M.I.Kamashany on 2/14/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol FeedTableViewCellDelegate <NSObject>

-(void)likePost:(NSIndexPath *)selectedIndex ;
-(void)showCommentsAtIndex:(NSIndexPath *)selectedIndex ;
-(void)showPollBy:(NSIndexPath *)selectedIndex ;

@end

@interface FeedTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *islikeImageview;
@property (nonatomic,weak) id<FeedTableViewCellDelegate> delegate ;
@property (weak, nonatomic) IBOutlet UIView *upperView;
@property (weak, nonatomic) IBOutlet UIView *lowerView;
@property (nonatomic,strong) NSIndexPath *selectedIndexPath ;
@property (weak, nonatomic) IBOutlet UILabel *feedOwnerLabel;
@property (weak, nonatomic) IBOutlet UILabel *feedDescLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *likesCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *CommentsCountLabel;
@property (weak, nonatomic) IBOutlet UIButton *likeBtn;
@property (weak, nonatomic) IBOutlet UILabel *feedPollDescLabel;
@property (weak, nonatomic) IBOutlet UIButton *showPollBtn;
@property (weak, nonatomic) IBOutlet UIImageView *FeedOwnerImageView;

- (IBAction)makeLike:(id)sender;
- (IBAction)addComment:(id)sender;
- (IBAction)showPollDetails:(id)sender;

@end
