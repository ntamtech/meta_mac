//
//  PublicCommentTableViewCell.m
//  Events
//
//  Created by M.I.Kamashany on 3/29/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import "PublicCommentTableViewCell.h"

@implementation PublicCommentTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        NSArray *nibs = [[NSBundle mainBundle] loadNibNamed:@"PublicCommentTableViewCell" owner:self options:nil];
        self = [nibs objectAtIndex:0];
    }
    self.commentOwnerImageView.layer.cornerRadius = 15 ;
    self.commentOwnerImageView.layer.masksToBounds = YES ;
    return self;
}
@end
