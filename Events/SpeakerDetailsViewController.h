//
//  SpeakerDetailsViewController.h
//  Events
//
//  Created by M.I.Kamashany on 2/11/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Speaker.h"

@interface SpeakerDetailsViewController : UIViewController

@property (nonatomic , strong) Speaker *selectedSpeaker ;
@property (nonatomic , assign) int selectedSpeakerIndex ;
@end
