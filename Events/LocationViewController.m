//
//  LocationViewController.m
//  Events
//
//  Created by M.I.Kamashany on 2/11/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import "LocationViewController.h"
#import "MBProgressHUD.h"
#import "UIViewController+Alert.h"
#import "Helper.h"
#import "SharedData.h"

@interface LocationViewController ()<UIWebViewDelegate>
{
    SharedData *shared ;
}

@property (weak, nonatomic) IBOutlet UIWebView *locationWebView;
@property (weak, nonatomic) IBOutlet UILabel *errorLabel;

@end



@implementation LocationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    shared = [SharedData getSharedObject];
}

-(UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent ;
}

-(void)viewWillAppear:(BOOL)animated {
    if (![Helper checkInternet]) {
        self.locationWebView.hidden = YES ;
        self.errorLabel.hidden = NO ;
    }else{
        self.errorLabel.hidden = YES ;
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [self.locationWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:shared.about.event_location]]];
    }
}

#pragma mark- UIWebViewDelegate 


- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    self.locationWebView.hidden = YES ;
    self.errorLabel.hidden = NO ;
}

-(IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
