//
//  SpeakerDetailsHeaderView.h
//  Ntam Care
//
//  Created by M.I.Kamashany on 2/12/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SpeakerDetailsHeaderViewDelegate <NSObject>

@optional
-(void)completeShow:(int)flag ;

@end


@interface SpeakerDetailsHeaderView : UIView

@property (nonatomic,weak) id<SpeakerDetailsHeaderViewDelegate> delegate ;
@property (weak, nonatomic) IBOutlet UILabel *speakerNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *speakerImageView;
@property (weak, nonatomic) IBOutlet UILabel *speakerDetailsLabel;
@property (weak, nonatomic) IBOutlet UITextView *speakerMoreInfo;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *speakerMoreInfoHightConstraint;

- (IBAction)showTwitterAccount:(id)sender;
- (IBAction)showLinkedINAccount:(id)sender;
- (IBAction)showFacebookAccount:(id)sender;

@end
