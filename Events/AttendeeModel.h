//
//  AttendeeModel.h
//  Events
//
//  Created by M.I.Kamashany on 2/26/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"

@interface AttendeeModel : JSONModel

@property (nonatomic) NSString<Optional> *id ;
@property (nonatomic,strong)  NSString<Optional> *name ;
@property (nonatomic,strong)  NSString<Optional> *facebook_link ;
@property (nonatomic,strong)  NSString<Optional> *twitter_link ;
@property (nonatomic,strong)  NSString<Optional> *linkedin_link ;
@property (nonatomic,strong)  NSString<Optional> *bio ;
@property (nonatomic,strong)  NSString<Optional> *image ;
@property (nonatomic,strong)  NSString<Optional> *position ;
@property (nonatomic,strong)  NSString<Optional> *company ;
@property (nonatomic,strong)  NSString<Optional> *event_id ;
@property (nonatomic,strong)  NSMutableArray<Optional> *user_types ;

@end
