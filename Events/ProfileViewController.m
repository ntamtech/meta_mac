//
//  ProfileViewController.m
//  Events
//
//  Created by M.I.Kamashany on 2/28/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import "ProfileViewController.h"
#import "SideBarViewController.h"
#import "SocialTableViewCell.h"
#import "SharedData.h"
#import "ServiceHandler.h"
#import "Helper.h"
#import "NewBioViewController.h"
#import "UIView+Toast.h"
#import "UIImageView+WebCache.h"
#import "EditBasicUserDataViewController.h"
#import "UIViewController+Alert.h"
#import "EditSocailLinksViewController.h"
#import "SocialLinksViewController.h"

@interface ProfileViewController ()<UITableViewDelegate,UITableViewDataSource,ServiceHandlerDelegate,UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIGestureRecognizerDelegate>{
    
    SharedData *shared ;
    ServiceHandler *service ;
    NSString *imageBase64 ;
    UIImagePickerController *imagePicker ;
    BOOL isLoaded ;
}

//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstriantOfProfileImage;
//@property (weak, nonatomic) IBOutlet UIView *socialAccountViewContainer;
@property (nonatomic , weak) IBOutlet UIScrollView *scrollView ;
@property (nonatomic , weak) IBOutlet UIView *containerView ;

@end

@implementation ProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    service = [[ServiceHandler alloc]init];
    service.delegate = self ;
    shared = [SharedData getSharedObject];
    [_editBioBtn addTarget:self action:@selector(addNewBio:) forControlEvents:UIControlEventTouchUpInside];
    
    // add tap gesture on profile picture
    _userImageView.userInteractionEnabled = YES;
    UITapGestureRecognizer *pgr = [[UITapGestureRecognizer alloc]
                                     initWithTarget:self action:@selector(selectProfileImage:)];
    pgr.delegate = self;
    [_userImageView addGestureRecognizer:pgr];
    isLoaded = false ;
}

-(void)viewWillAppear:(BOOL)animated {
    
    if (self.attendee) {
        self.locationLabel.hidden = YES ;
        [self.view layoutIfNeeded];
        _userImageView.userInteractionEnabled = NO ;
        [_userImageView sd_setImageWithURL:[NSURL URLWithString:self.attendee.image] placeholderImage:[UIImage imageNamed:@"profile-image"]];
        _nameLabel.text = self.attendee.name ;
        if (self.attendee.company != nil && ![self.attendee.company isEqualToString:@""]) {
            _positionLabel.text = self.attendee.company ;
            _positionLabel.hidden = NO;
        } else {
            _positionLabel.hidden = YES;
        }
        
        if (self.attendee.bio != nil && ![self.attendee.bio isEqualToString:@""]) {
            _bioLabel.text = self.attendee.bio ;
        } else {
            _bioLabel.text = @"No bio" ;
        }
        _bioBtn.hidden = YES ;
        _editBioBtn.hidden = YES ;
        _socialLinksBtn.hidden = YES ;
        _personalDataBtn.hidden = YES ;
        dispatch_async(dispatch_get_main_queue(), ^{
            _scrollView.contentSize = CGSizeMake(0, _containerView.bounds.size.height);
        });
        if (isLoaded) {
            [_socialLinksTableView reloadData];
        }
    } else {
        _userImageView.userInteractionEnabled = YES ;
        [_userImageView sd_setImageWithURL:[NSURL URLWithString:shared.user.image] placeholderImage:[UIImage imageNamed:@"profile-image"]];
        _nameLabel.text = shared.user.name ;
        _positionLabel.text = shared.user.position ;
        _locationLabel.text = shared.user.location ;
        _bioLabel.text = shared.user.bio ;
        
        if ([shared.user.bio isEqualToString:@""]) {
            _bioBtn.hidden = NO ;
            _editBioBtn.hidden = YES ;
        } else {
            _bioBtn.hidden = YES ;
            _editBioBtn.hidden = NO ;
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            _scrollView.contentSize = CGSizeMake(0, _containerView.bounds.size.height);
        });
        if (isLoaded) {
            [_socialLinksTableView reloadData];
        }
    }
}

-(UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent ;
}

#pragma mark -TableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return  1 ;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3 ;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellID = @"socialCell";
    SocialTableViewCell *cell = (SocialTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell == nil) {
        cell =[[SocialTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    
    switch (indexPath.row) {
        case 0:
            cell.iconImageView.image = [UIImage imageNamed:@"facebook-1"];
            if (self.attendee) {
                if ([self.attendee.facebook_link isEqualToString:@""] || self.attendee.facebook_link == nil) {
                    cell.linkLabel.text = @"Facebook Link";
                }else{
                    cell.linkLabel.text = @"Facebook Link";
//                    cell.linkLabel.text = self.attendee.facebook_link;
                }
            } else {
                if ([shared.user.fb_link isEqualToString:@""] || shared.user.fb_link == nil){
                    cell.linkLabel.text = @"Facebook Link";
                }else{
                    cell.linkLabel.text = @"Facebook Link";
//                    cell.linkLabel.text = shared.user.fb_link;
                }
            }
            break;
        case 1:
            if (self.attendee) {
                cell.iconImageView.image = [UIImage imageNamed:@"linkedin-1"];
                if ([self.attendee.linkedin_link isEqualToString:@""] || self.attendee.linkedin_link == nil){
                    cell.linkLabel.text = @"Linkedin Link";
                }else{
                    cell.linkLabel.text = @"Linkedin Link";
//                    cell.linkLabel.text = self.attendee.linkedin_link ;
                }
            }else{
                cell.iconImageView.image = [UIImage imageNamed:@"linkedin-1"];
                if ([shared.user.linkedin_link isEqualToString:@""] || shared.user.linkedin_link == nil){
                    cell.linkLabel.text = @"Linkedin Link";
                }else{
                    cell.linkLabel.text = @"Linkedin Link";
//                    cell.linkLabel.text = shared.user.linkedin_link ;
                }
            }
            
            break;
        case 2:
            if (self.attendee) {
                cell.iconImageView.image = [UIImage imageNamed:@"twitter-2"];
                if ([self.attendee.twitter_link isEqualToString:@""] || self.attendee.twitter_link == nil){
                    cell.linkLabel.text = @"Twitter Link";
                }else{
                    cell.linkLabel.text = @"Twitter Link";
//                    cell.linkLabel.text = self.attendee.twitter_link ;
                }
            }else{
                cell.iconImageView.image = [UIImage imageNamed:@"twitter-2"];
                if ([shared.user.twitter_link isEqualToString:@""] || shared.user.twitter_link == nil){
                    cell.linkLabel.text = @"Twitter Link";
                }else{
                    cell.linkLabel.text = @"Twitter Link";
//                    cell.linkLabel.text = shared.user.twitter_link ;
                }
            }
            break;
        default:
            break;
    }
    tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    return cell ;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.row) {
        case 0:
            if (self.attendee) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.attendee.facebook_link]];
            } else {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:shared.user.fb_link]];
            }
            break;
        case 1:
            if (self.attendee) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.attendee.linkedin_link]];
            } else {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:shared.user.linkedin_link]];
            }
            break;
        case 2:
            if (self.attendee) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.attendee.twitter_link]];
            }else{
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:shared.user.twitter_link]];
            }
            break;
        
        default:
            break;
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(IBAction)editPersonalData:(id)sender{
    EditBasicUserDataViewController *destination = [self.storyboard instantiateViewControllerWithIdentifier:@"EditBasicUserDataViewController"];
    [self.navigationController pushViewController:destination animated:YES];
}

-(IBAction)addNewBio:(id)sender {
    NewBioViewController *destination = (NewBioViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"NewBioViewController"];
    [self.navigationController pushViewController:destination animated:YES];
}


-(IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}



#pragma mark - UIImagePickerController 

//Change Account Image

-(IBAction)selectProfileImage:(id)sender{
    NSString *title = @"Select Profile Picture From: ";
    UIAlertController *alertController=   [UIAlertController
                                           alertControllerWithTitle:title
                                           message:@""
                                           preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *gallery = [UIAlertAction
                              actionWithTitle:@"Photo Gallery"
                              style:UIAlertActionStyleDefault
                              handler:^(UIAlertAction * action)
                              {
                                  [self selectFromExisting];
                              }];
    
    UIAlertAction *camera = [UIAlertAction
                             actionWithTitle:@"Camera"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [self takeNewPhoto];
                             }];
    
    UIAlertAction *cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleCancel
                             handler:^(UIAlertAction * action)
                             {
                                 [alertController dismissViewControllerAnimated:YES completion:nil];
                             }];
    [alertController addAction:camera];
    [alertController addAction:gallery];
    [alertController addAction:cancel];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void) takeNewPhoto{
    //this is used to check if the device have a camera or not .To avoid crashes .
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        imagePicker.allowsEditing = YES;
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:imagePicker animated:YES completion:NULL];
    }else{
        // NSLog(@"simulator doesnot have a camera");
    }
}
- (void) selectFromExisting{
    //this is used to check if the device have photo library or not .To avoid crashes .
    if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeSavedPhotosAlbum])
    {
        imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:imagePicker animated:YES completion:nil];
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *newImage  = [info objectForKey:UIImagePickerControllerOriginalImage];
//    [self.userImageView setImage:newImage];
//    //circle profile image
//    self.userImageView.layer.masksToBounds = YES ;
//    self.profileImageView.layer.cornerRadius = 50 ;
//    self.profileImageView.layer.borderColor = [UIColor lightGrayColor].CGColor;
//    self.profileImageView.layer.borderWidth = 2 ;
    [picker dismissViewControllerAnimated:YES completion:^{
        [self.userImageView setImage:newImage];
        if (![Helper checkInternet]) {
            [self.view makeToast:@"No Internet Connection!" duration:1 position:CSToastPositionBottom];
        } else {
            UIImage *img = self.userImageView.image ;
            NSData *imageData = UIImageJPEGRepresentation(img, .8);
            int size = (int)[imageData length]/1024 ;
            float scale = 0.9 ;
            while (size > 500) {
                imageData = UIImageJPEGRepresentation(img,scale);
                scale -= .1;
                size = (int)[imageData length]/1024;
            }
            imageBase64 = [imageData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
            [service editProfileImage:[NSNumber numberWithInt:1] userID:[NSNumber numberWithInt:[shared.user.id intValue]] imageContent:imageBase64];
            [self.view makeToast:@"Uploading Picture..." duration:100000 position:CSToastPositionBottom];
        }
    }];
}

#pragma mark - ServiceHandlerDelegate

-(void)accountImageEditSuccessfully:(NSString *)newURL {
    [self.view hideToasts];
    [self.view makeToast:@"Done" duration:1 position:CSToastPositionBottom];
    shared.user.image = newURL ;
    [_userImageView sd_setImageWithURL:[NSURL URLWithString:shared.user.image] placeholderImage:_userImageView.image];
}

-(void)requestFailWithError:(NSString *)err {
    [self.view hideToasts];
    [self showAlert:err];
}



@end
