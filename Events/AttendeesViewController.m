//
//  AttendeesViewController.m
//  Events
//
//  Created by M.I.Kamashany on 2/26/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import "AttendeesViewController.h"
#import "ServiceHandler.h"
#import "Helper.h"
#import "MBProgressHUD.h"
#import "AttendeeTableViewCell.h"
#import "ChattingViewController.h"
#import "SharedData.h"
#import "UIView+Toast.h"
#import "UIImageView+WebCache.h"
#import "ProfileViewController.h"

@interface AttendeesViewController ()<ServiceHandlerDelegate,UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate,UISearchDisplayDelegate,AttendeeTableViewCellDelegate>
{
    UIButton *refreshBtn ;
    UISearchDisplayController *searchDisplayController ;
    ServiceHandler *service ;
    int flag ;
    NSMutableArray *searchResults ;
    NSMutableDictionary *allAttendees;
    NSArray *attendeeSectionTitles;
    SharedData *shared ;
    
}
@property (nonatomic,weak) IBOutlet UITableView *allAttendeesTableView ;
@property (strong, nonatomic) UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UILabel *noAttendeesLabel;
@end

@implementation AttendeesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 20, 320, 44)];
    self.searchBar.placeholder = @"Search by company";
    self.searchBar.delegate = self;
    self.searchBar.showsCancelButton = YES;
    searchDisplayController = [[UISearchDisplayController alloc] initWithSearchBar:self.searchBar contentsController:self];
    searchDisplayController.delegate = self;
    searchDisplayController.searchResultsDataSource = self;
    searchDisplayController.searchResultsDelegate = self;
    searchDisplayController.searchResultsTableView.separatorStyle = UITableViewCellSelectionStyleNone ;
    
    [self.view addSubview:self.searchBar];
    self.searchBar.hidden = YES;
    [searchDisplayController setActive:NO animated:NO];

    service = [[ServiceHandler alloc]init];
    service.delegate = self ;
    shared = [SharedData getSharedObject];
    attendeeSectionTitles = [[NSMutableArray alloc]init];
    allAttendees = [[NSMutableDictionary alloc]init];
    flag = 0 ;

    self.allAttendeesTableView.estimatedRowHeight = 65 ;
    self.allAttendeesTableView.rowHeight = UITableViewAutomaticDimension ;
    [self hideTableView:YES];
    _noAttendeesLabel.hidden = YES ;
}


-(void)viewWillAppear:(BOOL)animated {
    if (flag == 0) {
        [self hideTableView:YES];
        if (![Helper checkInternet]) {
            //get cached attendees it exists
            NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:@"allAttendees"];
            if (data != nil) {
                shared.allAttendees = [NSKeyedUnarchiver unarchiveObjectWithData:data];
                allAttendees = shared.allAttendees ;
                [self showAttendees:allAttendees];
            }
            [self.view makeToast:@"No Internet Connection!"
                        duration:1.5
                        position:CSToastPositionBottom];
        }else{
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            [self hideTableView:YES];
            _noAttendeesLabel.hidden = YES ;
            [service loadAllAttendees:[NSNumber numberWithInt:1]];
        }
    }else{
        [self searchBarCancelButtonClicked:_searchBar];
        [_allAttendeesTableView reloadData];
    }
}

-(UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent ;
}

#pragma mark - ServiceHandlerDelegate

-(void)getAllAttendees:(NSMutableDictionary *)attendees {
    [self showAttendees:attendees];
}


-(void)requestFailWithError:(NSString *)err {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [self hideTableView:YES];
    _noAttendeesLabel.hidden = YES ;
    [self createRefreshBtn:err enabled:NO];
}

#pragma mark -TableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    if (tableView == searchDisplayController.searchResultsTableView) {
        return 1 ;
    }else{
        return [attendeeSectionTitles count];
    }
    
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (tableView == searchDisplayController.searchResultsTableView) {
        return nil ;
    }else{
        return [[attendeeSectionTitles objectAtIndex:section] uppercaseString];
    }
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == searchDisplayController.searchResultsTableView) {
        return [searchResults count];
    } else {
        // Return the number of rows in the section.
        NSString *sectionTitle = [attendeeSectionTitles objectAtIndex:section];
        NSArray *sectionAttendees = [allAttendees objectForKey:sectionTitle];
        return [sectionAttendees count];
    }
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellID = @"attendeeCell";
    AttendeeTableViewCell *cell = (AttendeeTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell == nil) {
        cell =[[AttendeeTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    if (tableView == searchDisplayController.searchResultsTableView) {
        AttendeeModel *attendeeModel = [searchResults objectAtIndex:indexPath.row];
        cell.attendeeNameLabel.text = attendeeModel.name;
//        if (![attendeeModel.company isEqualToString:@""] || attendeeModel.company == nil) {
//            cell.attendeePositionLabel.text = attendeeModel.company ;
//            cell.attendeePositionLabel.hidden = NO ;
//        }else {
//            cell.attendeePositionLabel.hidden = YES ;
//        }
        [cell.attendeeImageView setShowActivityIndicatorView:YES];
        [cell.attendeeImageView setIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [cell.attendeeImageView sd_setImageWithURL:[NSURL URLWithString:attendeeModel.image] placeholderImage:[UIImage imageNamed:@"profile-image"]];
        cell.delegate = self ;
        cell.selectedAttendee = attendeeModel ;
        tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    }else{
        NSString *attendeeTitle = [attendeeSectionTitles objectAtIndex:indexPath.section];
        NSArray *sectionAttendees = [allAttendees objectForKey:attendeeTitle];
        AttendeeModel *attendeeModel = [sectionAttendees objectAtIndex:indexPath.row];
        cell.attendeeNameLabel.text = attendeeModel.name;
//        if (![attendeeModel.company isEqualToString:@""] || attendeeModel.company == nil) {
//            cell.attendeePositionLabel.text = attendeeModel.company ;
//            cell.attendeePositionLabel.hidden = NO ;
//        }else {
//            cell.attendeePositionLabel.hidden = YES ;
//        }
        [cell.attendeeImageView setShowActivityIndicatorView:YES];
        [cell.attendeeImageView setIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [cell.attendeeImageView sd_setImageWithURL:[NSURL URLWithString:attendeeModel.image] placeholderImage:[UIImage imageNamed:@"profile-image"]];
        cell.delegate = self ;
        cell.selectedAttendee = attendeeModel ;
        tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    }
    
//    if ([shared.user.is_speaker isEqualToString:@"0"]) {
//        cell.sendMessageBtn.hidden = YES;
//        cell.sendMessageBigBtn.hidden = YES;
//    }else{
//        cell.sendMessageBtn.hidden = NO;
//        cell.sendMessageBigBtn.hidden = NO;
//    }
    return cell ;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 65 ;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    flag = 1 ;
    if (tableView == searchDisplayController.searchResultsTableView) {
        AttendeeModel *attendeeModel = [searchResults objectAtIndex:indexPath.row];
        ProfileViewController *destination = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
        destination.attendee = attendeeModel ;
        self.searchBar.hidden = YES;
        [self.searchBar resignFirstResponder];
        [self.navigationController pushViewController:destination animated:YES];
    }else{
        NSString *attendeeTitle = [attendeeSectionTitles objectAtIndex:indexPath.section];
        NSArray *sectionAttendees = [allAttendees objectForKey:attendeeTitle];
        AttendeeModel *attendeeModel = [sectionAttendees objectAtIndex:indexPath.row];
        ProfileViewController *destination = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
        destination.attendee = attendeeModel ;
        [self.navigationController pushViewController:destination animated:YES];
    }
    
}


- (IBAction)searchAboutAttendees:(id)sender {
    self.searchBar.hidden = NO;
    [self.searchBar becomeFirstResponder];
    [searchDisplayController setActive:YES animated:YES];
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    searchBar.hidden = YES;
    [searchBar resignFirstResponder];
    searchDisplayController.searchResultsTableView.hidden = YES ;
    _allAttendeesTableView.hidden = NO ;
    [searchDisplayController setActive:NO animated:YES];
}

- (void)searchDisplayControllerDidBeginSearch:(UISearchDisplayController *)controller {
    controller.searchResultsTableView.backgroundColor = [UIColor clearColor];
}

-(void)searchBarTextDidEndEditing:(UISearchBar *)searchBar{
    if ([searchBar.text isEqualToString:@""]) {
        searchBar.hidden = YES;
        [searchBar resignFirstResponder];
        searchDisplayController.searchResultsTableView.hidden = YES ;
        _allAttendeesTableView.hidden = NO ;
        [searchDisplayController setActive:NO animated:YES];
    }
}

-(IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)createRefreshBtn:(NSString *)title enabled:(BOOL)enabled{
    refreshBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [refreshBtn setTitle:title forState:UIControlStateNormal];
    refreshBtn.enabled = enabled ;
    refreshBtn.titleLabel.textColor = [UIColor darkGrayColor];
    refreshBtn.frame = CGRectMake(0, self.view.frame.size.height/2, self.view.frame.size.width, 25);
    [refreshBtn addTarget:self
                   action:@selector(refresh:)
         forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:refreshBtn];
    [self.view bringSubviewToFront:refreshBtn];
}

-(IBAction)refresh:(id)sender {
    if ([Helper checkInternet]) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [refreshBtn removeFromSuperview];
        [service loadAllAttendees:[NSNumber numberWithInt:1]];
    }
}

-(void)hideTableView:(BOOL)isHidden {
    _allAttendeesTableView.hidden = isHidden ;
}

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope {
//    NSArray *keys = [allAttendees allKeys];
//    NSArray *arrayToSearch ;
//    NSString *searchKey = [searchText stringByReplacingOccurrencesOfString:@" " withString:@""] ;
//    if (![searchKey isEqualToString:@""]) {
//        for (NSString *key in keys) {
//            if ([key.lowercaseString isEqualToString:[searchText substringToIndex:1].lowercaseString]) {
//               arrayToSearch = [allAttendees objectForKey:key];
//                break ;
//            }
//        }
//    }
    NSMutableArray *arrayToSearch = [[NSMutableArray alloc]init];
    NSArray *allKeys = [allAttendees allKeys];
    for (NSString *key in allKeys) {
        [arrayToSearch addObjectsFromArray:[allAttendees objectForKey:key]];
    }
    NSString *searchKey = [searchText stringByReplacingOccurrencesOfString:@" " withString:@""] ;
    
    NSPredicate *resultPredicate = [NSPredicate
                                    predicateWithFormat:@"company contains[c] %@",
                                    searchText];
    if([searchText isEqualToString:@""]){
        searchResults = arrayToSearch ;
    }else{
        searchResults = [NSMutableArray arrayWithArray:[arrayToSearch filteredArrayUsingPredicate:resultPredicate]];
        NSSortDescriptor *sorter = [[NSSortDescriptor alloc]initWithKey:@"name" ascending:YES];
        NSArray *sortdiscriptor=[[NSArray alloc]initWithObjects:sorter, nil];
        [searchResults sortUsingDescriptors:sortdiscriptor];
        _allAttendeesTableView.hidden = YES ;
    }
    
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller
shouldReloadTableForSearchString:(NSString *)searchString {
    [self filterContentForSearchText:searchString
                               scope:[[searchDisplayController.searchBar scopeButtonTitles]
                                      objectAtIndex:[searchDisplayController.searchBar
                                                     selectedScopeButtonIndex]]];
    
    return YES;
}

#pragma mark - AttendeeTableViewCellDelegate

-(void)sendMessageToAttendee:(AttendeeModel *)attendee {
    if (![Helper checkInternet]) {
        [self.view makeToast:@"No Internet Connection!"
                    duration:1.5
                    position:CSToastPositionBottom];
    } else {
        self.searchBar.hidden = YES;
        [self.searchBar resignFirstResponder];
        flag = 1 ;
        ChattingViewController *destination = (ChattingViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"ChattingViewController"];
        destination.anotherUserID = attendee.id ;
        destination.anotherUserImageURL = attendee.image ;
        [self.navigationController pushViewController:destination animated:YES];
    }
}

-(void)showAttendees:(NSMutableDictionary *)attendees {
    if ([attendees allKeys].count > 0) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        allAttendees = attendees ;
        shared.allAttendees = allAttendees ;
        NSMutableDictionary *newAttendees = [[NSMutableDictionary alloc]init];
        NSArray *attendeeArr ;
        for (NSString *key in [attendees allKeys]) {
            attendeeArr = [attendees objectForKey:key];
            if (attendeeArr.count > 0) {
                [newAttendees setObject:attendeeArr forKey:key];
            }
        }
        allAttendees = newAttendees ;
        attendeeSectionTitles = [[allAttendees allKeys]sortedArrayUsingSelector: @selector(localizedCaseInsensitiveCompare:)];
        [_allAttendeesTableView reloadData];
        [self hideTableView:NO];
        _noAttendeesLabel.hidden = YES ;
    } else {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [self hideTableView:YES];
        _noAttendeesLabel.hidden = NO ;
    }
}


@end
