//
//  SpeakerDetailsViewController.m
//  Events
//
//  Created by M.I.Kamashany on 2/11/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import "SpeakerDetailsViewController.h"
#import "SpeakerDetailsHeaderView.h"
#import "SessionTableViewCell.h"
#import "CoordinateManager.h"
#import "SpeakerDetailsHeaderView.h"
#import "UIImageView+RoundedImage.h"
#import "SideBarViewController.h"
#import "Speaker.h"
#import "UIImageView+WebCache.h"
#import "SessionDetailsViewController.h"
#import "SharedData.h"
#import "Helper.h"
#import "UIView+Toast.h"


@interface SpeakerDetailsViewController ()<UITableViewDelegate,UITableViewDataSource,SpeakerDetailsHeaderViewDelegate>
{
    SharedData *shared ;
}
@property CoordinateManager *coordinateManager;
@property (strong, nonatomic) UITableView *sessionsTableView;
@property (strong, nonatomic) SpeakerDetailsHeaderView *speakerDetailsView;

@end

@implementation SpeakerDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    shared = [SharedData getSharedObject];
}

-(void)viewWillAppear:(BOOL)animated {
    self.sessionsTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, self.view.frame.size.width, self.view.frame.size.height-60) style:UITableViewStylePlain];
    self.sessionsTableView.dataSource = self;
    self.sessionsTableView.delegate = self;
    self.sessionsTableView.backgroundColor = [UIColor colorWithRed:222.0/255 green:222.0/255 blue:222.0/255 alpha:1];
    
    self.speakerDetailsView = [[SpeakerDetailsHeaderView alloc]init];
    self.speakerDetailsView.frame = CGRectMake(0, 0, self.view.frame.size.width, 323);
    self.speakerDetailsView.delegate = self ;
    
    self.coordinateManager = [[CoordinateManager alloc]initManager:self scroll:self.sessionsTableView header:self.speakerDetailsView];
    self.sessionsTableView.separatorColor = [UIColor clearColor];
    self.sessionsTableView.backgroundColor = [UIColor colorWithRed:255.0/255 green:255.0/255 blue:255.0/255 alpha:1];
    // set views
    [_coordinateManager setContainer:self.sessionsTableView views:nil];
    //    _coordinateManager.backgroundColor = [UIColor colorWithRed:222.0/255 green:222.0/255 blue:222.0/255 alpha:1];
    [self.view addSubview:self.sessionsTableView];
    
    self.sessionsTableView.estimatedRowHeight = 118 ;
    self.sessionsTableView.rowHeight = UITableViewAutomaticDimension ;
    self.sessionsTableView.separatorStyle = UITableViewCellSeparatorStyleNone ;

    
    if (self.selectedSpeaker) {
        self.speakerDetailsView.speakerNameLabel.text = [NSString stringWithFormat:@"%@",self.selectedSpeaker.name] ;
        [self.speakerDetailsView.speakerImageView setShowActivityIndicatorView:YES];
        [self.speakerDetailsView.speakerImageView setIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [self.speakerDetailsView.speakerImageView sd_setImageWithURL:[NSURL URLWithString:self.selectedSpeaker.image] placeholderImage:[UIImage imageNamed:@"profile-image"]];
        self.speakerDetailsView.speakerDetailsLabel
        .text = self.selectedSpeaker.position ;
        self.speakerDetailsView.speakerMoreInfo.text = self.selectedSpeaker.bio ;
        [self changeTextViewFrame:_speakerDetailsView.speakerMoreInfo];
    }
}

#pragma mark -SpeakerDetailsHeaderViewDelegate 

-(void)completeShow:(int)flag  {
    switch (flag) {
        case 1:
            if (self.selectedSpeaker.facebook_link != nil && ![self.selectedSpeaker.facebook_link isEqualToString:@""]) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.selectedSpeaker.facebook_link]];
            } else {
                [self.view makeToast:@"There is no link yet!"
                            duration:1.0
                            position:CSToastPositionBottom];
            }
            break;
        case 2:
            if (self.selectedSpeaker.twitter_link != nil && ![self.selectedSpeaker.twitter_link isEqualToString:@""]) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.selectedSpeaker.twitter_link]];
            } else {
                [self.view makeToast:@"There is no link yet!"
                            duration:1.0
                            position:CSToastPositionBottom];
            }
            break;
        case 3:
            if (self.selectedSpeaker.linkedin_link != nil && ![self.selectedSpeaker.linkedin_link isEqualToString:@""]) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.selectedSpeaker.linkedin_link]];
            } else {
                [self.view makeToast:@"There is no link yet!"
                            duration:1.0
                            position:CSToastPositionBottom];
            }
            break;
        default:
            break;
    }
}


#pragma mark -TableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.selectedSpeaker.sessions) {
        return self.selectedSpeaker.sessions.count;
    }else{
        return 0 ;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellID = @"sessionCell";
    SessionTableViewCell *cell = (SessionTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell == nil) {
        cell =[[SessionTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    
    Session *selectedSession = [self.selectedSpeaker.sessions objectAtIndex:indexPath.row];
    cell.sessionDescLabel.text = selectedSession.session_name;
    NSString *startTime = [Helper detectPMAM:selectedSession.start_time];
    NSString *endTime = [Helper detectPMAM:selectedSession.end_time];
    cell.calenderLabel.text = [NSString stringWithFormat:@"Ends %@ - %@",startTime,endTime];
    cell.timeLabel.text = @"Main Meeting Room" ;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell ;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    SessionDetailsViewController *destination = [self.storyboard instantiateViewControllerWithIdentifier:@"SessionDetailsViewController"];
    destination.selectedSession = [self.selectedSpeaker.sessions objectAtIndex:indexPath.row];
    destination.isComingFromSpeakers = YES ;
    destination.selectedSessionIndexPath = indexPath ;
    destination.selectedSpeakerIndex = self.selectedSpeakerIndex ;
    [self.navigationController pushViewController:destination animated:YES];
}
-(IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)showMenu:(id)sender {
    SideBarViewController *destination = (SideBarViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"SideBarViewController"];
    [self.navigationController pushViewController:destination animated:YES];
}

- (void)changeTextViewFrame:(UITextView *)textView {
    
    CGFloat fixedWidth = textView.frame.size.width;
    CGSize newSize = [textView sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
    CGRect newFrame = textView.frame;
    newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), newSize.height);
    textView.frame = newFrame;
    if (newSize.height > _speakerDetailsView.speakerMoreInfoHightConstraint.constant) {
        _speakerDetailsView.speakerMoreInfoHightConstraint.constant = newSize.height ;
        [self.view layoutIfNeeded];
    }
}
         
@end
