//
//  FullAgendaListViewController.m
//  Events
//
//  Created by M.I.Kamashany on 3/6/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import "FullAgendaListViewController.h"
#import "FullAgendaTableViewCell.h"
#import "Speaker.h"
#import "SessionDetailsViewController.h"
#import "SharedData.h"
#import "AgendaModel.h"
#import "Speaker.h"
#import "ServiceHandler.h"
#import "Helper.h"
#import "UIView+Toast.h"


@interface FullAgendaListViewController ()<FullAgendaTableViewCellDelegate>
{
    SharedData *shared ;
    ServiceHandler *service ;
}
@property (nonatomic,weak) IBOutlet UITableView *agendaSessionsTableView ;

@end

@implementation FullAgendaListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    service = [[ServiceHandler alloc]init];
    shared = [SharedData getSharedObject];
    _agendaSessionsTableView.estimatedRowHeight = 116 ;
    _agendaSessionsTableView.rowHeight = UITableViewAutomaticDimension ;
    _agendaSessionsTableView.separatorStyle = UITableViewCellSeparatorStyleNone ;
    _agendaSessionsTableView.allowsSelection = false;
}

-(UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent ;
}

#pragma mark -TableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.sessionOfDay) {
        return self.sessionOfDay.count ;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellID = @"fullAgendaCell";
    FullAgendaTableViewCell *cell = (FullAgendaTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell == nil) {
        cell =[[FullAgendaTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    Session *session = [self.sessionOfDay objectAtIndex:indexPath.row];
    if (session.list_of_speaker.count == 0 ) {
        cell.speakerNameLabel.hidden = true ;
        cell.speakerFixedImageView.hidden = true;
    }else{
        cell.speakerNameLabel.hidden = false ;
        cell.speakerFixedImageView.hidden = false;
        Speaker *speaker = session.list_of_speaker[0];
        cell.speakerNameLabel.text = speaker.name;
    }
    
    cell.sessionDescLabel.text = session.session_name ;
    NSString *startTime = [Helper detectPMAM:session.start_time];
    NSString *endTime = [Helper detectPMAM:session.end_time];
    cell.calenderLabel.text = [NSString stringWithFormat:@"%@ - %@",startTime,endTime];
    if (session.physical_address == nil ){
        cell.timeLabel.text = @"No Location" ;
    }else{
        if ([session.physical_address isEqualToString:@""] ){
            cell.timeLabel.text = @"No Location" ;
        }else{
            cell.timeLabel.text = session.physical_address ;
        }
        
    }
    
//    cell.timeLabel.text = @"Main Meeting Room";
    
    cell.indexPath = indexPath ;
    cell.delegate = self ;
    if ([session.add_to_agenda isEqualToString:@"1"]) {
        [cell.addToAgendaBtn setImage:[UIImage imageNamed:@"addedToAgenda.png"] forState:UIControlStateNormal];
    }
    return cell ;
}

//-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    [tableView deselectRowAtIndexPath:indexPath animated:YES];
//    SessionDetailsViewController *destination = [self.storyboard instantiateViewControllerWithIdentifier:@"SessionDetailsViewController"];
//    destination.selectedSession = [self.sessionOfDay objectAtIndex:indexPath.row];
//    //used when press from full agenda
//    destination.isFromFullAgenda = @"0";
//    destination.selectedAgendaIndex = _selectedAgendaIndex ;
//    destination.selectedSessionIndex = (int)indexPath.row ;
//    destination.isComingFromSpeakers = NO ;
//    [self.navigationController pushViewController:destination animated:YES];
//}

#pragma mark - FullAgendaTableViewCellDelegate

-(void)addToAgenda:(NSIndexPath *)index {
    AgendaModel *agenda = [shared.allAgenda objectAtIndex:_selectedAgendaIndex];
    Session *selectedSession = [agenda.sessions objectAtIndex:index.row];
    if ([selectedSession.add_to_agenda isEqualToString:@"0"]) {
        if (![Helper checkInternet]) {
            [self.agendaSessionsTableView makeToast:@"No Internet Connection!"
                        duration:0.75
                        position:CSToastPositionBottom];
        } else {
            selectedSession.add_to_agenda = @"1";
            [agenda.sessions removeObjectAtIndex:index.row];
            [agenda.sessions insertObject:selectedSession atIndex:index.row];
            [shared.allAgenda removeObjectAtIndex:_selectedAgendaIndex];
            [shared.allAgenda insertObject:agenda atIndex:_selectedAgendaIndex];
            
            [self.agendaSessionsTableView beginUpdates];
            [self.agendaSessionsTableView reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationNone];
            [self.agendaSessionsTableView endUpdates];
            [service addSessionToAgenda:[NSNumber numberWithInt:1] sessionID:[NSNumber numberWithInt:[selectedSession.id intValue]] userID:[NSNumber numberWithInt:[shared.user.id intValue]]];
        }
    }else{
        if (![Helper checkInternet]) {
            [self.agendaSessionsTableView makeToast:@"No Internet Connection!"
                        duration:0.75
                        position:CSToastPositionBottom];
        } else {
            selectedSession.add_to_agenda = @"0";
            [agenda.sessions removeObjectAtIndex:index.row];
            [agenda.sessions insertObject:selectedSession atIndex:index.row];
            [shared.allAgenda removeObjectAtIndex:_selectedAgendaIndex];
            [shared.allAgenda insertObject:agenda atIndex:_selectedAgendaIndex];
            
            [self.agendaSessionsTableView beginUpdates];
            [self.agendaSessionsTableView reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationNone];
            [self.agendaSessionsTableView endUpdates];
            [service removeSessionFromAgenda:[NSNumber numberWithInt:1] sessionID:[NSNumber numberWithInt:[selectedSession.id intValue]] userID:[NSNumber numberWithInt:[shared.user.id intValue]]];
        }
    }
}

@end
