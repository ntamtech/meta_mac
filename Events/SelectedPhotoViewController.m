//
//  SelectedPhotoViewController.m
//  Events
//
//  Created by M.I.Kamashany on 3/25/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import "SelectedPhotoViewController.h"
#import "KIImagePager.h"
#import "SharedData.h"
#import "SliderModel.h"
#import "SideBarViewController.h"
#import "Social/Social.h"
#import "PostModel.h"
#import "Helper.h"
#import "UIView+toast.h"
#import "ServiceHandler.h"
#import "MBProgressHUD.h"
#import "UIViewController+Alert.h"
#import "CommentsViewController.h"

@interface SelectedPhotoViewController ()<KIImagePagerDelegate,ServiceHandlerDelegate>
{
    ServiceHandler *service ;
    SharedData *shared ;
}
@property (weak, nonatomic) IBOutlet KIImagePager *imagePager;
@property (weak, nonatomic) IBOutlet UIButton *saveBtn;
@property (weak, nonatomic) IBOutlet UILabel *imageNumberLabel;
@property (weak, nonatomic) IBOutlet UIButton *deleteBtn;
@property (weak, nonatomic) IBOutlet UIImageView *likePhotoImageView;

- (IBAction)deletePhoto:(id)sender;
- (IBAction)savePhoto:(id)sender;
@end

@implementation SelectedPhotoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    service = [[ServiceHandler alloc]init];
    service.delegate = self ;
    shared = [SharedData getSharedObject];
    _saveBtn.layer.cornerRadius = 3 ;
    _saveBtn.layer.borderColor = [UIColor whiteColor].CGColor;
    _saveBtn.layer.borderWidth =0.75 ;
    
    _imagePager.pageControl.currentPageIndicatorTintColor = [UIColor whiteColor];
    _imagePager.slideshowShouldCallScrollToDelegate = YES;
    [_imagePager setImageCounterDisabled:YES];
    _imagePager.indicatorDisabled = YES ;
    self.imageNumberLabel.text = [NSString stringWithFormat:@"%lu/%lu",(unsigned long)self.selectedPhotoNumber.row+1,(unsigned long)shared.photosArr.count];
    _imagePager.delegate = self ;
    
}

-(void)viewWillAppear:(BOOL)animated {
    [self isShowDeleteBtn];
    [self ISPhotoLiked];
}

-(void)viewDidAppear:(BOOL)animated{
    [_imagePager setCurrentPage:self.selectedPhotoNumber.row] ;
}

#pragma mark - KIImagePager DataSource

- (NSArray *) arrayWithImages:(KIImagePager*)pager {
    NSMutableArray *arr = [[NSMutableArray alloc]init];
    for (SliderModel *slider in shared.photosArr) {
        [arr addObject:slider.image];
    }
    return  arr ;
}

- (UIViewContentMode) contentModeForImage:(NSUInteger)image inPager:(KIImagePager *)pager {
    return UIViewContentModeScaleAspectFill;
}


- (void) imagePager:(KIImagePager *)imagePager didScrollToIndex:(NSUInteger)index {
    _selectedPhotoNumber = [NSIndexPath indexPathForRow:index inSection:0];
    [self ISPhotoLiked];
    [self isShowDeleteBtn] ;
    self.imageNumberLabel.text = [NSString stringWithFormat:@"%lu/%lu",(unsigned long)index+1,(unsigned long)shared.photosArr.count];
}

//- (void) imagePager:(KIImagePager *)imagePager didSelectImageAtIndex:(NSUInteger)index {
//    self.imageNumberLabel.text = [NSString stringWithFormat:@"%lu/%lu",(unsigned long)index,(unsigned long)shared.photosArr.count];
//}


- (IBAction)savePhoto:(id)sender {
    PhotoModel *photoModel = [shared.photosArr objectAtIndex:self.selectedPhotoNumber.row];
    UIImage *selectedImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:photoModel.image]]];
    if (selectedImage != nil) {
        UIImageWriteToSavedPhotosAlbum(selectedImage, nil, nil, nil);
        [self.view makeToast:@"Photo saved"
                    duration:1
                    position:CSToastPositionCenter];
    }else{
        [self.view makeToast:@"No Internet Connection to save photo"
                    duration:1
                    position:CSToastPositionCenter];
    }
    
}

-(IBAction)showMenu:(id)sender {
    SideBarViewController *destination = (SideBarViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"SideBarViewController"];
    [self.navigationController pushViewController:destination animated:YES];
}

-(IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)sharePhoto:(id)sender {
    if (![Helper checkInternet]) {
        [self.view makeToast:@"No Internet Connection!"
                    duration:1
                    position:CSToastPositionCenter];
    } else {
        PhotoModel *photoModel = [shared.photosArr objectAtIndex:self.selectedPhotoNumber.row];
    //    UIImage *selectedImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:];
        NSArray * shareItems = @[photoModel.image];
        UIActivityViewController * avc = [[UIActivityViewController alloc] initWithActivityItems:shareItems applicationActivities:nil];
        [self presentViewController:avc animated:YES completion:nil];
    }
}

-(IBAction)likePhoto:(id)sender {
    PhotoModel *photoModel = [shared.photosArr objectAtIndex:self.selectedPhotoNumber.row];
    if (![Helper checkInternet]) {
        [self.view makeToast:@"No Internet Connection!"
                    duration:1
                    position:CSToastPositionBottom];
    } else {
        if ([photoModel.make_like isEqualToString:@"0"]) {
            [service likePhoto:[NSNumber numberWithInt:1] userID:[NSNumber numberWithInt:[shared.user.id intValue]] photoID:[NSNumber numberWithInt:[photoModel.id intValue]]];
            photoModel.make_like = @"1";
            _likePhotoImageView.image = [UIImage imageNamed:@"likeIcon.png"];
            [shared.photosArr removeObjectAtIndex:self.selectedPhotoNumber.row];
            [shared.photosArr insertObject:photoModel atIndex:self.selectedPhotoNumber.row];
        }else{
            [service likePhoto:[NSNumber numberWithInt:1] userID:[NSNumber numberWithInt:[shared.user.id intValue]] photoID:[NSNumber numberWithInt:[photoModel.id intValue]]];
            photoModel.make_like = @"0";
            _likePhotoImageView.image = [UIImage imageNamed:@"unlikeIcon2.png"];
            [shared.photosArr removeObjectAtIndex:self.selectedPhotoNumber.row];
            [shared.photosArr insertObject:photoModel atIndex:self.selectedPhotoNumber.row];
        }
    }
    
}

-(IBAction)deletePhoto:(id)sender {
    if (![Helper checkInternet]) {
        [self.view makeToast:@"No Internet Connection!"
                    duration:1
                    position:CSToastPositionCenter];
    } else {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        PhotoModel *photoModel = [shared.photosArr objectAtIndex:self.selectedPhotoNumber.row];
        [shared.photosArr removeObjectAtIndex:self.selectedPhotoNumber.row];
        [service deletePhoto:[NSNumber numberWithInt:[photoModel.id intValue]]];
    }
}

-(BOOL)canDeletePhoto {
    PhotoModel *photoModel = [shared.photosArr objectAtIndex:self.selectedPhotoNumber.row];
    if ([shared.user.id intValue] == [photoModel.user_id intValue] || [shared.user.admin isEqualToString:@"1"]) {
        return YES ;
    } else {
        return NO ;
    }
}

//check if your admin or owner of the image
-(void) isShowDeleteBtn {
    if ([self canDeletePhoto]) {
        self.deleteBtn.hidden = NO ;
    }else{
        self.deleteBtn.hidden = YES ;
    }
}

-(void)ISPhotoLiked {
    PhotoModel *photoModel = [shared.photosArr objectAtIndex:self.selectedPhotoNumber.row];
    if ([photoModel.make_like isEqualToString:@"0"]) {
        _likePhotoImageView.image = [UIImage imageNamed:@"unlikeIcon2.png"];
    } else {
        _likePhotoImageView.image = [UIImage imageNamed:@"likeIcon.png"];
    }
}

#pragma mark - ServiceHandlerDelegate

-(void)likePhotoSuccessfully {}

-(void)deletePhotoSuccessfully {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)requestFailWithError:(NSString *)err {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [self showAlert:err];
}

-(IBAction)showPhotoComments:(id)sender {
    
    PhotoModel *photo = [shared.photosArr objectAtIndex:self.selectedPhotoNumber.row];
    CommentsViewController *destination = [self.storyboard instantiateViewControllerWithIdentifier:@"CommentsViewController"];
    destination.selectePhoto = photo ;
    destination.selectedIndex = self.selectedPhotoNumber ;
    destination.selectedPost = nil ;
    destination.selectedSession = nil ;
    [self.navigationController pushViewController:destination animated:YES];
}

@end
