//
//  SessionDetailsHeaderView.h
//  Events
//
//  Created by M.I.Kamashany on 2/27/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EDStarRating.h"
#import "Speaker.h"

@protocol  SessionDetailsHeaderViewDelegate <NSObject>

@optional

-(void)addSessionToAgendaWithSessionID:(NSNumber *)sessionID ;
-(void)addSessionRate:(NSNumber *)sessionID ;

@end

@interface SessionDetailsHeaderView : UIView

@property (nonatomic,weak) id<SessionDetailsHeaderViewDelegate> delegate ;
@property (nonatomic,strong) Session *selectedSession ;
@property (weak, nonatomic) IBOutlet UILabel *sessionNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *sessionTagLabel;
@property (weak, nonatomic) IBOutlet UILabel *calenderLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;
@property (weak, nonatomic) IBOutlet UIImageView *addToAgendaImageView;
@property (weak, nonatomic) IBOutlet UIButton *addToAgendaBtn;
- (IBAction)addToAgenda:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *sessionDetailsTV;
@end
