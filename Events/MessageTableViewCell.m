//
//  MessageTableViewCell.m
//  Events
//
//  Created by M.I.Kamashany on 2/12/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import "MessageTableViewCell.h"
#import "UIImageView+RoundedImage.h"

@implementation MessageTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        NSArray *nibs = [[NSBundle mainBundle] loadNibNamed:@"MessageTableViewCell" owner:self options:nil];
        self = [nibs objectAtIndex:0];
        [self.messageIcon setRoundedWithCornerRadius:27.5];
    }
    return self;
}

@end
