//
//  PublicCommentTableViewCell.h
//  Events
//
//  Created by M.I.Kamashany on 3/29/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

#import <UIKit/UIKit.h>

//comment messages
@interface PublicCommentTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *commentBodyLabel;
@property (weak, nonatomic) IBOutlet UIImageView *commentOwnerImageView;

@end
